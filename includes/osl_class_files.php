<?php
/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/

/////////////////////////////////////
// OSLogbook file management info //
///////////////////////////////////

class oslFile
{
	// Upload function.
	static function upload($content,$step,$callRep,$allowed_file_type,$allowed_img_type,$upload_dir,$addCommentTo,$tabs)
	{
	 	// Set uploads location.
		$uploadLocation = "uploads/";
		// Add number of tabs required.
	 	$tabStr = oslStructure::getRequiredTabs($tabs);
		// Check if report is a comment.
		$parentFK = oslContent::getReportInfo("parentFK",$callRep);
	 	$isComment = FALSE;
		// If editing a comment.
		if($parentFK != 0 && $parentFK != NULL)
		{
			$isComment = TRUE;
		}
		// Provide possibility to upload files.
		// Open form.
		$str .= "$tabStr<form name=\"uploadFile\" id=\"uploadFile\" method=\"POST\" enctype=\"multipart/form-data\">\n";
		// Open div.
		$str .= oslStructure::openDiv("divUpload",$tabs,"formContainer");
		$str .= "$tabStr	<p>Browse to file to upload <input type=\"file\" name=\"filetoupload\"></p>\n";
		$str .= oslFile::outputUploadableTypes(FALSE,4);
		$str .= "$tabStr	".oslFile::getMaxFilesizeForDisplay()."\n";
		$str .= "$tabStr	<p><img src=\"images/attention.gif\" alt=\"Attention\" class=\"attention\" />Note that, when your report is posted to the Logbook, files attached to it will be ordered in the same way as they are below.</p>\n";
		$str .= "$tabStr	<p><img src=\"images/attention.gif\" alt=\"Attention\" class=\"attention\" />Note that, large images will not have previews generated for them (above 17 mega-pixels).</p>\n";
		// Close div.
		$str .= oslStructure::closeDiv("divUpload",$tabs);
		// Get buttons.
		$str .= "$tabStr<div class=\"formContainer\">\n";
		$str .= oslStructure::getButton("btnReturnToReport","RETURN TO REPORT",NULL,"index.php?content=3&callRep=$callRep&addCommentTo=$addCommentTo",NULL,NULL,$tabs+1);
		$str .= oslStructure::getButton("btnUploadFile","UPLOAD FILE","uploadFile","includes/confirmation.php?adminType=upload&content=$content&step=$step&callRep=$callRep&addCommentTo=$addCommentTo",NULL,NULL,$tabs+1);
		$str .= "$tabStr</div>\n";
		// Close form.
		$str .= "$tabStr</form>\n";
		// Display attached images.
		$str .= oslFile::displayFiles($callRep,$content,$step,1,$isComment,$tabs);
		// Display other files.
		$str .= oslFile::displayFiles($callRep,$content,$step,0,$isComment,$tabs);
		// Return.
		return $str;
	}

	// Format a filesize.
	static function formatFileSize($filesize,$addText)
	{
		// If the file size is 1GB or greater, round it.
		if($filesize > 999999999)
		{
			$filesize_formatted = round($filesize/1000000000,2);
			$type = " GB";
		}
		// Otherwise, if 1MB or greater.
		elseif($filesize > 999999)
		{
			$filesize_formatted = round($filesize/1000000,2);
			$type = " MB";
		}
		// Otherwise, treat as KB.
		else
		{
			$filesize_formatted = round($filesize/1000,0);
			$type = " KB";
		}
		// If KB/MB text has been requested add it here.
		if($addText == TRUE)
		{
			$filesize_formatted .= $type;
		}
		// Return filesize_formatted.
		return $filesize_formatted;
	}

	// Get Max file size as display.
	static function getMaxFilesizeForDisplay()
	{
		// Get MB value.
		$str = "<p><strong>Maximum file size:</strong> ".oslFile::formatFileSize(oslApp::getAppInfo("appMaxFileSize"),TRUE)."</p>\n";
		// Return.
		return $str;
	}

	// Get files as SQL array.
	static function getFilesAsSqlArray($callRep, $imgsOrFiles)
	{
		// Get files attached to this report.
		// $sql = oslDAO::executeQuery("SELECT *, DATE_FORMAT(date_uploaded,'%H:%i, %W, %e %M %Y') AS 'date_uploaded_fmt'
		// 							 FROM tblFiles
		// 							 LEFT JOIN tblFileTypes ON tblFiles.fileTypeFK = tblFileTypes.fileTypeID
		// 							 WHERE reportFK=$callRep AND fileImage=$imgsOrFiles
		// 							 ORDER BY fileID");
    //
		// return $sql

		$sql = "SELECT *, DATE_FORMAT(date_uploaded,'%H:%i, %W, %e %M %Y')
							AS 'date_uploaded_fmt' FROM tblFiles
							LEFT JOIN tblFileTypes ON tblFiles.fileTypeFK = tblFileTypes.fileTypeID
							WHERE reportFK= ? AND fileImage=? ORDER BY fileID";
		$types = "ii";
		$input = array($callRep, $imgsOrFiles);
		$rows = oslDAO::executePrepared($sql, $types, $input);
		// Return value.
		return $rows;
	}

	// Display files that have been uploaded to this report.
	static function displayFiles($callRep,$content,$step,$imgsOrFiles,$isComment,$tabs)
	{
	 	// Get variables set elsewhere.
	 	global $upload_dir, $tn_dir, $preview;
	 	// Init.
	 	$txt = "";
	 	$box = "";
		// Add number of tabs required.
	 	$tabStr = oslStructure::getRequiredTabs($tabs);
		// Check if files exist for this report.
	 	if(oslFile::checkForFiles($callRep,$imgsOrFiles) && $callRep != 0 && $callRep != NULL)
		{
		    // If comment.
		 	$repOrCom = "report";
		 	if($isComment == TRUE)
		 	{
				$repOrCom = "comment";
			}
			// prefix for the url
			$prefix = "http".($_SERVER["HTTPS"]?"s":"")."://".$_SERVER["HTTP_HOST"].str_replace("/includes", "", pathinfo($_SERVER["PHP_SELF"], PATHINFO_DIRNAME ));
			// Get file SQL array.
			$rows = oslFile::getFilesAsSqlArray($callRep,$imgsOrFiles);
			// Loop.
			// while($loop = mysql_fetch_array($sql))
			foreach($rows as $loop)
			{
				// Init.
				$fileID = $loop["fileID"];
				$fullFilename = $loop["filename"];
				$exp = explode("_",$fullFilename,3);
				$filename = $exp[2];
				$fullFilename = urlencode($fullFilename);
				$fileType = $loop["fileType"];
				$date_uploaded = "Uploaded ".$loop["date_uploaded_fmt"];
				$filesize = $loop["filesize"];
	 			// Images only.
				if($imgsOrFiles == 1)
				{
					$title = $tabStr."Images attached to this $repOrCom\n";
					$fileImg = $tn_dir."tn_$fullFilename";
					$imgClass = "thumbnailImg";
					$outputFile = $fileImg;
				}
				// Other file types.
				else
				{
					$title = $tabStr."Non-image files attached to this $repOrCom\n";
					// If viewing.
					$fileImg = "images/$fileType.gif";
					$imgClass = "";
					$outputFile = $upload_dir.$fullFilename;
				}
				// If dealing with non-images or editing.
				if($imgsOrFiles == 0 || $content == 3 || $content == 30)
				{
					$txt .= "$tabStr	<div class=\"uploadedImg\">\n";
					// Add edit options.
					if($content == 3)
					{
						$txt .= "$tabStr		<div class=\"uploadedFileType\"><a href=\"includes/confirmation.php?adminType=delFile&callRep=$callRep&callFile=$fileID\"><img src=\"images/delFile.gif\" onclick=\"return confirm('Are you sure you wish to delete this file?')\" /></a></div>\n";
					}
					$txt .= "$tabStr		<div class=\"uploadedFileType\"><img src=\"$prefix/$fileImg\"  class=\"$imgClass\" /></div>\n";
					$txt .= "$tabStr		<div class=\"uploadedFileName\"><a href=\"$prefix/$upload_dir$fullFilename\" target=\"blank\">$filename</a>";
					// Add date info.
					if($content == 3 || $content == 30)
					{
						$txt .= " ($date_uploaded) (".oslFile::formatFileSize($filesize,TRUE).")";
					}
					$txt .= "</div>\n";
					$txt .= "$tabStr	</div>\n";
				}
				// Otherwise user is outputting from main page.
				else
				{
		 			// Images only.
					if($content == 1 && $imgsOrFiles == 1)
					{
						// Add file.
						$txt .= "$tabStr	<a href=\"$prefix/uploads/$fullFilename\" target=\"blank\"><img src=\"$prefix/$outputFile\" class=\"outputImg\" /></a>\n";
					}
				}
			}
			// If editing.
			if($content == 3 || $content == 30)
			{
			 	$box = oslStructure::openDiv("divFile$reportID",$tabs,"formContainer");
			 	$box .= "$tabStr	<!-- Output $title -->\n";
			 	$box .= "$tabStr	<p><strong>$title</strong></p>\n";
				$box .= $txt;
				$box .= oslStructure::closeDiv("divFile$reportID",$tabs);
			}
			else
			{
	 			// Set preview class if required.
				$callType = NULL;
				if($preview == $callRep)
				{
					$callType = "Preview";
				}
				// Incorporate title in header.
				$box = oslStructure::openDiv("file_".$imgsOrFiles."_$callRep",$tabs,"commentHdr").
					   $tabStr.$title.
					   oslStructure::closeDiv("file_".$imgsOrFiles."_$callRep",$tabs);
				// Add to div.
				$box .= oslStructure::openDiv("files_".$imgsOrFiles."_$callRep",$tabs,"reportDetails$callType");
				// Add contents.
				$box .= $txt;
				// Close div.
				$box .= oslStructure::closeDiv("files_".$imgsOrFiles."_$callRep",$tabs);
			}
			// Add break.
			$box .= oslStructure::getBreak($tabs);
		}
		// Return content.
		return $box;
	}

	// Get file details.
	static function getFileDetails($callFile,$field)
	{
	 	// Init.
	 	$val = NULL;
		// If a file has been called.
		if($callFile != 0 && $callFile != NULL)
		{
			// Get info.
			// $sql = oslDAO::executeQuery("SELECT $field
			// 							 FROM tblFiles
			// 							 LEFT JOIN tblFileTypes ON tblFiles.fileTypeFK = tblFileTypes.fileTypeID
			// 							 WHERE fileID=$callFile");
			$sql = "SELECT $field FROM tblFiles LEFT JOIN tblFileTypes
							ON tblFiles.fileTypeFK = tblFileTypes.fileTypeID
							WHERE fileID=?";
			$types = "i";
			$input = array($callFile);
			$rows = oslDAO::executePrepared($sql, $types, $input);
			// Init.
			// $val = mysql_result($sql,0,"$field");
			$val = oslDAO::getFirstResultEntry($rows, "$field");
		}
		// Return value.
		return $val;
	}

	// Get file details.
	static function getFileTypeDetails($callFileType,$field)
	{
	 	// Init.
	 	$val = NULL;
		// If a file has been called.
		if($callFileType != 0 && $callFileType != NULL)
		{
			// Get info.
			// $sql = oslDAO::executeQuery("SELECT $field
			// 							 FROM tblFileTypes
			// 							 WHERE fileTypeID=$callFileType");

			$sql = "SELECT $field FROM tblFileTypes WHERE fileTypeID=?";
			$types = "i";
			$input = array($callFileType);
			$rows = oslDAO::executePrepared($sql, $types, $input);
			// Init.
			// $val = mysql_result($sql,0,"$field");
			$val = oslDAO::getFirstResultEntry($rows, $field);
		}
		// Return value.
		return $val;
	}

	// Check if file type is an image.
	static function checkIfFileTypeIsImg($callFileType)
	{
	 	// Init.
	 	$isImg = FALSE;
		// If a file has been called.
		if($callFileType != NULL)
		{
			// Get info.
			// $sql = oslDAO::executeQuery("SELECT fileImage
			// 							 FROM tblFileTypes
			// 							 WHERE fileType LIKE '$callFileType' AND fileImage=1
			// 							 LIMIT 1");

			$sql = "SELECT fileImage FROM tblFileTypes WHERE fileType
							LIKE ? AND fileImage=1 LIMIT 1";
			$types = "s";
			$input = array($callFileType);
			$rows = oslDAO::executePrepared($sql, $types, $input);
			// Loop.
			// while($loop = mysql_fetch_array($sql))
			foreach($rows as $loop)
			{
				// Init.
				$isImg = TRUE;
			}
		}
		// Return value.
		return $isImg;
	}

	// Delete file type.
	static function deleteFileType($userID,$callFileType)
	{
	 	// Init.
	 	$url = "../index.php?content=7";
	 	// If the user is Admin.
	 	if(oslUser::checkIfAdmin($userID))
	 	{
			// Delete.
			// $del = oslDAO::executeQuery("DELETE FROM tblFileTypes
			// 							 WHERE fileTypeID = $callFileType");

			$sql = "DELETE FROM tblFileTypes WHERE fileTypeID = ?";
			$types = "i";
			$input = array($callFileType);
			$rows = oslDAO::executePrepared($sql, $types, $input);
		}
		// Return.
		return $url;
	}

	// Check if files are attached to this report.
	static function checkForFiles($callRep,$imgsOrFiles)
	{
		// Init.
		$filesAttached = FALSE;
		// If a rep has been called.
		if($callRep != 0 && $callRep != NULL)
		{
			// Get files attached to this report.
			// $sql = oslDAO::executeQuery("SELECT fileID
			// 							 FROM tblFiles
			// 							 LEFT JOIN tblFileTypes ON tblFiles.fileTypeFK = tblFileTypes.fileTypeID
			// 							 WHERE reportFK=$callRep AND fileImage=$imgsOrFiles
			// 							 LIMIT 1");

			$sql = "SELECT fileID FROM tblFiles LEFT JOIN tblFileTypes
							ON tblFiles.fileTypeFK = tblFileTypes.fileTypeID
							WHERE reportFK=? AND fileImage=? LIMIT 1";
			$types = "ii";
			$input = array($callRep, $imgsOrFiles);
			$rows = oslDAO::executePrepared($sql, $types, $input);
			// Loop.
			// while($loop = mysql_fetch_array($sql))
			foreach($rows as $loop)
			{
				$filesAttached = TRUE;
			}
		}
		// Return value.
		return $filesAttached;
	}

	// Get latest file number.
	static function getLatestFileNumber()
	{
		// Init.
		$now = date('YmdHis');
		// At the end return the latest file number.
		return $now;
	}

	// Output file types that may be uploaded.
	static function outputUploadableTypes($admin,$tabs)
	{
	 	// Open div.
		$list .= oslStructure::openDiv("divFileTypes",$tabs,"fileTypeCover");
		// Add number of tabs required.
	 	$tabStr = oslStructure::getRequiredTabs($tabs);
	 	// Get allowed file types.
		$array = oslFile::getAllowedFileTypes();
		// Loop.
		foreach($array as $key => $val)
		{
		 	// If in admin.
		 	if($admin == TRUE)
		 	{
				// Turn into link.
				$val = "<a href=\"index.php?content=7&&callFileType=$key\">$val</a>";
			}
			$list .= "$tabStr	<div class=\"fileType\">\n";
			$list .= "$tabStr		.$val";
			$list .= "\n$tabStr	</div>\n";
		}
	 	// If not in admin.
	 	if($admin == FALSE)
	 	{
			// Add header.
			$list = "$tabStr<p><strong>Permitted file types:</strong></p>\n".
					"$list";
		}
		// Close list.
	 	$list .= oslStructure::closeDiv("divFileTypes",$tabs);
		// Return list of available file types
		return $list;
	}

	// Get file type ID from a string.
	static function getAllowedFileTypeIDFromStr($str)
	{
		// Get.
		// $sql = oslDAO::executeQuery("SELECT fileTypeID
		// 							 FROM tblFileTypes
		// 							 WHERE fileType LIKE '$str'");

		$sql = "SELECT fileTypeID FROM tblFileTypes WHERE fileType LIKE ?";
		$types = "s";
		$input = array($str);
		$rows = oslDAO::executePrepared($sql, $types, $input);
		// Loop.
		// while($loop = mysql_fetch_array($sql))
		foreach($rows as $loop)
		{
		 	// Init.
		 	$fileTypeID = $loop["fileTypeID"];
		}
		// Return.
		return $fileTypeID;
	}

	// Get allowed file type array for upload.
	static function getAllowedFileTypes()
	{
	 	// Init.
	 	$allowed_file_type = array();
		// Get files.
		// $sql = oslDAO::executeQuery("SELECT *
		// 							 FROM tblFileTypes
		// 							 ORDER BY fileType");

		$sql = "SELECT * FROM tblFileTypes ORDER BY fileType";
		$types = "";
		$input = array();
		$rows = oslDAO::executePrepared($sql, $types, $input);
		// Build array of file types.
		// while($loop = mysqli_fetch_array($sql))
		foreach($rows as $loop)
		{
		 	// Init.
		 	$fileTypeID = $loop["fileTypeID"];
		 	$fileType = $loop["fileType"];
			$allowed_file_type[$fileTypeID] = $fileType;
		}
		// Return.
		return $allowed_file_type;
	}

	static function splitOnExtension($fname)
	{
		$parts = explode(".", $fname);
		$ext = array_pop($parts);
		if ($ext === NULL) {
			$ext = "";
		}
		return array(implode(".", $parts), $ext);
	}

	// Upload file.
	static function uploadFile($content,$step,$callRep,$addCommentTo)
	{
	 	// Get variables set elsewhere.
	 	global $upload_dir, $maxFileSize, $_FILES, $tnW, $tnH, $strongFilenameCleaning;

	 	// Set redirect URL.
		$url = "../index.php?content=$content&step=$step&callRep=$callRep&addCommentTo=$addCommentTo";

		// Check if the directory exists or not.
		if (!is_dir("../$upload_dir"))
		{
		   	// Directory does not exist error.
			oslMessages::addError("Internal error, the file uploads directory does not exist");
			return $url;
		}
		// Check if the directory is writable error.
		if (!is_writeable("../$upload_dir"))
		{
		   	// Directory not writeable.
			oslMessages::addError("Internal error, file uploads directory is currently not write-able.");
			return $url;
		}
		// Check if a file has been uploaded.
		if (is_uploaded_file($_FILES['filetoupload']['tmp_name']))
		{
			// Get the Size of the File.
		    $size = $_FILES['filetoupload']['size'];
			// Make sure that $size is less than maximum file size.
		    if ($size > oslApp::getAppInfo("appMaxFileSize"))
		    {
		    	// File too large error.
				oslMessages::addError("This file exceeds the maximum file size for this application.");
				return $url;
			}
			// Check file type. Explode the string to get the extension.
			$getFilename = $_FILES['filetoupload']['name'];
			$expFilename = oslFile::splitOnExtension($getFilename);
		    $getExt = strtolower($expFilename[1]);
			$getFilenameStr = $expFilename[0];
			$getFilenameStr = str_replace("'", "", $getFilenameStr);
			$getFilenameStr = str_replace(" ", "", $getFilenameStr);
			if ($strongFilenameCleaning) {
				$getFilenameStr = strtolower($getFilenameStr);
				$getFilenameStr = str_replace("_", "", $getFilenameStr);
			}
			$getFilenameStr = str_replace("/", "", $getFilenameStr);
			$getFilenameStr = str_replace("\\", "", $getFilenameStr);
			$getFilenameStr = str_replace(".", "", $getFilenameStr);
			$getFilenameStr = str_replace("+", "-", $getFilenameStr);


			if (($limit_file_type == "yes")
			   && (!in_array($getExt,$allowed_file_type)
				  && !in_array($getExt,$allowed_img_type)))
		    {
		    	// Wrong file type error.
				oslMessages::addError("This file type is not accepted for this application.");
		        return $url;
		    }
			// Build new filename.
		    $filename = $callRep."_".oslFile::getLatestFileNumber()."_".$getFilenameStr.".".$getExt;
			if ($strongFilenameCleaning) {
				$filename = strtolower($filename);
			}
			// Check if the file already exists.
		    if(file_exists($upload_dir.$filename))
			{
		    	// Filename already exists error.
				oslMessages::addError("Unable to upload file as the filename already exists.");
		    	return $url;
		    }
			// Move the file to the upload directory specified previously.
		    if (move_uploaded_file($_FILES['filetoupload']['tmp_name'],"../".$upload_dir.$filename))
			{
			 	// Get missing file parameters.
				$filesize = filesize("../".$upload_dir.$filename);
				$fileType = $getExt;
				$fileTypeID = oslFile::getAllowedFileTypeIDFromStr($fileType);
				$date_uploaded = date('Y-m-d H:i:s');
				// Add.
				// $upd = oslDAO::executeQuery("INSERT INTO tblFiles
				// 							 (reportFK, filename, fileTypeFK, date_uploaded, filesize)
				// 							 VALUES
				// 							 ($callRep, '$filename', '$fileTypeID', '$date_uploaded', $filesize)");

				$upd = "INSERT INTO tblFiles (reportFK, filename, fileTypeFK, date_uploaded, filesize)
								VALUES (?, ?, ?, ?, ?)";
				$types = "isisi";
				$input = array($callRep, $filename, $fileTypeID, $date_uploaded, $filesize);
				$rows = oslDAO::executePrepared($upd, $types, $input);
				// Create thumbnail.
				oslFile::createThumbnail("$filename",$tnW,$tnH);
			}
			else
		    {
		    	// General error.
					oslMessages::addError("Internal error, unable to move the attachment.");
					return $url;
		    }
		}
		// If there is no file re-route the user.
		else
		{
			// No file.
			oslMessages::addError("No file was selected for upload.");
			return $url;
		}
		// Return url.
		return $url;
	}

	// Delete individual file.
	static function deleteFile($callRep,$appDir,$upload_dir,$callFile)
	{
	 	// Get variables set elsewhere.
	 	global $tn_dir;

		// If a file number has been called.
		if($callFile != NULL && $callFile != 0)
		{
			// Init.
			$filenameWithPath = $appDir.$upload_dir.oslFile::getFileDetails($callFile,"filename");
			// Remove actual file.
      // print_r($filenameWithPath);
      // unlink($filenameWithPath);
      // die();
			if(unlink($filenameWithPath))
			{
			 	// If this file is an image, remove its thumbnail.
			 	if(oslFile::getFileDetails($callFile,"fileImage"))
			 	{
			 	 	// Set the path to the thumbnail.
					$tnWithPath = $appDir.$tn_dir."tn_".oslFile::getFileDetails($callFile,"filename");
					// Remove thumbnail.
					unlink($tnWithPath);
				}
				// Remove from database.
				// $sql = oslDAO::executeQuery("DELETE
				// 							 FROM tblFiles
				// 							 WHERE fileID=$callFile");

				$sql = "DELETE FROM tblFiles WHERE fileID=?";
				$types = "i";
				$input = array($callFile);
				$rows = oslDAO::executePrepared($sql, $types, $input);
			}
		}
	}

	// Delete all files attached to a report.
	static function deleteFiles($callRep,$upload_dir,$content,$appDir)
	{
		// If a report has been called.
		if($callRep != NULL && $callRep != 0)
		{
			// Get all files associated with this report.
			// $sql = oslDAO::executeQuery("SELECT *
			// 							 FROM tblFiles
			// 							 WHERE reportFK=$callRep");

			$sql = "SELECT * FROM tblFiles WHERE reportFK=?";
			$types = "i";
			$input = array($callRep);
			$rows = oslDAO::executePrepared($sql, $types, $input);
			// Loop.
			// while($loop = mysql_fetch_array($sql))
			foreach($rows as $loop)
			{
				// Init.
				$fileID = $loop["fileID"];

				// Delete file.
				oslFile::deleteFile($callRep,$appDir,$upload_dir,$fileID);
			}
		}
	}

	// Create a thumbnail from a bitmap file.
	static function imagecreatefrombmp($filename)
	{
		 	// Open file in binary mode.
			if (! $f1 = fopen($filename,"rb")) return FALSE;
			 //1 : Chargement des ent?tes FICHIER
		   $FILE = unpack("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread($f1,14));
		   if ($FILE['file_type'] != 19778) return FALSE;

		 //2 : Chargement des ent?tes BMP
		   $BMP = unpack('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel'.
		                 '/Vcompression/Vsize_bitmap/Vhoriz_resolution'.
		                 '/Vvert_resolution/Vcolors_used/Vcolors_important', fread($f1,40));
		   $BMP['colors'] = pow(2,$BMP['bits_per_pixel']);
		   if ($BMP['size_bitmap'] == 0) $BMP['size_bitmap'] = $FILE['file_size'] - $FILE['bitmap_offset'];
		   $BMP['bytes_per_pixel'] = $BMP['bits_per_pixel']/8;
		   $BMP['bytes_per_pixel2'] = ceil($BMP['bytes_per_pixel']);
		   $BMP['decal'] = ($BMP['width']*$BMP['bytes_per_pixel']/4);
		   $BMP['decal'] -= floor($BMP['width']*$BMP['bytes_per_pixel']/4);
		   $BMP['decal'] = 4-(4*$BMP['decal']);
		   if ($BMP['decal'] == 4) $BMP['decal'] = 0;

		 //3 : Chargement des couleurs de la palette
			$PALETTE = array();
			if ($BMP['colors'] < 16777216 && $BMP['colors'] != 65536)
			{
			$PALETTE = unpack('V'.$BMP['colors'], fread($f1,$BMP['colors']*4));
			#nei file a 16bit manca la palette,
			}
		 //4 : Cr?ation de l'image
		   $IMG = fread($f1,$BMP['size_bitmap']);
		   $VIDE = chr(0);

		   $res = imagecreatetruecolor($BMP['width'],$BMP['height']);
		   $P = 0;
		   $Y = $BMP['height']-1;
		   while ($Y >= 0)
		   {
		    $X=0;
		    while ($X < $BMP['width'])
		    {
		     if ($BMP['bits_per_pixel'] == 24)
		        $COLOR = unpack("V",substr($IMG,$P,3).$VIDE);
		     elseif ($BMP['bits_per_pixel'] == 16)
			{
			$COLOR = unpack("v",substr($IMG,$P,2));
			$blue  = (($COLOR[1] & 0x001f) << 3) + 7;
			$green = (($COLOR[1] & 0x03e0) >> 2) + 7;
			$red   = (($COLOR[1] & 0xfc00) >> 7) + 7;
			$COLOR[1] = $red * 65536 + $green * 256 + $blue;
			}
		     elseif ($BMP['bits_per_pixel'] == 8)
		     {
		        $COLOR = unpack("n",$VIDE.substr($IMG,$P,1));
		        $COLOR[1] = $PALETTE[$COLOR[1]+1];
		     }
		     elseif ($BMP['bits_per_pixel'] == 4)
		     {
		        $COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
		        if (($P*2)%2 == 0) $COLOR[1] = ($COLOR[1] >> 4) ; else $COLOR[1] = ($COLOR[1] & 0x0F);
		        $COLOR[1] = $PALETTE[$COLOR[1]+1];
		     }
		     elseif ($BMP['bits_per_pixel'] == 1)
		     {
		        $COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
		        if     (($P*8)%8 == 0) $COLOR[1] =  $COLOR[1]        >>7;
		        elseif (($P*8)%8 == 1) $COLOR[1] = ($COLOR[1] & 0x40)>>6;
		        elseif (($P*8)%8 == 2) $COLOR[1] = ($COLOR[1] & 0x20)>>5;
		        elseif (($P*8)%8 == 3) $COLOR[1] = ($COLOR[1] & 0x10)>>4;
		        elseif (($P*8)%8 == 4) $COLOR[1] = ($COLOR[1] & 0x8)>>3;
		        elseif (($P*8)%8 == 5) $COLOR[1] = ($COLOR[1] & 0x4)>>2;
		        elseif (($P*8)%8 == 6) $COLOR[1] = ($COLOR[1] & 0x2)>>1;
		        elseif (($P*8)%8 == 7) $COLOR[1] = ($COLOR[1] & 0x1);
		        $COLOR[1] = $PALETTE[$COLOR[1]+1];
		     }
		     else
		        return FALSE;
		     imagesetpixel($res,$X,$Y,$COLOR[1]);
		     $X++;
		     $P += $BMP['bytes_per_pixel'];
		    }
		    $Y--;
		    $P+=$BMP['decal'];
		   }

		 	//Fermeture du fichier
		   	fclose($f1);

		 	return $res;
	}

	// Given a image file name return the path to thumbnail file
	// that contains an image stating that no preview is available.
	// This is inteneded to be used for files that are too large to
	// resample or cannot otherwise have a regular thumbnail generated
	// for them.
	// Returns the path to a thumbnail file or FALSE on error (typically
	// an unknown file type)
	static function getImageNoPreviewFile($name) {
		$ext = "";
		if (preg_match('/jpg|jpeg/i',$name))
		{
		    $ext = "jpg";
		}
		elseif (preg_match('/png/i',$name))
		{
			$ext = "png";
		}
		elseif (preg_match('/gif/i',$name))
		{
			$ext = "gif";
		}
		elseif (preg_match('/bmp/i',$name))
		{
			$ext = "bmp";
		} else {
			return FALSE;
		}
		$output =  "../images/nopreview.$ext";
		return $output;
}

	// Create a thumbnail.
	static function createThumbnail($srcName,$new_w,$new_h)
	{
	 	// Get file type.
		$system=explode('.',$srcName);
		// If thumbnail-able.
		if (oslFile::checkIfFileTypeIsImg($system[1]) == 1)
		{
			echo "Thumbnailable!";
		 	// Get variables set elsewhere.
		 	global $upload_dir, $tn_dir;
		 	// Tag upload directory to before name of source file.
		 	$name = "../".$upload_dir.$srcName;
		 	// Tag thumbnail dir to before name of output file.
		 	$filename = "../".$tn_dir."tn_".$srcName;

		 	// get the dimensions
		 	$sdata = getimagesize($name);
		 	if ($sdata === FALSE)
			{
				// we cannot handle this input file
				$src = oslFile::getImageNoPreviewFile($system[1]);
				if ($src !== FALSE) {
				    copy($src, $filename);
				}
		 		return;
		 	}
		 	// get the width/height
		 	$old_x = $sdata[0];
		 	$old_y = $sdata[1];
			// guess the ram requirements for 24bit color with
			// a fudge factor
		 	$ram_required = $old_x * $old_y * 3 * 2;
		 	// memory_limit == -1 means unlimited, if this is set just go ahead
			// otherwise check to see if the image will fit.
			$mem_limit = ini_get('memory_limit');
			if ($mem_limit > 0) {
				if (is_string($mem_limit)) {
						list($mem_limit, $scale) = sscanf($mem_limit, "%d%s");
						if ($scale == "K" || $scale == "k") {
								$mem_limit = $mem_limit*1024;
						}
						if ($scale == "M" || $scale == "m") {
								$mem_limit = $mem_limit*1024*1024;
						} elseif ($scale == "G" || $scale == "g") {
								$mem_limit = $mem_limit*1024*1024;
						}
				}
			}
			$mem_usage = memory_get_usage();

				//$f = fopen("/tmp/phpmem.txt", "at");
				//if ($f === FALSE) {
				//		die("Unable to open log file");
				//}
				//$delta = "-";
				//$remaining = "-";
				//if ($mem_limit != -1) {
				//	$delta = $mem_limit - $mem_usage;
				//	if ($ram_required > $delta) {
				//		$remaining = "Negative";
				//	} else {
				//		$remaining = $mem_limit - $mem_usage - $ram_required;
				//	}
				//}
				//$mpixels = ($old_x * $old_y)/1000000;
				//fwrite($f, "used: $mem_usage\trequired: $ram_required\tlimit: $mem_limit\tdelta: $delta\tremaining: $ram_required\t($old_x x $old_y) $mpixels MP\n");
				//fclose($f);

			if (($mem_limit - $mem_usage < $ram_required) && $mem_limit >= 0) {
				$src = oslFile::getImageNoPreviewFile($system[1]);
				if ($src !== FALSE) {
				    copy($src, $filename);
				}
				return;
			}

			// Begin with image creation.
			if (preg_match('/jpg|jpeg/i',$system[1]))
			{
				$src_img=imagecreatefromjpeg($name);
			}
			elseif (preg_match('/png/i',$system[1]))
			{
				$src_img=imagecreatefrompng($name);
			}
			elseif (preg_match('/gif/i',$system[1]))
			{
				$src_img=imagecreatefromgif($name);
			}
			elseif (preg_match('/bmp/i',$system[1]))
			{
				$src_img=oslFile::imagecreatefrombmp($name);
			} else {
				// we don't know how to handle the file at this point
				return;
			}
			// Get existing file dimensions.
			$old_x=imageSX($src_img);
			$old_y=imageSY($src_img);
			if ($old_x > $old_y)
			{
				$thumb_w=$new_w;
				$thumb_h=$old_y*($new_h/$old_x);
			}
			elseif ($old_x < $old_y)
			{
				$thumb_w=$old_x*($new_w/$old_y);
				$thumb_h=$new_h;
			}
			elseif ($old_x == $old_y)
			{
				$thumb_w=$new_w;
				$thumb_h=$new_h;
			}
			// Create thumbnail.
			$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
			imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
			// Set image type.
			if (preg_match("/png/i",$system[1]) || preg_match("/bmp/i",$system[1]))
			{
				imagepng($dst_img,$filename);
			}
			elseif (preg_match("/jpg|jpeg/i",$system[1]))
			{
				imagejpeg($dst_img,$filename);
			}
			elseif (preg_match("/gif/i",$system[1]))
			{
				imagegif($dst_img,$filename);
			}
			// Destroy.
			imagedestroy($dst_img);
			imagedestroy($src_img);
		}
	}


}

?>
