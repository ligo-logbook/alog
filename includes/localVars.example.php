<?php
  $SETTINGS_DOC_ROOT = "/var/www/html/aLOG";

  $callDB = "osl_production";
  $SETTING_DB_HOST = "localhost";

  // read-write MySQL/MariaDB user account
  $SETTING_DB_USER = "osl_production";
  $SETTING_DB_PASSWORD = "really_secure_password";

  // read-only MySQL/MariaDB user account
  // used for all queries when REMOTE_USER is empty (not logged in)
  // should be granted only SELECT on $callDB.* tables
  $SETTING_DB_READONLY_USER = "osl_readonly";
  $SETTING_DB_READONLY_PASSWORD = "OTHER_really_secure_password";

  // displayed above the text entry box on the report draft screen
  // HTML may be used in this variable as it is echo'ed directly into the page
  // example usage: reminding reporters about posting policy
  $editorNote = "";

  // big on/off switch for L-Mail
  // FALSE: no mail
  // TRUE: send mail for L-Mail subscriptions
  $sendLMail = FALSE;

  // array log usernames that will be considered as automated-posting
  // "robots" - this is for filtering in searches
  $robotList  = array();
?>
