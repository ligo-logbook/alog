<?php
/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/

///////////////////////////////////
// OSLogbook Data Access Object //
/////////////////////////////////

class oslDAO
{
	// Execute a prepared statement
	static function executePrepared($sql, $types, $input)
	{
		global $dbi;

		ob_start();
		var_dump($input);
		$input_txt = ob_get_contents();
		ob_end_clean();
		// echo "<!-- SQL Query = \"$sql\" \"$types\" \"$input_txt\"-->\n";
		$entries = count($input);
		if (strlen($types) !== $entries) {
			die ("Unable to build SQL statement, parameter count mismatch");
		}
		$stmt = mysqli_prepare($dbi, $sql);
		if (!$stmt) {
			die("Invalid SQL");
		}
		if (count($input) > 0) {
			$inp = array(&$types);
			foreach ($input as $key => $value) {
				$inp[] = &$input[$key];
			}
			call_user_func_array(array($stmt, 'bind_param'), $inp);
		}
		if (mysqli_stmt_execute($stmt) === FALSE) {
			die("SQL ERROR");
		}
		if (mysqli_stmt_store_result($stmt) === FALSE) {
			die("Unable to retreive SQL results");
		}
		if (mysqli_stmt_num_rows($stmt) < 1) {
			// echo "<!-- SQL Results = no results, returning empty array -->\n";
			return array();
		}
		$params = array();
		$data = array();
		$meta = mysqli_stmt_result_metadata($stmt);
		while ($field = mysqli_fetch_field($meta)) {
			$params[] = &$data[$field->name];
		}

		call_user_func_array(array($stmt, 'bind_result'), $params);

		$result = array();
		while (mysqli_stmt_fetch($stmt)) {
			$c = array();
			foreach ($data as $key => $val) {
				$c[$key] = $val;
			}
			$result[] = $c;
		}
		mysqli_stmt_close($stmt);
		ob_start();
		var_dump($result);
		$result_txt = ob_get_contents();
		ob_end_clean();
		// echo "<!-- SQL Results = \"$result_txt\" -->\n";
		return $result;
	}

	// This is a replacement for mysql_result, given a array of sql results
	// return the given column ($field) return the value of that column in row 0
	// returns FALSE if $rows has no entries.
	static function getFirstResultEntry($rows, $field)
	{
		if (count($rows) > 0) {
			return $rows[0][$field];
		}
		return FALSE;
	}

	// Connect to server.
	static function serverConnect($server,$username,$password)
	{
		// Connect to server.
		$db = mysqli_connect("$server", "$username", "$password");
		if (!$db)
		{
			die ("Can't connect to server. " . mysqli_connect_error());
		}
		else
		{
			return $db;
		}
	}
	// Connect to database.
	static function dbConnect($db,$callDB)
	{
		// Select database.
		$getdb = mysqli_select_db($db, "$callDB");
		// Print error message if connection is unsuccessful.
		if (!$getdb)
		{
			die ("Can't get database. " . mysqli_error($db));
		}
	}
	// Connect to a database passing authentication details.
	static function serverEDbConnect($server,$user,$pass,$callDB)
	{
		// Connect to server.
		$db = mysqli_connect("$server", "$user", "$pass");
		if (!$db)
		{
			die ("Can't connect to server. " . mysqli_error($db));
		}
		else
		{
			// Select database.
			$getdb = mysqli_select_db($db, "$callDB");
			// Print error message if connection is unsuccessful.
			if (!$getdb)
			{
				die ("Can't get database. " . mysqli_error($db));
			}
			else
			{
				return $db;
			}
		}
	}
	// Close connection to server.
	static function dbClose($db)
	{
	 	// Close.
		mysqli_close($db);
	}

	// Sanitize a string for HTML output.
	// This is done by replacing & with &amp;, < with &lt;, and > with &gt;
	static function sanitizeForHTML($text)
	{
		if (!is_string($text)) {
			if (is_array($text)) {
				$tmp = array();
				foreach ($text as $k => $v) {
					$tmp[$k] = "".$v;
				}
				$text = $tmp;
			} else {
				$text = "".$text;
			}
		}
		return str_replace(array("&", "<", ">"), array("&amp;", "&lt;", "&gt;"), $text);
	}

	// Filter out characters un-wanted by MySQL.
	static function filterForMySQL($var)
	{
	 	// Filter
		$var_mod = str_replace("'", "&#39;", $var);
		$var_mod = str_replace("`", "&#39;", $var_mod);
		// Return.
		return $var_mod;
	}

 	// Filter a posted array.
 	static function filterArray($array)
 	{
		// Loop through array.
		foreach($array as $key => $val)
		{
			// Check for button coordinates.
			if(preg_match("/_x/",$key)
			|| preg_match("/_y/",$key))
			{
			 	// Remove from array.
				unset($array[$key]);
			}
		}
		// Return array.
		return $array;
	}

	// Insert tag
	static function insertTag($userID,$callTag,$tag)
	{
	 	// Init.
	 	$url = "../index.php?content=7";
	 	// If the user is Admin.
	 	if(oslUser::checkIfAdmin($userID))
	 	{
			// If a new tag is being added.
			if($callTag == NULL)
			{
				// Insert.
				// $ins = oslDAO::executeQuery("INSERT INTO tblTags
				// 							 (tag)
				// 							 VALUES
				// 							 ('$tag')");
				$sql = "INSERT INTO tblTags (tag) VALUES (?)";
				$types = "s";
				$input = array($tag);
				$ins = oslDAO::executePrepared($sql, $types, $input);
			}
			// Otherwise, a section is being updated.
			else
			{
				// Update.
				// $upd = oslDAO::executeQuery("UPDATE tblTags
				// 							 SET tag = '$callTag'
				// 							 WHERE tagID = $callTag");
				$sql = "UPDATE tblTags SET tag = ? WHERE tagID = ?";
				$types = "si";
				$input = array($callTag, $callTag);
				$upd = oslDAO::executePrepared($sql, $types, $input);
			}
		}
		// Return.
		return $url;
	}

	// Delete tag.
	static function deleteTag($userID,$callTag)
	{
	 	// Init.
	 	$url = "../index.php?content=7";
	 	// If the user is Admin.
	 	if(oslUser::checkIfAdmin($userID))
	 	{
			// Delete.
			// $del = oslDAO::executeQuery("DELETE FROM tblTags
			// 							 WHERE tagID = $callTag");

			$sql = "DELETE FROM tblTags WHERE tagID = ?";
			$types = "i";
			$input = array($callTag);
			$del = oslDAO::executePrepared($sql, $types, $input);

			// $del = oslDAO::executeQuery("DELETE FROM tblReportTags
			// 							where tagFK = $callTag");

			$sql = "DELETE FROM tblReportTags WHERE tagFK = ?";
			$types = "i";
			$input = array($callTag);
			$del = oslDAO::executePrepared($sql, $types, $input);
		}
		// Return.
		return $url;
	}

	static function _getTags($reportID, $tagType)
	{
		$tags = array();
		$args = array();
		$types = "i";
		if ($reportID === NULL) {
				$query = "SELECT tagID, tag FROM tblTags WHERE type = ?";
				$args[] = $tagType;
		} else {
				$query = "SELECT tagID, tag FROM tblTags, tblReportTags WHERE reportFK=? AND tagFK=tagID AND tblTags.type=?";
				$types .= "i";
				$args = array($reportID, $tagType);
		}
		$query .= " ORDER BY tag";
		$rows = oslDAO::executePrepared($query, $types, $args);
		foreach ($rows as $row) {
			$tags[$row['tagID']] = $row['tag'];
		}
		//if ($get !== FALSE) {
		//		while ($loop = mysql_fetch_array($get)) {
		//				$tags[$loop['tagID']] = $loop['tag'];
		//		}
		//}
		return $tags;
	}
	static function getAllTags()
	{
		return oslDAO::_getTags(NULL, 0);
	}
	static function getReportTags($reportID)
	{
		if ($reportID === NULL || $reportID === 0) {
				return array();
		}
		return oslDAO::_getTags($reportID, 0);
	}
	static function getAllAnnotations()
	{
		return oslDAO::_getTags(NULL, 1);
	}
	static function getReportAnnotations($reportID)
	{
		return oslDAO::_getTags($reportID, 1);
	}
	static function setReportTags($userID, $reportID, $tagIDList)
	{
		oslDAO::executePrepared("DELETE FROM tblReportTags WHERE reportFK=?", "i", array($reportID));
		//oslDAO::executeQuery("DELETE FROM tblReportTags WHERE reportFK=$reportID");
		if (count($tagIDList)>0) {
				$query = "INSERT INTO tblReportTags (reportFK, tagFK) VALUES ";
				$prefix = "";
				$types = "";
				$values = array();
				foreach ($tagIDList as $tagID) {
						$types .= "ii";
						$values[] = $reportID;
						$values[] = $tagID;
						$query .= "$prefix(?, ?)";
						$prefix = ", ";
				}
				oslDAO::executePrepared($query, $types, $values);
				//oslDAO::executeQuery($query);
		}
	}

    // return the list of tagIDs for tags with text similar to $text.
	static function getTagsIDsMatchingText($text)
	{
		$ids = array();
		//$query = "SELECT tagID FROM tblTags WHERE tag LIKE '%". oslDAO::filterForMySQL($text)."%' AND type = 0";
		$query = "SELECT tagID FROM tblTags WHERE tag LIKE ? AND type = 0";
		//$get = oslDAO::executeQuery($query);
		$rows = oslDAO::executePrepared($query, "s", array("%$text%"));
		foreach ($rows as $k => $row) {
			$ids[] = "".$row['tagID'];
		}
		return $ids;
	}

	// Given a count of values return a string suitable for use in a sql query
	// in an 'in' statement (?, ?, ?, ? ...)
	static function createListForSelect($count)
	{
		$text = "(";
		$prepend = "";
		for ($i = 0; $i < $count; $i++) {
				$text .= $prepend;
				$text .= "?";
				$prepend = ", ";
		}
		return $text.")";
	}

	// Insert section
	static function insertSection($userID,$callSection,$adSectionName)
	{
	 	// Init.
	 	$url = "../index.php?content=7";
	 	// If the user is Admin.
	 	if(oslUser::checkIfAdmin($userID))
	 	{
			// If a new section is being added.
			if($callSection == NULL)
			{
				// Get new colour for this section.
				$adSectionColour = oslStructure::setNewSectionColour();
				// Insert.
				// $ins = oslDAO::executeQuery("INSERT INTO tblSections
				// 							 (sectionName,sectionColour)
				// 							 VALUES
				// 							 ('$adSectionName','$adSectionColour')");

				$sql = "INSERT INTO tblSections (sectionName,sectionColour)
								VALUES (?,'$adSectionColour')";
				$types = "s";
				$input = array($adSectionName);
				$ins = oslDAO::executePrepared($sql, $types, $input);
				// Get new sectionID.
				$newSectionID = oslContent::getSectionID();
				// Insert new task.
				oslDAO::insertNewTask($newSectionID,"General");
			}
			// Otherwise, a section is being updated.
			else
			{
				// Update.
				// $upd = oslDAO::executeQuery("UPDATE tblSections
				// 							 SET sectionName = '$adSectionName'
				// 							 WHERE sectionID = $callSection");

				$sql = "UPDATE tblSections SET sectionName = ? WHERE sectionID = ?";
				$types = "si";
				$input = array($adSectionName, $callSection);
				$upd = oslDAO::executePrepared($sql, $types, $input);
			}
		}
		// Return.
		return $url;
	}

	// Delete section.
	static function deleteSection($userID,$callSection)
	{
	 	// Init.
	 	$url = "../index.php?content=7";
	 	// If the user is Admin.
	 	if(oslUser::checkIfAdmin($userID))
	 	{
			// Delete.
			// $del = oslDAO::executeQuery("DELETE FROM tblSections
			// 							 WHERE sectionID = $callSection");
			$sql = "DELETE FROM tblSections WHERE sectionID = ?";
			$del = oslDAO::executePrepared($sql, "i", array($callSection));
			// Delete all tasks associated to this section.
			oslDAO::deleteTasks($callSection);
		}
		// Return.
		return $url;
	}

	// Edit task.
	static function editTask($userID,$edSectionID,$adTaskName,$reportTask)
	{
	 	// Init.
	 	$url = "../index.php?content=7";
	 	// If the user is Admin.
	 	if(oslUser::checkIfAdmin($userID))
	 	{
			// Check if new task or already existing.
			if($reportTask == NULL)
			{
				// Insert.
				oslDAO::insertNewTask($edSectionID,$adTaskName);
			}
			// Otherwise, update.
			else
			{
				// $upd = oslDAO::executeQuery("UPDATE tblTasks
				// 							 SET taskName='$adTaskName'
				// 							 WHERE taskID=$reportTask");
				$sql = "UPDATE tblTasks SET taskName=? WHERE taskID=?";
				$upd = oslDAO::executePrepared($sql, "si", array($adTaskName, $reportTask));
			}
		}
		// Return.
		return $url;
	}

	// Insert new task.
	static function insertNewTask($sectionID,$taskName)
	{
		// $ins = oslDAO::executeQuery("INSERT INTO tblTasks
		// 							 (sectionFK,taskName)
		// 							 VALUES
		// 							 ($sectionID,'$taskName')");
		$sql = "INSERT INTO tblTasks (sectionFK,taskName) VALUES (?,?)";
		$ins = oslDAO::executePrepared($sql, "is", array($sectionID, $taskName));
	}

	// Delete a single task.
	static function deleteTask($userID,$callTask)
	{
	 	// Init.
	 	$url = "../index.php?content=7";
	 	// If the user is Admin.
	 	if(oslUser::checkIfAdmin($userID))
	 	{
			// $del = oslDAO::executeQuery("DELETE FROM tblTasks
			// 							 WHERE taskID=$callTask");
			$sql = "DELETE FROM tblTasks WHERE taskID=?";
			$del = oslDAO::executePrepared($sql, "i", array($callTask));
		}
		// Return.
		return $url;
	}

	// Delete all tasks associated to a section.
	static function deleteTasks($sectionID)
	{
		// $del = oslDAO::executeQuery("DELETE FROM tblTasks
		// 							 WHERE sectionFK=$sectionID");
		$sql = "DELETE FROM tblTasks WHERE sectionFK=?";
		$del = oslDAO::executePrepared($sql, "i", array($sectionID));
	}

	// Get Edit button.
	static function canEditReport($callRep,$userID,$preview)
	{
		// get information about the report
		$authorFK = oslContent::getReportInfo("authorFK", $callRep);
		$dateStr = oslContent::getReportInfo("dateAdded", $callRep);
		$postConfirmed = oslContent::getReportInfo("postConfirmed", $callRep);
		$lastEditableTime = strtotime("+1 day", strtotime($dateStr));
		$curTime = time();
		//echo "<!-- lastEditableTime = $lastEditableTime curTime = $curTime diff = ".($curTime < $lastEditableTime ? "edit" : "")." -->\n";
		//echo "<!-- authorFK = $authorFK userID = $userID -->\n";

		if (oslUser::checkIfAdmin($userID)
			|| ($preview != NULL && $authorFK == $userID && $preview == $callRep)
			|| ($authorFK == $userID && ($curTime < $lastEditableTime || $postConfirmed == "0")))
		{
				return TRUE;
		}
		// Return.
		return FALSE;
	}

	static function setLastCommentDateOnParents($callRep, $newDate) {
		if ($callRep == NULL || $callRep == 0) {
			return;
		}
		// $upd = oslDAO::executeQuery("UPDATE tblReports
		// 							SET lastCommentDate = '$newDate'
		// 							WHERE reportID=$callRep AND lastCommentDate < '$newDate'");

		$sql = "UPDATE tblReports SET lastCommentDate = ?  WHERE reportID=? AND lastCommentDate < ?";
		$upd = oslDAO::executePrepared($sql, "sis", array($newDate, $callRep, $newDate));
		// $count = mysqli_affected_rows($upd);
		// if ($count === FALSE || $count === 0) {
		if ($upd === FALSE || $upd === 0) {
			return;
		}
		// $res = oslDAO::executeQuery("SELECT parentFK FROM tblReports WHERE reportID=$callRep");

		$sql = "SELECT parentFK FROM tblReports WHERE reportID=?";
		$res = oslDAO::executePrepared($sql, "i", array($callRep));
		// $row = mysqli_fetch_array($res);
		// if ($row !== FALSE) {
		if ($res !== FALSE) {
			oslDAO::setLastCommentDateOnParents($res['parentFK'], $newDate);
		}
	}

	// Return an array of "name" -> tagID values for system tags/annotations that are available
	static function getAvailableAnnotationList()
	{
		$tags = array();

		// $res = oslDAO::executeQuery("SELECT tagID, tag FROM tblTags WHERE type = 1");

		$sql = "SELECT tagID, tag FROM tblTags WHERE type = 1";
		$res = oslDAO::executePrepared($sql, "", array());
		if ($res === FALSE) {
			return $tags;
		}
		foreach ($res as $loop) {
		// while (($row = mysqli_fetch_array($res)) !== FALSE) {
			$tags[$loop["tag"]] = $loop["tagID"];
			// $tags[$row["tag"]] = $row["tagID"];
		}
		return $tags;
	}

	static function doAnnotateImages($availableTags, $tagList, $reportText)
	{
		if (array_key_exists("sys:HasImages", $availableTags)) {
			if (preg_match("/<img.*src=/i", $reportText)) {
				$tagList[] = $availableTags["sys:HasImages"];
			}
		}
		return $tagList;
	}

	static function doAnnotateAttachments($availableTags, $tagList, $callRep)
	{
		if (array_key_exists("sys:HasAttachments", $availableTags)) {
			// $cnt = oslDAO::executeQuery("SELECT count(*) as attachmentCnt FROM tblFiles WHERE reportFK=$callRep");

			$sql = "SELECT count(*) as attachmentCnt FROM tblFiles WHERE reportFK=?";
			$cnt = oslDAO::executePrepared($sql, "i", array($callRep));
			if ($cnt === FALSE) {
				return $tagList;
			}
			// $row = mysqli_fetch_array($cnt);
			if ($cnt === FALSE) {
				return $tagList;
			}
			// if ($row['attachmentCnt'] > 0) {
			if ($cnt['attachmentCnt'] > 0) {
				$tagList[] = $availableTags["sys:HasAttachments"];
			}
		}
		return $tagList;
	}

	static function doAnnotateRobot($availableTags, $tagList, $reportAuthors)
	{
		global $robotList;

		echo "<!-- annototate robot ".print_r($robotList, TRUE)." -->\n";

		if (array_key_exists("sys:Robot", $availableTags)) {
			echo "<!-- has robot tag -->\n";
			foreach ($robotList as $value) {
				echo "<!-- comparing $reportAuthors to $value -->\n";
				if ($reportAuthors === $value) {
					echo "<!-- match -->\n";
					$tagList[] = $availableTags["sys:Robot"];
					return $tagList;
				}
			}
		}
		return $tagList;
	}

	// Given a report and the current list of tags add system/annotation tags based on attributes of the report
	// returns the new tagList
	static function getAnnotatedTagList($callRep, $userID, $reportAuthors, $reportText, $tagList)
	{
		$availableTags = oslDAO::getAvailableAnnotationList();

		$tagList = oslDAO::doAnnotateImages($availableTags, $tagList, $reportText);
		$tagList = oslDAO::doAnnotateAttachments($availableTags, $tagList, $callRep);
		$tagList = oslDAO::doAnnotateRobot($availableTags, $tagList, $reportAuthors);
		return $tagList;
	}

	// Save a report.
	static function saveReport($callRep,$userID,$nextStep,$addCommentTo,$appURL)
	{
	 	// Get variables from set elsewhere.
	 	global $reportTitle,$reportText,$reportTask,$editorInstance,$reportAuthors,$now,$tagList;
		// If the user is posting directly to Logbook then confirm.
	 	$postStatus = oslContent::getPostStatus($nextStep);
	 	// Update editor instance to be used.
	 	oslUser::updateEdInst($userID);
		// If a title and authors have been sent then update.
		if($reportTitle != NULL)
		{
		    $updateAllowed = TRUE;
			$lastCommentDate = $now;

			// These could be null, change them to default values if they are.
			$addCommentToVal =  $addCommentTo;
			if ($addCommentToVal === NULL) {
				$addCommentToVal = 0;
			}
			$editorInstanceVal = $editorInstance;
			if ($editorInstanceVal === NULL) {
				$editorInstanceVal = 1;
			}

		 	// If the report does not yet exist in the database insert it.
		 	if($callRep == NULL)
		 	{
				// Insert report.
				// $ins = oslDAO::executeQuery("INSERT INTO tblReports
				// 				 			 (reportTitle,reportText,taskFK,authorFK,authorNames,parentFK,postConfirmed,editorFK,dateAdded,lastCommentDate)
				// 							 VALUES
				// 				 			 ('$reportTitle','$reportText',$reportTask,$userID,'$reportAuthors','$addCommentTo','$postStatus','$editorInstance','$now','$now')");

				$sql = "INSERT INTO tblReports
								(reportTitle,reportText,taskFK,authorFK,authorNames,parentFK,postConfirmed,editorFK,dateAdded,lastCommentDate)
								VALUES (?,?,?,?,?,?,?,?,?,?)";
				$types = "sssisiiiss";
				$input = array($reportTitle,$reportText,$reportTask,$userID,$reportAuthors,$addCommentToVal,$postStatus,$editorInstanceVal,$now,$now);
				$ins = oslDAO::executePrepared($sql, $types, $input);
				// Get new ID.
				$callRep = oslContent::getNewReportID($userID);
			}
			// Otherwise update it.
			else
			{
				if (oslDAO::canEditReport($callRep, $userID, NULL)) {
						// If this is being edited after the event by an administrator, make sure that the authorFK and dateAdded are not changed.
						if(oslContent::getReportInfo("postConfirmed",$callRep) == TRUE)
						{
							$authorFK = oslContent::getReportInfo("authorFK",$callRep);
							$dateAdded = oslContent::getReportInfo("dateAdded",$callRep);
							$lastCommentDate = oslContent::getReportInfo("lastCommentDate",$callRep);
						}
						else
						{
							$authorFK = $userID;
							$dateAdded = $now;
							$lastCommentDate = $now;
						}
						// Update report.
						// $upd = oslDAO::executeQuery("UPDATE tblReports
						// 							 SET reportTitle='$reportTitle',reportText='$reportText',taskFK=$reportTask,authorFK=$authorFK,parentFK='$addCommentTo',postConfirmed=$postStatus,editorFK='$editorInstance',dateAdded='$dateAdded',lastCommentDate='$lastCommentDate'
						// 							 WHERE reportID=$callRep");
						$sql = "UPDATE tblReports
										SET reportTitle=?,reportText=?,taskFK=?,authorFK=?,parentFK=?,postConfirmed=?,editorFK=?,dateAdded=?,lastCommentDate=?
										WHERE reportID=?";
						$types = "ssiiiiissi";
						$input = array($reportTitle, $reportText, $reportTask, $authorFK, $addCommentToVal, $postStatus, $editorInstanceVal, $dateAdded, $lastCommentDate, $callRep);
						$upd = oslDAO::executePrepared($sql, $types, $input);
				} else {
						$updateAllowed = FALSE;
				}
			}
			if ($updateAllowed) {
				oslDAO::setLastCommentDateOnParents($addCommentTo, $lastCommentDate);
				oslDAO::setReportTags($userID, $callRep, oslDAO::getAnnotatedTagList($callRep, $userID, $reportAuthors, $reportText, $tagList));
			}
		}
		// Re-route accordingly.
		if($nextStep == 3)
		{
			// Send mail notification and then re-route.
			oslUser::sendMail($callRep,$reportTask,$appURL);
			$url = "../index.php";
		}
		else
		{
			// Saved to Draft - Return to report.
			if($nextStep == 1 || $nextStep == 6)
			{
				$url = "../index.php?content=3&callRep=$callRep&addCommentTo=$addCommentTo";
			}
			// Adding images. Send to correct page.
			elseif($nextStep == 2)
			{
				$url = "../index.php?content=3&step=1&callRep=$callRep&addCommentTo=$addCommentTo";
			}
			// Previewing. Go to homepage and output preview.
			elseif($nextStep == 5)
			{
				$url = "../index.php?preview=$callRep&callRep=$callRep&addCommentTo=$addCommentTo";
			}
		}
		// Return url.
		return $url;
	}

 	// Delete report.
 	static function deleteReport($callRep,$userID,$nextStep,$upload_dir,$content,$appDir)
 	{
 	 	// If a report has been called and the user is either admin or the user is author and the report has not been confirmed.
 	 	if($callRep != NULL && $callRep != 0
		  && (oslUser::checkIfAdmin($userID)
		     || (oslContent::getReportInfo("authorFK",$callRep) == $userID
			    && oslContent::getReportInfo("postConfirmed",$callRep) == 0)))
 	 	{
		    // delete all tags on the report first
		  oslDAO::setReportTags($userID, $callRep, array());
		 	// $del = oslDAO::executeQuery("DELETE FROM tblReports
			//  							 WHERE reportID=$callRep");
			$sql = "DELETE FROM tblReports WHERE reportID=?";
			$del = oslDAO::executePrepared($sql, "i", array($callRep));
			// Delete all related files.
			oslFile::deleteFiles($callRep,$upload_dir,$content,$appDir);
			// Set next step.
			$nextStep = 1;
			// If deleting from drafts, return to that section.
			if($content == 6)
			{
				$nextStep = 6;
			}
			// Set re-direct.
			$url = "../index.php?content=$nextStep";
		}
		// Return.
		return $url;
	}
}

?>
