<?php
/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/

///////////////////////////////////
// OSLogbook content management //
/////////////////////////////////

class oslContent
{
	// Output content.
	static function outputContent($callContent,$userID,$callUser)
	{
		// If this Logbook is a closed application, only open it if the user has logged-in.
		if(oslApp::getAppInfo("appOpen") == 0 && $userID == NULL)
		{
			echo "<p class=\"center\">This is an internal application. You need to log-in before you can use it.</p>\n";
		}
		// Otherwise, open it to everyone.
		else
		{
			oslContent::outputMessages();
 			// Output main reports.
			if($callContent == 1)
			{
				oslContent::outputReports($userID);
			}
			// Output Search
			elseif($callContent == 2)
			{
				oslContent::outputSearch($userID,$callContent,3);
			}
 			// Output manage report.
			elseif($callContent == 3)
			{
				oslContent::outputManageReport($userID,3);
			}
 			// Output help.
			elseif($callContent == 4)
			{
				oslContent::outputHelp($callContent,1);
			}
 			// Output L-mail.
			elseif($callContent == 5)
			{
				oslUser::outputLmail($userID,$callContent,1);
			}
 			// Output drafts.
			elseif($callContent == 6)
			{
				oslContent::outputDrafts($userID,$callContent);
			}
			// Output Admin section.
			elseif($callContent == 7)
			{
				oslContent::outputAdmin($userID,$callContent,3);
			}
			// Output User Management.
			elseif($callContent == 8)
			{
				oslUser::outputUserManagement($userID,$callContent,$callUser,3);
			}
			// Output the Style Guide.
			elseif($callContent == 9)
			{
				oslContent::outputStyleGuide($callContent,1);
			}
			// Output links page.
			elseif($callContent == 20)
		    {
				oslContent::outputLinksPage($callContent,1);
			}
		}
	}

	static function outputMessageWithClass($messages, $className)
	{
		if (count($messages) > 0) {
			echo "<div class=\"$className\">\n";
			foreach ($messages as $msg) {
				echo "<p>$msg</p>\n";
			}
			echo "</div>\n";
		}
	}

	static function outputMessages()
	{
		oslContent::outputMessageWithClass(oslMessages::getNotes(), "msgNote");
		oslContent::outputMessageWithClass(oslMessages::getErrors(), "msgError");
	}

	// Get content details.
	static function getContentDetails($callContent,$field)
	{
	 	// If parameters have been sent.
	 	if($callContent != NULL && $field != NULL)
	 	{
            // Get contents to be output in top links.
            $rows = oslDAO::executePrepared("SELECT $field
							 FROM tblContents
							 WHERE contentID=?", "i", array($callContent));
            // Init.
            $val = oslDAO::getFirstResultEntry($rows, $field);
            // Return.
            return $val;
        }
	}

	// Output content title
	static function getContentTitle($callContent)
	{
	 	// If parameters have been sent.
	 	if($callContent != NULL)
	 	{
			// Set.
			$str = oslContent::getContentDetails($callContent,"contentName");
		}
		// Return.
		return $str;
	}

	// Check if the content is selected.
	static function checkContent($content,$contentID)
	{
	 	// Init.
	 	$str = NULL;
		// If the two are equal then the user is in the selected content.
		if($content == $contentID)
		{
			$str = "Sel";
		}
		// Return.
		return $str;
	}

	// Get the highest/lowest contentID.
	static function getContentRange($callContent,$sort)
	{
		// Init.
		$val = 0;
		// If content sent.
		if($callContent != NULL && $callContent != 0)
		{
			if (!($sort === "ASC" || $sort === "DESC")) {
				$sort = "ASC";
			}
			// Get.
			$rows = oslDAO::executePrepared("SELECT contentID
										 FROM tblContents
										 ORDER BY contentID $sort
										 LIMIT 1","", array());
			// Set.
			$val = oslDAO::getFirstResultEntry($rows, "contentID");
		}
		// Return.
		return $val;
	}

	// Get report display information.
	static function reportTotalDisplay($sql, $sql_types, $sql_args)
	{
		global $counter, $startPage, $printCall;
	 	// Init.
	 	$str = NULL;
	 	$s = NULL;
	 	$oDiv = NULL;
	 	// If not printing.
		if($printCall == NULL)
		{
			// Get reports.
			$rows = oslDAO::executePrepared("SELECT COUNT(reportID) AS 'totalReports'
										 FROM tblReports
										 LEFT JOIN tblTasks ON tblReports.taskFK = tblTasks.taskID
										 LEFT JOIN tblSections ON tblTasks.sectionFK = tblSections.sectionID
										 LEFT JOIN tblUsers ON tblReports.authorFK = tblUsers.userID
										 WHERE postConfirmed=1 $sql", $sql_types, $sql_args);
			// Get report total.
			$reportTotal = oslDAO::getFirstResultEntry($rows, "totalReports");
			// If the report total is not 1, pluralise the text.
			if($reportTotal != 1)
			{
				$s = "s";
			}
			// Deal with pagination.
			if($counter == NULL)
			{
				// Set default start.
				$startRecord = 1;
				// If there are less than 21 reports, set the last record total to that returned by the query.
				if($reportTotal < 21)
				{
					$endRecord = $reportTotal;
				}
				// Otherwise set it to 20 as only 20 reports can be displayed at a time.
				else
				{
					$endRecord = 20;
				}
			}
			// Set titleBar div.
			$oDiv = oslStructure::openDiv("titleBar",1,NULL);
			// If reports exist.
			if($reportTotal > 0)
			{
				// Calculate which records are being displayed.
				if($startPage != NULL)
				{
					$startRecord = ($startPage-1)*20+1;
					$endRecord = $startRecord+19;
				}
				else
				{
					$startRecord = 1;
					$endRecord = 20;
				}
			 	// If the report total is currently less than 21, set displayEndRecord to the actual total.
				if($reportTotal <= 20)
				{
					$displayEndRecord = $reportTotal;
				}
				else
				{
					$displayEndRecord = $endRecord;
				}
				// endRecord cannot be greater than reportTotal.
				if($endRecord > $reportTotal)
				{
					$displayEndRecord = $reportTotal;
				}
				// Set string.
				$str .=  "Displaying report$s <strong>$startRecord</strong>-<strong>$displayEndRecord</strong> of <strong>$reportTotal</strong>.";
			}
			// Output links to other records.
			if($reportTotal > 20)
			{
			 	// Add text.
				$str .=  "Go to page \n";
				// First and last link.
				if ( $startPage == NULL ) { $startPage=0; }
				$startLink = $startPage-5;
				$endLink = $startPage+5;
				// Output START link.
				if($startPage > 5)
				{
					$str .=  "<a href=\"index.php?startPage=$b\" class=\"pageNo\" target=\"_parent\">Start</a> ";
				}
				// Loop through and output page links.
				for($a=20;$a<$reportTotal;$a=$a+20)
				{
					$b++;
					if(($b > $startLink && $b < $endLink) || ($endLink <= 10 && $b <= 10))
					{
					 	if(($startPage == NULL && $b == 1) || $b == $startPage)
					 	{
							$outputB = "<strong>$b</strong>";
							$classes = "curPageNo";
						}
						else
						{
							$outputB = $b;
							$classes = "pageNo";
						}
						$str .=  "<a href=\"index.php?startPage=$b\" class=\"$classes\" target=\"_parent\">$outputB</a> ";
					}
				}
				// Output END link.
				$b++;
				$str .=  "<a href=\"index.php?startPage=$b\" class=\"pageNo\" target=\"_parent\">End</a> ";

			}
			// If the user is searching set the search again button.
			if($searchCriteria != NULL)
			{
				$str .=  "<a href=\"index.php?content=2\" target=\"_parent\"><img src=\"images/searchAgain.gif\" class=\"searchAgain\" /></a>\n";
			}
			// Insert str into div.
			$str = oslStructure::getTitleHdr($str,2);
			// Close titleBar div.
			$oDiv = $oDiv.$str.oslStructure::closeDiv("titleBar",1);
		}
		// Return.
		return $oDiv;
	}

	// Check for comments.
	static function checkForComments($callRep, $previewID=NULL)
	{
	 	// Init.
	 	$val = NULL;
	 	// If report called.
	 	if($callRep != NULL)
	 	{
			// Get.
			$query = "SELECT reportID
					  FROM tblReports
					  WHERE parentFK=? AND postConfirmed=1
					  LIMIT 1";
			$types = "i";
			$args = array($callRep);
			if ($previewID != NULL) {
				// special case there is an active preview in this view somewhere
				// not necessarily in in this post, but we need to check
				$query = "SELECT reportID
						  FROM tblReports
						  WHERE parentFK=? AND (postConfirmed=1 OR reportID=?)
						  LIMIT 1";
				$types .= "i";
				$args[] = $previewID;
			}

			$rows = oslDAO::executePrepared($query, $types, $args);
			// If exists.
			if(count($rows) > 0)
			{
				$val = TRUE;
			}
			else
			{
				$val = FALSE;
			}
		}
		// Return.
		return $val;
	}

	// Output draft reports.
	static function getDrafts($userID)
	{
	 	// Init.
	 	$str = NULL;
		// Get reports.
		$rows = oslDAO::executePrepared("SELECT *, DATE_FORMAT(dateAdded,'%H:%i, %W %d %M %Y') AS 'dateAddedAdj'
									 FROM tblReports
									 WHERE postConfirmed=0 AND authorFK=?
									 ORDER BY dateAdded DESC", "i", array($userID));
		// Loop through and output reports.
		foreach ($rows as $loop)
		{
			// Initialise.
			$reportID = $loop["reportID"];
			$reportTitle = $loop["reportTitle"];
			$dateAddedAdj = $loop["dateAddedAdj"];
			$parentFK = $loop["parentFK"];
			$addCommentTo = NULL;
			if($parentFK != 0)
			{
				$addCommentTo = $parentFK;
			}
			// Output.
			$str .= "<div class=\"uploadedImg\">\n";
			$str .= "	<div class=\"uploadedFileType\"><a href=\"includes/confirmation.php?adminType=deleteReport&callRep=$reportID\"><img src=\"images/delFile.gif\" onclick=\"return confirm('Are you sure you wish to delete this draft report?')\" /></a></div>\n";
			$str .= "	<div class=\"uploadedFileName\"><a href=\"index.php?content=3&callRep=$reportID&addCommentTo=$addCommentTo\">$reportTitle</a> (Last saved: $dateAddedAdj)</div>\n";
			$str .= "</div>\n";
		}
		// If there are no drafts output the message here.
		if($reportID == NULL)
		{
			$str .= "<p>You currently have no reports saved to draft.</p>\n";
		}
		// Return.
		return $str;
	}

	// Count number of draft reports for header.
	static function countDrafts($userID)
	{
		// Get reports.
		$rows = oslDAO::executePrepared("SELECT COUNT(reportID) AS 'totalDrafts'
									 FROM tblReports
									 WHERE postConfirmed=0 AND authorFK=?","i", array($userID));
		// Return total value.
		return oslDAO::getFirstResultEntry($rows, "totalDrafts");
	}

	// Output Drafts.
	static function outputDrafts($callUser,$callContent)
	{
		// Output title.
		$title = oslContent::getContentTitle($callContent);
		// Output header.
		$str = "	<p><img src=\"images/default-theme/SquareShadowed.gif\" alt=\"\" class=\"greenSquareShadowed\" />Reports listed in this section have been saved in draft format</p>\n";
		// Output Introduction.
		$str .= "	<p>- To edit a report click on the name below.<br />\n";
		$str .= "- Click on the <img src=\"images/delFile.gif\" /> button next to the filename to delete the report completely.</p>\n";
		// Output draft reports.
		$str .= oslContent::getDrafts($callUser);
		// Output.
		echo oslStructure::getRoundedContentDiv("divSearch",$title,$str,$tabs);
	}

	// Get new section ID
	static function getSectionID()
	{
		// Get.
		$rows = oslDAO::executePrepared("SELECT sectionID
						 FROM tblSections
						 ORDER BY sectionID DESC
						 LIMIT 1", "", array());
		// Return value.
		return oslDAO::getFirstResultEntry($rows, "sectionID");
	}

	// Get section info
	static function getSectionInfo($field,$sectionID)
	{
		// Get.
		$rows = oslDAO::executePrepared("SELECT $field
					 FROM tblSections
					 WHERE sectionID = ?", "i", array($sectionID));
		// Return value.
		return oslDAO::getFirstResultEntry($rows, $field);
	}

	// Build the search array.
	static function buildSearchArray($input)
	{
	 	// Init.
	 	$array = array();
		// Loop through input array.
		foreach($input as $key => $val)
		{
			// Built.
			$array[$key] = $val;
		}
		$array['srcQuickSearch'] = FALSE;
		// Return.
		return $array;
	}

	// Build the quick search array.
	static function buildQuickSearchArray($input)
	{
		global $showTags;
		// Init.
		$array = array();
		if ($input['srcKeyword'] != NULL and $input['srcKeyword'] != '') {
				$keywordUnsafe = $input['srcKeyword'];
				$keywordSafe = $input['srcKeyword'];
				$array['srcKeywordType'] = 2;
				$array['srcKeyword'] = $keywordUnsafe;
				$array['srcAuthorType'] = 2;
				$array['srcAuthor'] = $keywordUnsafe;
				if (is_numeric($keywordSafe)) {
						$array['srcCallRep'] = $keywordSafe;
				}
				$rows = oslDAO::executePrepared("SELECT sectionFK, taskID from tblTasks WHERE taskName = ?;", "s", array($keywordUnsafe));
				if (count($rows) > 0) {
						$row = $rows[0];
						$array['reportSection'] = $row['sectionFK'];
						$array['reportTask'] = $row['taskID'];
				}
				if ($showTags == TRUE) {
					// getTagsIDsMatchingText escapes its input
					$tagIDs = oslDAO::getTagsIDsMatchingText($keywordUnsafe);
					if (count($tagIDs) > 0) {
						$array['srcReportTag'] = $tagIDs;
					}
				}
				$array['srcQuickSearch'] = TRUE;
		}
		return $array;
	}

	static function buildSearchArrayFromTextInput($searchSection, $searchTask)
	{
		$rows = oslDAO::executePrepared("SELECT sectionID FROM tblSections WHERE sectionName=?", "s", array($searchSection));
		$_searchReportSection = oslDAO::getFirstResultEntry($rows, "sectionID");
		if ($_searchReportSection !== FALSE) {
			if ($searchTask != "") {
				$rows = oslDAO::executePrepared("SELECT taskID FROM tblTasks WHERE sectionFK=? AND taskName = ?", "is", array($_searchReportSection, $searchTask));
				$_searchReportTask = oslDAO::getFirstResultEntry($rows, "taskID");
			}
			$_searchArray = array("reportSection"=>$_searchReportSection);
			if ($_searchReportTask !== FALSE) {
				$_searchArray["reportTask"] = $_searchReportTask;
			}
			$_SESSION["searchArray"] = oslContent::buildSearchArray($_searchArray);
		}
	}

	// Build SQL statement for search.
	// returns an array (sql w/placeholders, types, sql args, html description)
	static function buildSQL()
	{
	 	// Get variables set elsewhere.
	 	global $preview,$callRep,$userID,$searchArray,$defaultDateFormat, $excludeRobots, $robotList, $showTags;

		$sql_types = "";
		$sql_args = array();

		$previewActive = FALSE;
	 	// If calling preview.
		if($preview != NULL && oslContent::getReportInfo("authorFK",$preview) == $userID)
		{
			$sql = " OR reportID = ?";
			$previewActive = TRUE;
			$sql_types .= "i";
			$sql_args[] = $preview;
		}
		// If searching or calling specific report.
		if(!empty($searchArray))
		{
		    $quickSearch = FALSE;
			$andOr = $_andOr = "AND";
			$searchSql = "";
		    if (isset($searchArray['srcQuickSearch']) && $searchArray['srcQuickSearch'] === TRUE && count($searchArray) > 1) {
				$quickSearch = TRUE;
				$andOr = "";
				$_andOr = "OR";
			}
		    //print_r($searchArray);
		 	// Loop through array and create variables on the fly.
		 	foreach($searchArray as $key => $val)
		 	{
				// Formerly this filtered for sql, but since moving to
				// prepared statments this is not needed anymore.
				// However all values used here must be escaped/sanitized for HTML output
				${$key} = $val;
			}
			if (is_array($srcAuthor)) {
				$srcAuthor = implode(" ", $srcAuthor);
			}
			// Section
			if($reportSection != NULL && $reportTask == NULL)
			{
				$searchSql .= " $andOr sectionFK=? ";
				$searchCriteria = "<strong>Section:</strong> ".oslContent::getTaskInfo("sectionName",$reportTask)."<br />\n";
				$andOr = $_andOr;
				$sql_types .= "i";
				$sql_args[] = $reportSection;
			}
			// Task.
			if($reportTask != NULL)
			{
				$sectionName = oslContent::getTaskInfo("sectionName",$reportTask);
				$taskName = oslContent::getTaskInfo("taskName",$reportTask);

				$tagIDs = oslDAO::getTagsIDsMatchingText($taskName);

				$sql_types .= "i";
				$sql_args[] = $reportTask;
				if ($showTags == TRUE && count($tagIDs) > 0) {
					$searchSql .= " $andOr (taskFK=? OR reportID in (SELECT reportFK FROM tblReportTags WHERE tagFK in ".oslDAO::createListForSelect(count($tagIDs)).")) ";
					foreach ($tagIDs as $k => $val) {
						$sql_types .= "i";
						$sql_args[] = $val;
					}
				} else {
					$searchSql .= " $andOr taskFK=? ";
				}
				$searchCriteria .= "<strong>Section:</strong> ".$sectionName."<br /><strong>Task:</strong> ".$taskName."<br />\n";
				$andOr = $_andOr;
			}
			// Report ID.
			if($srcCallRep != NULL)
			{
				$searchSql .= " $andOr reportID=? ";
				$searchCriteria .= "<strong>Report ID:</strong> ".oslDAO::sanitizeForHTML("".$srcCallRep)."<br />\n";
				$andOr = $_andOr;
				$sql_types .= "i";
				$sql_args[] = $srcCallRep;
			}
			// Date from.
			if($srcDateFrom != NULL)
			{
				$mysqlDateFrom = oslStructure::mysqlDateConversion($srcDateFrom,NULL);
				$searchSql .= " $andOr lastCommentDate >= ? ";
				$searchCriteria .= "<strong>Date (from):</strong> ".oslDAO::sanitizeForHTML("".$srcDateFrom)."<br />\n";
				$andOr = $_andOr;
				$sql_types .= "s";
				$sql_args[] = $mysqlDateFrom;
			}
			// Date To.
			if($srcDateTo != NULL)
			{
				$mysqlDateTo = oslStructure::mysqlDateConversion($srcDateTo,1);
				$searchSql .= " $andOr lastCommentDate < ? ";
				$searchCriteria .= "<strong>Date (to):</strong> ".oslDAO::sanitizeForHTML("".$srcDateTo)."<br />\n";
				$andOr = $_andOr;
				$sql_types .= "s";
				$sql_args[] = $mysqlDateTo;
			}
			// Author.
			if($srcAuthor != NULL)
			{
				//$andOr = $_andOr;
			 	// Exact string.
				if($srcAuthorType == 1)
				{
					$searchSql .= " $andOr authorNames LIKE ? ";
					$sql_types .= "s";
					$sql_args[] = "%$srcAuthor%";
				}
				// Any.
				elseif($srcAuthorType == 2)
				{
					// Explode string.
					$authorArray = explode(" ",$srcAuthor);
					$searchSql .= " $andOr (";
					foreach($authorArray as $key => $val)
					{
						if($checkForOR == TRUE)
						{
							$searchSql .= " OR ";
						}
						$searchSql .= "authorNames LIKE ? ";
					 	$checkForOR = TRUE;
						$sql_types .= "s";
						$sql_args[] = "%$val%";
					}
					$searchSql .= ") ";
				}
				// All.
				elseif($srcAuthorType == 3)
				{
					// Explode string.
					$authorArray = explode(" ",$srcAuthor);
					$searchSql .= " $andOr (";
					foreach($authorArray as $key => $val)
					{
						if($checkForAND == TRUE)
						{
							$searchSql .= " AND ";
						}
						$searchSql .= "authorNames LIKE ? ";
					 	$checkForAND = TRUE;
						$sql_types .= "s";
                                                $sql_args[] = "%$val%";
					}
					$searchSql .= ") ";
				}
				$searchCriteria .= "<strong>Author(s):</strong> ".oslDAO::sanitizeForHTML("".$srcAuthor)." (".oslContent::getSearchStringType($srcAuthorType).")<br />\n";
				$andOr = $_andOr;
			}
			// Keyword.
			if($srcKeyword != NULL)
			{
			 	// Exact string.
				if($srcKeywordType == 1)
				{
					$searchSql .= " $andOr (reportText LIKE ? OR reportTitle LIKE ? ) ";
					$sql_types .= "ss";
					$sql_args[] = "%$srcKeyword%";
					$sql_args[] = "%$srcKeyword%";
				}
				// Any.
				elseif($srcKeywordType == 2)
				{
					// Explode string.
					$keywordArray = explode(" ",$srcKeyword);
					$searchSql .= " $andOr (";
					foreach($keywordArray as $key => $val)
					{
						if($checkForKeyOR == TRUE)
						{
							$searchSql .= " OR ";
						}
						$searchSql .= "(reportText LIKE ? OR reportTitle LIKE ?) ";
					 	$checkForKeyOR = TRUE;
						$sql_types .= "ss";
						$sql_args[] = "%$val%";
						$sql_args[] = "%$val%";
					}
					$searchSql .= ") ";
				}
				// All.
				elseif($srcKeywordType == 3)
				{
					// Explode string.
					$keywordArray = explode(" ",$srcKeyword);
					$searchSql .= " $andOr (";
					foreach($keywordArray as $key => $val)
					{
						if($checkForKeyAND == TRUE)
						{
							$searchSql .= " AND ";
						}
						$searchSql .= "(reportText LIKE ? OR reportTitle LIKE ?) ";
					 	$checkForKeyAND = TRUE;
						$sql_types .= "ss";
                                                $sql_args[] = "%$val%";
                                                $sql_args[] = "%$val%";
					}
					$searchSql .= ") ";
				}
				$sanitizedKeyword = oslDAO::sanitizeForHTML("".$srcKeyword);
				$srcKeywordArray = explode(" ",$sanitizedKeyword);
				// Colour each individual keyword.
				foreach($srcKeywordArray as $key => $val)
				{
					$colour = oslStructure::getColour($key);
					$sanitizedKeyword = str_replace("$val", "<font color=\"$colour\">$val</font>", $sanitizedKeyword);
				}
				$searchCriteria .= "<strong>Keyword(s):</strong> $sanitizedKeyword (".oslContent::getSearchStringType($srcKeywordType).")<br />\n";
				$andOr = $_andOr;
			}
			// Tags.
			if (isset($srcReportTag) && is_array($srcReportTag) && $showTags == TRUE) {
				if (count($srcReportTag) > 0) {
						// srcReportTag wants to be an array of int, but may need to be an array of string
						$subSearch = oslDAO::createListForSelect(count($srcReportTag));

						$searchSql .= " $andOr (reportID in (SELECT reportFK FROM tblReportTags WHERE tagFK in $subSearch))";

						$sub_types = "";
						$sub_args = array();
						foreach ($srcReportTag as $k => $val) {
							$sql_types .= "i";
							$sql_args[] = intval($val);
							$sub_types .= "i";
							$sub_args[] = intval($val);
						}

						$searchCriteria .= "<strong>Tags:</strong>";

						//$get = oslDAO::executeQuery("SELECT tag FROM tblTags WHERE tagID in $subSearch");
						//if ($get !== FALSE) {
						//		while ($loop = mysql_fetch_array($get)) {
						//				$searchCriteria .= " ".$loop['tag'];
						//		}
						//}
						$rows = oslDAO::executePrepared("SELECT tag FROM tblTags WHERE tagID in $subSearch", $sub_types, $sub_args);
						foreach ($rows as $k => $row) {
							$searchCriteria .= " ".$row['tag'];
						}

						$searchCriteria .= "<br/>\n";
				}
			}
			if ($quickSearch) {
				if ($searchSql != "") {
						$sql .= " AND ($searchSql)";
				}
			} else {
				$sql .= " ".$searchSql;
			}

		    if($excludeRobots) {
				if ($srcExcludeRobots != NULL and count($robotList) > 0) {
					$sql .= " AND authorNames NOT IN ".oslDAO::createListForSelect(count($robotList));;
					foreach ($robotList as $value) {
						$sql_types .= "s";
						$sql_args[] = $value;
					}
					$searchCriteria .= "<strong>Exclude Robots</strong><br/>\n";
				}
			}
		}
		// If calling specific report, over-write SQL.
		elseif($callRep != NULL and !$previewActive)
		{
			// Set default value to search value.
			$sql .= " AND reportID=? ";
			$sql_types .= "i";
			$sql_args[] = $callRep;
		}
		return array($sql, $sql_types, $sql_args, $searchCriteria);
	}

	// Return type of search string.
	static function getSearchStringType($type)
	{
		if($type == 1)
		{
			return "Exact string";
		}
		elseif($type == 2)
		{
			return "Any";
		}
		elseif($type == 3)
		{
			return "All";
		}
	}

	// Get first section.
	static function getFirstSectionID()
	{
		// $sql = oslDAO::executeQuery("SELECT sectionID
		// 							 FROM tblSections
		// 							 ORDER BY sectionName
		// 							 LIMIT 1");

		$sql = "SELECT sectionID FROM tblSections ORDER BY sectionName LIMIT 1";
		$types = "";
		$input = array();
		$rows = oslDAO::executePrepared($sql, $types, $input);
		// Return value
		// return mysql_result($sql,0,"sectionID");
		return oslDAO::getFirstResultEntry($rows, "sectionID");
	}

	// Confirm post.
	static function getPostStatus($nextStep)
	{
	 	if($nextStep == 3)
	 	{
			$postStatus = 1;
		}
		else
		{
			$postStatus = 0;
		}
		return $postStatus;
	}

	// Get new report ID.
	static function getNewReportID($userID)
	{
		// Get.
		// $sql = oslDAO::executeQuery("SELECT reportID
		// 							 FROM tblReports
		// 							 WHERE authorFK=$userID
		// 							 ORDER BY reportID DESC
		// 							 LIMIT 1");

	  $sql = "SELECT reportID FROM tblReports WHERE authorFK=?
						ORDER BY reportID DESC LIMIT 1";
		$types = "i";
		$input = array($userID);
		$rows = oslDAO::executePrepared($sql, $types, $input);
		// Return.
		// return mysql_result($sql,0,"reportID");
		return oslDAO::getFirstResultEntry($rows, "reportID");
	}

	// Get task info.
	static function getTaskInfo($field,$taskFK)
	{
		if ($taskFK==NULL) {
			return FALSE;
		}
		// Get.
		// $sql = oslDAO::executeQuery("SELECT $field
		// 							 FROM tblTasks
		// 							 LEFT JOIN tblSections ON tblTasks.sectionFK=tblSections.sectionID
		// 							 WHERE taskID=$taskFK");

		$sql = "SELECT $field FROM tblTasks LEFT JOIN tblSections
						ON tblTasks.sectionFK=tblSections.sectionID WHERE taskID=?";
		$types = "i";
		$input = array($taskFK);
		$rows = oslDAO::executePrepared($sql, $types, $input);
		// Return.
		// return mysql_result($sql,0,"$field");
		return oslDAO::getFirstResultEntry($rows, $field);
	}

	// Get report info.
	static function getReportInfo($field,$callRep)
	{
	 	// If callRep exists.
	 	if($callRep != NULL)
	 	{
			// Get.
			// $get = oslDAO::executeQuery("SELECT $field
			// 							 FROM tblReports
			// 							 WHERE reportID=$callRep");

			$get = "SELECT $field FROM tblReports WHERE reportID=?";
			$types = "i";
			$input = array($callRep);
			$rows = oslDAO::executePrepared($get, $types, $input);
			// Return.
			// return mysql_result($get,0,"$field");
			return oslDAO::getFirstResultEntry($rows, $field);
		}
	}

	// Get the tagCall attribute associated with the report
	// may be FALSE or ''
	static function getReportTagCall($callRep)
	{
		if ($callRep != NULL)
		{
		    // $get = oslDAO::executeQuery("SELECT tagCall
				// 						FROM tblReports, tblValues
				// 						WHERE editorFK = valueID
				// 								AND reportID=$callRep");

				$get = "SELECT tagCall FROM tblReports, tblValues WHERE editorFK = valueID AND reportID=?";
				$types = "i";
				$input = array($callRep);
				$rows = oslDAO::executePrepared($get, $types, $input);

		    // return mysql_result($get, 0, "tagCall");
				return oslDAO::getFirstResultEntry($rows, "tagCall");
		}
		return FALSE;
	}

	// Output reports.
	static function outputReports($callUser)
	{
	 	// Get variables set elsewhere.
	 	global $content,$step,$preview,$callRep,$userID,$startPage,$searchArray,$printCall,$appURL,$showCommentStandalone,$showTags;
	 	// Init.
	 	$hdr = NULL;
	 	$txt = NULL;
	 	$str = NULL;
		$firstEntry = TRUE;

		// some special handling so we do not loose previews
		$_previewID = NULL;
		$_inPreview = FALSE;

		$colourSeen = array();
		// If searching.
		if(!empty($searchArray))
		{
		 	// Loop through array and create variables on the fly.
		 	// foreach($searchArray as $key => $val)
		 	// {
			// 	${$key} = oslDAO::filterForMySQL($val);
			// }
		}
		// Build SQL string.
		$sql_info = oslContent::buildSQL();
		$sql = $sql_info[0];
		$sql_types = $sql_info[1];
		$sql_args = $sql_info[2];
		$searchCriteria = $sql_info[3];
		// Output report info beforehand.
		echo oslContent::reportTotalDisplay($sql, $sql_types, $sql_args);
		// If searchCriteria contains information output it here.
		if($searchCriteria != NULL)
		{
			$str .= "<p class=\"searchCriteria\"><strong>Search criteria</strong><br />\n";
			// Output search criteria.
			$str .= "$searchCriteria</p>\n";
			// Get buttons.
			$str .= "<div class=\"searchBtnDiv\">\n";
			$str .= oslStructure::getButton("btnRemoveSearchFilter","REMOVE SEARCH FILTER",NULL,"includes/search.php?adminType=endSearch&content=1",NULL,NULL,FALSE);
			$str .= oslStructure::getButton("btnSearchAgain","SEARCH AGAIN",NULL,"index.php?content=2",NULL,NULL,FALSE);
			$str .= "</div>\n";
			$str .= oslStructure::getBreak($tabs);
		}
		// If the user has selected a start page set SQL here.
		if($startPage != NULL)
		{
			$lowerLimit = ($startPage-1)*20;
			$upperLimit = $lowerLimit+20;
			$limit = "$lowerLimit, 20";
		}
		else
		{
			$limit = "20";
		}
		$getRes = FALSE;
		if ($showCommentStandalone) {
			// Get reports.
			$get = "SELECT *, DATE_FORMAT(dateAdded,'%H:%i, %W %d %M %Y') AS 'dateAddedAdj', DATE_FORMAT(lastCommentDate,'%H:%i, %W %d %M %Y') AS 'lastCommentDateAdj'
					FROM tblReports
					LEFT JOIN tblTasks ON tblReports.taskFK = tblTasks.taskID
					LEFT JOIN tblSections ON tblTasks.sectionFK = tblSections.sectionID
					LEFT JOIN tblUsers ON tblReports.authorFK = tblUsers.userID
					LEFT JOIN tblValues ON tblReports.editorFK = tblValues.valueID
					WHERE postConfirmed=1 $sql
					ORDER BY dateAdded DESC, reportID DESC
					LIMIT $limit";
			$rows = oslDAO::executePrepared($get, $sql_types, $sql_args);
		} else {
			$sqlDateSortField = "dateAdded"; // "lastCommentDate";
			if (isset($preview) && is_numeric($preview)) {
				$_previewID = intval($preview);
			}
			// Handle things differently when we do not show the comments as seperate entries.
			// What we want is to show each parent entry, which will in turn show all the comments.
			// Note, this may need to be done multiple times to catch a tree structure of comments.
			$get = "SELECT reportID, parentFK, authorFK
					FROM tblReports
					LEFT JOIN tblTasks ON tblReports.taskFK = tblTasks.taskID
					LEFT JOIN tblSections ON tblTasks.sectionFK = tblSections.sectionID
					LEFT JOIN tblUsers ON tblReports.authorFK = tblUsers.userID
					LEFT JOIN tblValues ON tblReports.editorFK = tblValues.valueID
					WHERE postConfirmed=1 $sql
					ORDER BY $sqlDateSortField DESC, reportID DESC
					LIMIT $limit";
			$ids = array();
			$rows = oslDAO::executePrepared($get, $sql_types, $sql_args);
			foreach ($rows as $loop) {
				if ($_previewID != NULL && $loop["reportID"] == $_previewID && $loop["authorFK"] == $userID) {
				    $_inPreview = TRUE;
				}
				$ids[] = ($loop["parentFK"] == 0 ? $loop["reportID"] : $loop["parentFK"]);
			}
			$getRes = array();
			// minor optimization, don't continually call count
			$idCount = count($ids);
			if ($idCount > 0) {
				$new_types = "";
				$new_args = array();
				if ($idCount == 1) {
					$sqlStr = " AND reportID = ?";
					$new_types .= "i";
					$new_args[] = $ids[0];
				} else {
					// This should not be a burdensome query as we are already limited
					// be the first query
					$sqlStr = " AND reportID in (";
					$firstId = TRUE;
					foreach ($ids as $id) {
						$sqlStr .= ($firstId ? "" : ", ")."?";
						$firstId = FALSE;
						$new_types .= "i";
						$new_args[] = $id;
					}
					$sqlStr .= ")";
				}
				// reissue the query, this time asking for all fields.  We do not
				// need a limit term as we have already limited the input
				// this needs to be special cased for previewing - FIXME and handle this better
				$get = "SELECT *, DATE_FORMAT(dateAdded,'%H:%i, %W %d %M %Y') AS 'dateAddedAdj', DATE_FORMAT(lastCommentDate,'%H:%i, %W %d %M %Y') AS 'lastCommentDateAdj'
					FROM tblReports
					LEFT JOIN tblTasks ON tblReports.taskFK = tblTasks.taskID
					LEFT JOIN tblSections ON tblTasks.sectionFK = tblSections.sectionID
					LEFT JOIN tblUsers ON tblReports.authorFK = tblUsers.userID
					LEFT JOIN tblValues ON tblReports.editorFK = tblValues.valueID ";
				if ($_inPreview) {
				    $get .= "WHERE (postConfirmed=1 OR reportID=$_previewID) $sqlStr ";
				} else {
				    $get .= "WHERE postConfirmed=1 $sqlStr ";

				}
				$get .= "ORDER BY $sqlDateSortField DESC, reportID DESC";

				$rows = oslDAO::executePrepared($get, $new_types, $new_args);
			}
		}
		// $reportCount is used to check if any reports have been output
		$reportCount = 0;
		if (count($rows) > 0) {
				// Loop through and output reports.
				foreach ($rows as $loop)
				{
					// Initialise.
					$reportID = $loop["reportID"];
					$reportTitle = $loop["reportTitle"];
					$parentFK = $loop["parentFK"];


					// If this is a comment, change a few things.
					if($parentFK != 0)
					{
						if (!$showCommentStandalone) {
								continue;
						}
						$reportTitle = $reportTitle." (Click here to view original report: <a href=\"index.php?callRep=$parentFK\" target=\"_parent\">$parentFK</a>)";
						$reportTitleClass = "commentTitle";
						$adCommentID = $parentFK;
					}
					$reportCount += 1;
					$tagCall = $loop["tagCall"];
					// Handle report text, dealing with keyword search.
					$reportText = oslContent::handleKeywordSearch($srcKeyword,$loop["reportText"]);
					if($tagCall != NULL)
					{
						$reportText = "<$tagCall>\n".
									  $reportText.
									  "</$tagCall>\n";
					}
					$editorFK = $loop["editorFK"];
					$sectionName = $loop["sectionName"];
					$sectionColour = $loop["sectionColour"];
					// only add a style tag for the section colour if it has not
					// been added before.
					if (!isset($colourSeen[$sectionColour])) {
						oslStructure::setReportHdr($sectionColour);
						$colourSeen[$sectionColour] = 1;
					}
					$taskName = $loop["taskName"];
					$authorNames =  $loop["authorNames"];
					$dateAddedAdj = $loop["dateAddedAdj"];
					$lastCommentDate = $loop["lastCommentDate"];
					$lastCommentDateAdj = $loop["lastCommentDateAdj"];

					if ($firstEntry) {
						$str .= "Reports until $dateAddedAdj";
						$firstEntry = FALSE;
					}
					// Set preview class if required.
					$callType = NULL;
					$previewTxt = NULL;
					if($preview == $reportID)
					{
						$callType = "Preview";
						$previewTxt = " <strong>(PREVIEW)</strong>";
					}
					// Open reportShell div.
					$str .= oslStructure::openDiv("report_$reportID",1,"reportShell");
					// Add section header.
					$str .= oslStructure::openDiv("sectHdr_$reportID",2,"reportSection$sectionColour");
					// Add section/task div.
					$str .= oslStructure::openDiv("sectTask_$reportID",3,"sectionTask");
					// Add section details.
					$str .= "				<a name=\"$reportID\" id=\"$reportID\"></a> $sectionName $taskName $previewTxt\n";

					if ($showTags) {
						// Get tags.
						$currentTags = oslContent::outputReportTags($reportID,$content);
						if ($currentTags != "") {
							$str .= "(".$currentTags.")";
						}
					}

					// $commentText = oslContent::getComments($reportID,$content,$printCall, $preview, $_previewID, $step,1,$tabs);
					$commentText = oslContent::getComments($reportID,$content,$printCall, $preview, $_previewID, $step,$tabs);

					// Close section/task div.
					$str .= oslStructure::closeDiv("sectTask_$reportID",3);
					// Add report options div.
					$str .= oslStructure::openDiv("reportOptions_$reportID",3,"reportOptions");
					// Get Print icon.
					$str .= oslContent::getPrintIcon($printCall,$reportID,4);
					// Get Add Comment button.
					$str .= oslContent::getAddCommentButton($printCall,$reportID,$userID,4);
					// Get Edit button.
					$str .= oslContent::getEditButton($printCall,$reportID,$preview,$userID,$editorFK,4);
					// Get a link to the report
					$str .= "<div class=\"report_link\"> <a target=\"_parent\" href=\"index.php?callRep=$reportID\">Link</a> </div>";
					// Close report options div.
					$str .= oslStructure::closeDiv("reportOptions_$reportID",3);
					// Close section header.
					$str .= oslStructure::closeDiv("sectHdr_$reportID",2);
					// Add author header.
					$str .= oslStructure::openDiv("authHdr_$reportID",2,"authorDetails$callType");
					// Add author details.
					$str .= "			$authorNames - <span class=\"datePost\">posted $dateAddedAdj</span> ";
					if ($commentText != "") {
						$str .= " - <span class=\"dateComment\">last comment - $lastCommentDateAdj</span>";
					}
					$str .= "($reportID)\n";
					// Close author header.
					$str .= oslStructure::closeDiv("authHdr_$reportID",2);
					// Add title header.
					$str .= oslStructure::openDiv("titleHdr_$reportID",2,"reportDetails$callType");
					// Add title details.
					$str .= "			<strong>$reportTitle</strong>\n";
					// Close title header.
					$str .= oslStructure::closeDiv("titleHdr_$reportID",2);
					// Add report header.
					$str .= oslStructure::openDiv("repHdr_$reportID",2,"reportDetails$callType");
					// Add report details.
					$str .= "			$reportText\n";
					// Close report header.
					$str .= oslStructure::closeDiv("reportHdr_$reportID",2);
					// Display attached images.
					$str .= oslFile::displayFiles($reportID,$content,$step,1,FALSE,2);
					// Display other files.
					$str .= oslFile::displayFiles($reportID,$content,$step,0,FALSE,2);
					// Get comments.
					$str .= $commentText;
					// Close reportShell div.
					$str .= oslStructure::closeDiv("report_$reportID",1);
					// Open repBL div.
					$str .= oslStructure::openDiv("repBL_$reportID",1,"reportBottomLine");
					// Open reportBottomLeft div.
					$str .= oslStructure::openDiv("reportBottomLeft_$reportID",2,"reportBottomLeft");
					// Close reportBottomLeft div.
					$str .= oslStructure::closeDiv("reportBottomLeft_$reportID",2);
					// Open reportBottomRight div.
					$str .= oslStructure::openDiv("reportBottomRight_$reportID",2,"reportBottomRight");
					// Close reportBottomRight div.
					$str .= oslStructure::closeDiv("reportBottomRight_$reportID",2);
					// Close repBL div.
					$str .= oslStructure::closeDiv("repBL_$reportID",1);
				}
		}
		// Output no reports found if user is searching and nothing meets the criteria.
		if($reportCount == 0)
		{
			$str .= "<p class=\"searchCriteria\">No reports meet the specified criteria.</p>\n";
		}
		// Output reports.
		echo $str;
		// output report info again
		echo oslContent::reportTotalDisplay($sql,$sql_types,$sql_args);
	}

	// Attach print icon to report.
	static function getPrintIcon($printCall,$callRep,$tabs)
	{
	 	// Init.
	 	$str = NULL;
		// If no print call made.
		if($printCall == NULL)
		{
			// Add number of tabs required.
		 	$tabStr = oslStructure::getRequiredTabs($tabs);
			// Open Div.
			$str .= oslStructure::openDiv("divPrintIcon_$callRep",$tabs,"printIcon");
			// Get icon.
			$str .= "$tabStr	<!-- Print report icon -->\n";
			$str .= "$tabStr	<a href=\"iframeSrc.php?printCall=TRUE&callRep=$callRep\" target=\"_blank\"><img src=\"images/print_icon.gif\" class=\"printer\" alt=\"Print this report.\" /></a>\n";
			// Close Div.
			$str .= oslStructure::closeDiv("divPrintIcon_$callRep",$tabs);
		}
		// Return.
		return $str;
	}

	// Get add comment button.
	static function getAddCommentButton($printCall,$callRep,$userID,$tabs)
	{
	 	// Init.
	 	$str = NULL;
	 	$tgt = $callRep;
		// If no print call made.
		if($printCall == NULL)
		{
			// If this report is already a comment.
			$parentFK = oslContent::getReportInfo("parentFK",$callRep);
			if($parentFK != 0)
			{
				$tgt = $parentFK;
			}
		 	// Set string.
			if($userID != NULL && oslContent::getReportInfo("postConfirmed",$callRep) == 1)
			{
				// Add number of tabs required.
			 	$tabStr = oslStructure::getRequiredTabs($tabs);
			 	// Set string.
				$str .= oslStructure::getButton("btnAddComment_$callRep","ADD COMMENT",NULL,"index.php?content=3&addCommentTo=$tgt","Right",NULL,$tabs);
			}
		}
		// Return.
		return $str;
	}

	// Get Edit button.
	static function getEditButton($printCall,$callRep,$preview,$userID,$editorFK,$tabs)
	{
	 	// Set variables set elsewhere.
	 	global $addCommentTo;
	 	$str = "";
		// If no print call made.
		if($printCall == NULL && oslDAO::canEditReport($callRep,$userID,$preview))
		{
			// Add number of tabs required.
		 	$tabStr = oslStructure::getRequiredTabs($tabs);
		 	// Set string.
			// removed $addCommentTo
			$str .= oslStructure::getButton("btnEdit_$callRep","EDIT",NULL,"index.php?content=3&callRep=$callRep&addCommentTo=&callEditor=$editorFK","Right",NULL,$tabs);
		}
		// Return.
		return $str;
	}

	// Get comments.
	static function getComments($callReport,$content,$printCall,$preview,$previewID,$step,$tabs)
	{
		global $callRep, $userID, $showCommentStandalone, $showTags;
	 	// Init.
	 	$str = "";
	 	// Set tabs.
	 	$tabStr = oslStructure::getRequiredTabs($tabs);
		// Look for comments.
		if(oslContent::checkForComments($callReport, $previewID))
		{
		 	// Open div.
			$str .= oslStructure::openDiv("comment_block_$callReport",$tabs,"commentBlock");
			// Output header.
			$str .= oslStructure::openDiv("comment_header_$reportID",$tabs+1,"commentHdr");;
			$str .= "$tabStr		<strong>Comments related to this report</strong>\n";
			$str .= oslStructure::closeDiv("comment_header_$reportID",$tabs+1);
			// Get.
			$confirmed = ($previewID == NULL || $showCommentStandalone ? "postConfirmed=1" : "(postConfirmed=1 OR reportID=$previewID)");

			// $get = oslDAO::executeQuery("SELECT *, DATE_FORMAT(dateAdded,'%H:%i, %W %d %M %Y') AS 'dateAddedAdj'
			// 							 FROM tblReports
			// 							 LEFT JOIN tblValues ON tblReports.editorFK =tblValues.valueID
			// 							 WHERE parentFK=$callReport AND $confirmed");

			$get = "SELECT *, DATE_FORMAT(dateAdded,'%H:%i, %W %d %M %Y')
							AS 'dateAddedAdj' FROM tblReports LEFT JOIN tblValues
							ON tblReports.editorFK =tblValues.valueID WHERE parentFK=? AND $confirmed";
			$types = "i";
			$input = array($callReport);
			$rows = oslDAO::executePrepared($get, $types, $input);
			// Return.
			// while($loop = mysqli_fetch_array($get))
			foreach($rows as $loop)
			{
			 	// Init.
			 	$reportID = $loop["reportID"];
			 	$functionCall = $loop["functionCall"];
		 		$reportText = $loop["reportText"];
				$editorFK = $loop["editorFK"];
				$tagCall = $loop["tagCall"];
				if($tagCall != NULL)
				{
					$reportText = "<$tagCall>\n".
				 			  $reportText.
							  "</$tagCall>\n";
				}
			 	$authorNames = $loop["authorNames"];
			 	$dateAddedAdj = $loop["dateAddedAdj"];
			 	$lastCommentDate = $loop["lastCommentDate"];
				// Output commentAuthor div.
				$str .= oslStructure::openDiv("comment_author_$reportID",$tabs+1,($callRep != $reportID ? "commentAuthor" : "selectedCommentAuthor"));
				// Output author, etc.
				$str .= "$tabStr		<strong>$authorNames</strong> - $dateAddedAdj ($reportID)";
				if ($showTags) {
						// Get tags.
						$str .= oslContent::outputReportTags($reportID,$content);
				}
				if ($previewID != NULL && $reportID == $previewID) {
						$str .= " <strong>(PREVIEW)</strong>";
				}
				if (!$showCommentStandalone) {
					// Get Edit button.
					$str .= oslContent::getEditButton($printCall,$reportID,$preview,$userID,$editorFK,4);
				}
				// Get a link to the comment
				$str .= "<div class=\"report_link\"> <a target=\"_parent\" href=\"index.php?callRep=$reportID\">Link</a> </div>";
				$str .= "<br />\n";
				// Close commentAuthor div.
				$str .= oslStructure::closeDiv("comment_author_$reportID",$tabs+1);
				// Output comment div.
				if ($callRep != $reportID) {
					$str .= oslStructure::openDiv("comment_$reportID",$tabs+1,"comment");
				} else {
					$str .= oslStructure::openDiv("comment_$reportID",$tabs+1,"selectedComment");
				}
				// Output report.
				$str .= "$tabStr		$reportText\n";
				// Close comment div.
				$str .= oslStructure::closeDiv("comment_$reportID",$tabs+1);
				// Display attached images.
				$str .= oslFile::displayFiles($reportID,$content,$step,1,TRUE,$tabs);
				// Display other files.
				$str .= oslFile::displayFiles($reportID,$content,$step,0,TRUE,$tabs);
			}
			// Close overall div.
			$str .= oslStructure::closeDiv("comment_block_$callReport",$tabs);
		}
		// Return.
		return $str;
	}

	// Handle colour background in keyword search.
	static function handleKeywordSearch($keyword,$str)
	{
	 	// If keyword sent.
	 	if($keyword != NULL)
	 	{
			// Explode keyword string.
			$array = explode(" ",$keyword);
			// Loop array.
		 	foreach($array as $key => $val)
			{
			 	// Get colour background.
				$colour = oslStructure::getColour($key);
				// Replace string.
				//$str = str_replace("$val", "<font color=\"$colour\">$val</font>", $str);
				$pattern = "/(".str_replace("/", "\\/",$val).")/i";
				$str = preg_replace($pattern, "<font color=\"$colour\">$1</font>", $str);
			}
		}
		// Return.
		return $str;
	}
	// Ouput manage report.
	static function outputManageReport($callUser,$tabs)
	{
		// Get variables set elsewhere.
		global $username,$content,$step,$callRep,$addCommentTo,$userID,$callEditor,$showTags,$editorNote;
		// Init.
		$parentFK = oslContent::getReportInfo("parentFK",$callRep);
	 	$isComment = FALSE;
	 	$str = NULL;
		// Add number of tabs required.
	 	$tabStr = oslStructure::getRequiredTabs($tabs);
		// If adding a comment.
		if($addCommentTo != NULL)
		{
		 	// Set for image display later on.
		 	$isComment = TRUE;
		 	// Output title.
			$title = "Add comment to ".oslContent::getReportInfo("reportTitle",$addCommentTo);
			// Set default report title.
			$adRepTitle = "Comment to ".oslContent::getReportInfo("reportTitle",$addCommentTo);
			$adRepTaskFK = oslContent::getReportInfo("taskFK",$addCommentTo);
			$adSectionFK = oslContent::getTaskInfo("sectionFK",$adRepTaskFK);
		}
		// If editing a comment.
		elseif($parentFK != 0 && $parentFK != NULL)
		{
		 	// Set for image display later on.
		 	$isComment = TRUE;
		 	// Output title.
			$title = "Add comment to ".oslContent::getReportInfo("reportTitle",$parentFK);
		}
		// Otherwise, managing a full report.
		else
		{
		 	// Output title.
			$title = oslContent::getContentTitle($content);
		}
		// Incorporate title into span.
		$title = "$tabStr<span class=\"sectionHdrWithEd\">$title</span>\n";
		// Open form.
		$title .= "$tabStr<form name=\"manageReport\" id=\"manageReport\" method=\"post\">\n";
		// Add editor options to title before sending.
		$title .= "$tabStr<span class=\"editorOptions\">\n";
		$title .= $tabStr.oslContent::getHelpButton(4,$class);
		$title .= $tabStr."Choose editor: ";
		// Loop through and output different available instances.
		$title .= oslContent::getInstances($callUser,$callRep,$addCommentTo);
		$title .= "\n$tabStr</span>\n";
		// If uploading, ensure this form is closed.
		if($step != NULL)
		{
			$title .= "$tabStr</form>\n";
		}
		// Add report.;
		if($step == NULL)
		{
			$adRepAuthorNames = $user;
			$showReport = TRUE;
			// If the user is editing set the relevent variables.
			if($callRep != NULL)
			{
				$authorFK = oslContent::getReportInfo("authorFK", $callRep);
				$dateStr = oslContent::getReportInfo("dateAdded", $callRep);

				$adRepTitle = oslContent::getReportInfo("reportTitle",$callRep);
				$adRepText = oslContent::getReportInfo("reportText",$callRep);
				$adRepAuthorNames = oslContent::getReportInfo("authorNames",$callRep);
				$adRepTaskFK = oslContent::getReportInfo("taskFK",$callRep);
				$adSectionFK = oslContent::getTaskInfo("sectionFK",$adRepTaskFK);

				// If the author name is empty, default it to the user surname.
				//if($adRepAuthorNames == NULL)
				//{
				//	$adRepAuthorNames = oslUser::getUserInfo("surname",$callUser);
				//	// If still empty, default it to the username.
				//	if($adRepAuthorNames == NULL)
				//	{
				//		$adRepAuthorNames = oslUser::getUserInfo("username",$callUser);
				//	}
				//}

				// Admins can always edit
				// Users can edit iff the the current time < the last edit time OR it is in draft state (postConfirmed == 0)
				if (!oslDAO::canEditReport($callRep,$userID,NULL)) {
					$title = "Permission denied in editing $adRepTitle";
					$str .= "<p>You are not allowed to edit this entry.  Administrators can edit the entry at any point.";
					$str .= "  Authors may edit the entry up to 24 hours after its creation.</p>\n";
					$showReport = FALSE;
				}
			}
			if ($showReport) {
				// Open form.
	    			//$str .= "$tabStr<form name=\"manageReport\" id=\"manageReport\" method=\"post\">\n";
				// Before outputting the form, hide the parentFK if commenting.
				if($parentFK != 0 && $parentFK != NULL && $addCommentTo == NULL)
				{
					// Output parentFK.
					$str .= "<input type=\"hidden\" name=\"addCommentTo\" id=\"addCommentTo\" value=\"$parentFK\" />";
				}
				// Title.
				$str .= "$tabStr<div class=\"formContainer\">\n";
				$str .= "$tabStr	<div class=\"formHdr\">\n";
				$str .= "$tabStr		Title".oslContent::getHelpButton(5,NULL)."\n";

				$str .= "$tabStr	</div>\n";
				$str .= "$tabStr	<div class=\"formVal\">\n";
				$str .= "$tabStr		<input type=\"text\" class=\"reportInput\" id=\"reportTitle\" name=\"reportTitle\" value=\"$adRepTitle\" />\n";
				$str .= "$tabStr	</div>\n";
				$str .= "$tabStr</div>\n";
				// Report Section.
				$str .= "$tabStr<div class=\"formContainer\">\n";
				$str .= "$tabStr	<div class=\"formHdr\">\n";
				$str .= "$tabStr		Section".oslContent::getHelpButton(6,NULL)."\n";
				$str .= "$tabStr	</div>\n";
				$str .= "$tabStr	<div class=\"formVal\">\n";
				$str .= "$tabStr		<select name=\"reportSection\" id=\"reportSection\" onchange=\"updateTasks(0)\">\n";
				// Get sections.
				// $sql = oslDAO::executeQuery("SELECT *
				// 							 FROM tblSections
				// 							 ORDER BY sectionName");

				$sql = "SELECT * FROM tblSections ORDER BY sectionName";
				$types = "";
				$input = array();
				$rows = oslDAO::executePrepared($sql, $types, $input);

				// Loop sections.
				$aC = 0;
				// while($loop = mysqli_fetch_array($sql))
				foreach($rows as $loop)
				{
					// Init.
					$sectionID = $loop["sectionID"];
					$sectionName = $loop["sectionName"];
					// Count number of sections.
					$aC++;
					// If at the first count.
					if($aC == 1)
					{
						// Set the starting section number for the task select box.
						$starterSectionFK = $sectionID;
					}
					// Set sel.
					$sel = NULL;
					if($sectionID == $adSectionFK)
					{
						$sel = " selected";
					}
					// Output options.
					$str .= "$tabStr			<option value=\"$sectionID\"$sel>$sectionName</option>\n";
				}
				$str .= "$tabStr		</select>\n";
				$str .= "$tabStr	</div>\n";
				$str .= "$tabStr</div>\n";
				// Report Tasks.
				$str .= "$tabStr<div class=\"formContainer\">\n";
				$str .= "$tabStr	<div class=\"formHdr\">\n";
				$str .= "$tabStr		";
				if ($showTags) {
                                	$str .= "Primary ";
                                }
				$str .= "Task".oslContent::getHelpButton(7,NULL)."\n";
				$str .= "$tabStr	</div>\n";
				$str .= "$tabStr	<div class=\"formVal\">\n";
				$str .= "$tabStr		<select name=\"reportTask\" id=\"reportTask\">\n";
				// If a section has already been set.
				if($adSectionFK != NULL)
				{
					$selectedSectionFK = $adSectionFK;
				}
				else
				{
					$selectedSectionFK = $starterSectionFK;
				}
				// Get tasks.
				// $get = oslDAO::executeQuery("SELECT *
				// 							 FROM tblTasks
				// 							 $taskSQL
				// 							 ORDER BY taskName");

				$get = "SELECT * FROM tblTasks WHERE sectionFK=? ORDER BY taskName";
				$types = "i";
				$input = array($selectedSectionFK);
				$rows = oslDAO::executePrepared($get, $types, $input);
				// Loop sections.
				// while($tLoop = mysqli_fetch_array($get))
				foreach($rows as $tLoop)
				{
					// Init.
					$taskID = $tLoop["taskID"];
					$taskName = $tLoop["taskName"];
					// Set sel.
					$sel = NULL;
					if($taskID == $adRepTaskFK)
					{
						$sel = " selected";
					}
					// Output options.
					$str .= "$tabStr			<option value=\"$taskID\"$sel>$taskName</option>\n";
				}
				$str .= "$tabStr		</select>\n";
				$str .= "$tabStr	</div>\n";
				$str .= "$tabStr</div>\n";
				if ($showTags) {
                                	$str .= "$tabStr	<div class=\"formContainer\">\n";
					$str .= "$tabStr		<div class=\"formHdr\">Tags (Optional)</div>\n";
					$str .= "$tabStr		<div class=\"formVal\">\n";
				        $str .= oslContent::outputSelectTags($callRep, "reportTag");
					$str .= "$tabStr		</div>\n";
					$str .= "$tabStr	</div>\n";
                                } else {
                                        $str .= oslContent::outputHiddenTags($callRep, "reportTag");
                                }
				if ($editorNote !== NULL) {
                			$str .= "$tabStr	<div class=\"formContainer\">\n";
                			$str .= "$tabStr		<div class=\"formHdr\">Note</div>\n";
                			$str .= "$tabStr		<div class=\"formVal\">\n";
				    	$str .= $editorNote;
					$str .= "$tabStr		</div>\n";
					$str .= "$tabStr	</div>\n";
                		}
				// Report details.
				$str .= "$tabStr<div class=\"formContainer\">\n";
				$str .= oslStructure::getEditor($callUser,$adRepText,$callEditor,$tabs+3);
				$str .= "$tabStr</div>\n";
				// Report author(s).
				$str .= "$tabStr<div class=\"formContainer\">\n";
				$str .= "$tabStr	<div class=\"formHdr\">\n";
				$str .= "$tabStr		Author(s)".oslContent::getHelpButton(8,NULL)."\n";
				$str .= "$tabStr	</div>\n";
				$str .= "$tabStr	<div class=\"formVal\">\n";
	                        // Is this still needed with the shib timeout changes?  should we just use $adRepAuthorNames?
				if (!isset($username) || $username == "") {
					$str .= "$tabStr			<input type=\"text\" name=\"reportAuthors\" id=\"reportAuthors\" class=\"reportInput\" value=\"$username\" >\n";
				} else {
					$str .= "$tabStr			<input type=\"hidden\" name=\"reportAuthors\" id=\"reportAuthors\" value=\"$username\" />\n";
					$str .= "$tabStr			$username\n";
				}
				$str .= "$tabStr	</div>\n";
				$str .= "$tabStr</div>\n";
				// Close form.
				$str .= "$tabStr</form>\n";
				// Output buttons.
				$str .= "$tabStr<div class=\"formContainer\">\n";
				$str .= oslStructure::getButton("btnSaveToDraft","SAVE TO DRAFT",NULL,NULL,NULL,"send(1,'$callRep','$addCommentTo')",$tabs+1);
				$str .= oslStructure::getButton("btnUploadFiles","UPLOAD / MANAGE FILES",NULL,NULL,NULL,"send(2,'$callRep','$addCommentTo')",$tabs+1);
				$str .= oslStructure::getButton("btnPreview","PREVIEW",NULL,NULL,NULL,"send(5,'$callRep','$addCommentTo')",$tabs+1);
				$str .= oslStructure::getButton("btnPostToLogbook","POST TO LOGBOOK",NULL,NULL,NULL,"send(3,'$callRep','$addCommentTo')",$tabs+1);
				// If this report is already saved and is being edited by an Administrator.
				if($callRep != 0 && oslContent::getReportInfo("postConfirmed",$callRep) == TRUE && oslUser::checkIfAdmin($callUser) == TRUE)
				{
					$str .= oslStructure::getButton("btnDelete","DELETE",NULL,NULL,NULL,"requestConfirmation('Are you sure you want to delete this report?','includes/confirmation.php?adminType=deleteReport&adminDel=TRUE&callRep=$callRep')",$tabs+1);
				}
	                        // Add Help button.
				$str .= "$tabStr".oslContent::getHelpButton(9,"Down");
				// Close div.
				$str .= "$tabStr</div>\n";
				$str .= "$tabStr</form>\n";
				// Output a break.
				$str .= oslStructure::getBreak($tabs);
				// Display attached images
				$str .= oslFile::displayFiles($callRep,30,$step,1,$isComment,$tabs);
				// Display other files.
				$str .= oslFile::displayFiles($callRep,30,$step,0,$isComment,$tabs);
				$str .= "<p>To add/remove attachments click the &quot;UPLOAD / MANAGE FILES&quot; button above.</p>\n";
			}
		}
		// Upload files
		elseif($step == 1)
		{
			// Output header.
			$title = "Upload files to <strong>".oslContent::getReportInfo("reportTitle",$callRep)."</strong>";
			// Output upload form.
			$str .= oslFile::upload($content,$step,$callRep,$allowed_file_type,$allowed_img_type,$upload_dir,$addCommentTo,$tabs);
		}
		$str .= oslStructure::getBreak($tabs);
		// Output.
		echo oslStructure::getRoundedContentDiv("divManageReport",$title,$str,$tabs-2);
	}

	// Get instances.
	static function getInstances($callUser,$callRep,$addCommentTo)
	{
	 	// Init.
	 	$str = NULL;
		// Get user's default instance.
		$curUserDef = oslUser::getUserInfo("defaultEditor",$callUser);
		// Get instances.
		// $sql = oslDAO::executeQuery("SELECT *
		// 							 FROM tblValues
		// 							 WHERE valueGroupFK=1");

		$sql = "SELECT * FROM tblValues WHERE valueGroupFK=1";
		$types = "";
		$input = array();
		$rows = oslDAO::executePrepared($sql, $types, $input);
		// Loop.
		// while($loop = mysqli_fetch_array($sql))
		foreach($rows as $loop)
		{
			// Init.
			$valueID = $loop["valueID"];
			$value = $loop["value"];
			// Set default.
			$sel = NULL;
			if($curUserDef == $valueID)
			{
				$sel = " checked";
			}
			$str .=  " <input type=\"radio\" name=\"editorInstance\" id=\"editorInstance\" class=\"inputRadio\" value=\"$valueID\" onchange=\"send(6,'$callRep','$addCommentTo')\" $sel /> $value\n";
		}
		// Return.
		return $str;
	}

	// Output Search
	static function outputSearch($callUser,$callContent,$tabs)
	{
		// Get variables set elsewhere.
		global $searchArray,$defaultDateFormat, $excludeRobots, $showTags;
		// If searching.
		if(!empty($searchArray))
		{
		 	// Loop through array and create variables on the fly.
		 	// foreach($searchArray as $key => $val)
		 	// {
			// 	${$key} = oslDAO::filterForMySQL($val);
			// }
		}
		// Add number of tabs required.
	 	$tabStr = oslStructure::getRequiredTabs($tabs);
		// Output title.
		$title = oslContent::getContentTitle($callContent);
		// Output header.
		$str = oslStructure::getTitleHdr("Enter required search criteria and click on the Search button below",$tabs);
		$str .= "$tabStr<!-- Open Search form -->\n";
		$str .= "$tabStr<form name=\"frmSearch\" id=\"frmSearch\" method=\"post\">\n";
	 	// Section / Task.
		$str .= "$tabStr<div class=\"coverDiv\">\n".
				"$tabStr	<div class=\"coverHdrDiv\">Section / Task</div>\n".
				"$tabStr	<div class=\"coverDetailsDiv\">\n";
		$str .= "$tabStr		<select name=\"reportSection\" id=\"reportSection\" onchange=\"updateTasks(1)\">\n";
		// Output blank.
		$str .= "$tabStr			<option value=\"\"></option>\n";
		// Get sections.
		// $get = oslDAO::executeQuery("SELECT *
		// 							 FROM tblSections
		// 							 ORDER BY sectionName");

		$sql = "SELECT * FROM tblSections ORDER BY sectionName";
		$types = "";
		$input = array();
		$rows = oslDAO::executePrepared($sql, $types, $input);
		// Loop sections.
		// while($loop = mysqli_fetch_array($get))
		foreach($rows as $loop)
		{
			// Init.
			$sectionID = $loop["sectionID"];
			$sectionName = $loop["sectionName"];
			$sel = NULL;
			if($reportSection == $sectionID)
			{
				$sel = " selected";
			}
			// Output options.
			$str .= "$tabStr			<option value=\"$sectionID\"$sel>$sectionName</option>\n";
		}
		$str .= "$tabStr		</select>\n";
		$str .= "$tabStr		<select name=\"reportTask\" id=\"reportTask\">\n";
		$str .= "$tabStr			<option value=\"\"></option>\n";
	 	// If a section has been selected, get all related tasks
		if($reportSection != NULL)
		{
			// Get tasks.
			// $get = oslDAO::executeQuery("SELECT *
			// 							 FROM tblTasks
			// 							 WHERE sectionFK=$reportSection
			// 							 ORDER BY taskName");

			$get = "SELECT * FROM tblTasks WHERE sectionFK=? ORDER BY taskName";
			$types = "i";
			$input = array($reportSection);
			$rows = oslDAO::executePrepared($get, $types, $input);
			// Loop tasks.
			// while($loop = mysqli_fetch_array($get))
			foreach($rows as $loop)
			{
				// Init.
				$taskID = $loop["taskID"];
				$taskName = $loop["taskName"];
				$sel = NULL;
				if($reportTask == $taskID)
				{
					$sel = " selected";
				}
				// Output options.
				$str .= "$tabStr			<option value=\"$taskID\"$sel>$taskName</option>\n";
			}
		}
		$str .= "$tabStr		</select>\n";
		$str .= "$tabStr	</div>\n";
		$str .= "$tabStr</div>\n";
	 	// Report ID.
		$str .= "$tabStr<div class=\"coverDiv\">\n";
		$str .= "$tabStr	<div class=\"coverHdrDiv\">Report ID</div>\n";
		$str .= "$tabStr	<div class=\"coverDetailsDiv\">\n";
		$str .= "$tabStr		<input type=\"text\" name=\"srcCallRep\" id=\"srcCallRep\" value=\"$srcCallRep\" size=\"20\" />\n";
		$str .= "$tabStr	</div>\n";
		$str .= "$tabStr</div>\n";
	 	// Date (from).
		$str .= "$tabStr<div class=\"coverDiv\">\n";
		$str .= "$tabStr	<div class=\"coverHdrDiv\">Date from ($defaultDateFormat)</div>\n";
		$str .= "$tabStr	<div class=\"coverDetailsDiv\">\n";
		$str .= "$tabStr		<input type=\"text\" name=\"srcDateFrom\" id=\"srcDateFrom\" value=\"$srcDateFrom\" size=\"20\" />\n";
		// Get Tigra calendar.
		$str .= oslStructure::getTigraCalendar($tabs+2,"frmSearch","srcDateFrom");
		$str .= "$tabStr	</div>\n";
		$str .= "$tabStr</div>\n";
	 	// Date (to).
		$str .= "$tabStr<div class=\"coverDiv\">\n";
		$str .= "$tabStr	<div class=\"coverHdrDiv\">Date to ($defaultDateFormat)</div>\n";
		$str .= "$tabStr	<div class=\"coverDetailsDiv\">\n";
		$str .= "$tabStr		<input type=\"text\" name=\"srcDateTo\" id=\"srcDateTo\" value=\"$srcDateTo\" size=\"20\" />\n";
		// Get Tigra calendar.
		$str .= oslStructure::getTigraCalendar($tabs+2,"frmSearch","srcDateTo");
		$str .= "$tabStr	</div>\n";
		$str .= "$tabStr</div>\n";
		// Author
		//if($srcAuthorType == 1 || $srcAuthorType == NULL)
		//{
		//	$exactChk = "checked";
		//}
		//elseif($srcAuthorType == 2)
		//{
		//	$anyChk = "checked";
		//}
		//elseif($srcAuthorType == 3)
		//{
		//	$allChk = "checked";
		//}
		$str .= "$tabStr<div class=\"coverDiv\">\n";
		$str .= "$tabStr	<div class=\"coverHdrDiv\">Author(s)</div>\n";
		$str .= "$tabStr	<div class=\"coverDetailsDiv\">\n";
		$str .= "$tabStr		This is the list of all users who have logged into the logbook.<br/>If you wish to lookup more information about a user please see the <a href=\"http://roster.ligo.org/\" target=\"_blank\"> LIGO.ORG roster</a>.<br/>\n";
		$str .= "$tabStr		<select name=\"srcAuthor[]\" id=\"srcAuthor\" multiple=\"multiple\" size=\"15\" >\n";

		$userList = oslUser::getUserList(FALSE);
		foreach ($userList as $userLoop) {
			$str.="$tabStr				<option>".$userLoop["username"]."</option>\n";
		}

		$str .= "$tabStr		</select>\n";

		//$str .= "$tabStr		<input type=\"text\" name=\"srcAuthor\" id=\"srcAuthor\" value=\"$srcAuthor\" size=\"30\" />\n";
		$str .= "$tabStr		<input type=\"hidden\" name=\"srcAuthorType\" id=\"srcAuthorType\" value=\"2\" />\n";
		//$str .= "$tabStr		<input type=\"radio\" name=\"srcAuthorType\" id=\"srcAuthorType\" value=\"1\" class=\"radio\" $exactChk /> Exact string\n";
		//$str .= "$tabStr		<input type=\"radio\" name=\"srcAuthorType\" id=\"srcAuthorType\" value=\"2\" class=\"radio\" $anyChk /> Any\n";
		//$str .= "$tabStr		<input type=\"radio\" name=\"srcAuthorType\" id=\"srcAuthorType\" value=\"3\" class=\"radio\" $allChk /> All\n";
		$str .= "$tabStr	</div>\n";
		$str .= "$tabStr</div>\n";

		if ($excludeRobots) {
				$str .= "$tabStr<div class=\"coverDiv\">\n";
				$str .= "$tabStr	<div class=\"coverHdrDiv\">Exclude Robots</div>\n";
				$str .= "$tabStr	<div class=\"coverDetailsDiv\">\n";
				$str .= "$tabStr		<input type=\"checkbox\" name=\"srcExcludeRobots\" /><label>Exclude robot entries</label>\n";
				$str .= "$tabStr	</div>\n";
				$str .= "$tabStr</div>\n";
		}


		// Keyword
		if($srcKeywordType == 1 || $srcKeywordType == NULL)
		{
			$exactKeyChk = "checked";
		}
		elseif($srcKeywordType == 2)
		{
			$anyKeyChk = "checked";
		}
		elseif($srcKeywordType == 3)
		{
			$allKeyChk = "checked";
		}
		$str .= "$tabStr<div class=\"coverDiv\">\n";
		$str .= "$tabStr	<div class=\"coverHdrDiv\">Keyword(s)</div>\n";
		$str .= "$tabStr	<div class=\"coverDetailsDiv\">\n";
		$str .= "$tabStr		<input type=\"text\" name=\"srcKeyword\" id=\"srcKeyword\" value=\"$srcKeyword\" size=\"30\" />\n";
		$str .= "$tabStr		<input type=\"radio\" name=\"srcKeywordType\" id=\"srcKeywordType\" value=\"1\" class=\"radio\" $exactKeyChk /> Exact string\n";
		$str .= "$tabStr		<input type=\"radio\" name=\"srcKeywordType\" id=\"srcKeywordType\" value=\"2\" class=\"radio\" $anyKeyChk /> Any\n";
		$str .= "$tabStr		<input type=\"radio\" name=\"srcKeywordType\" id=\"srcKeywordType\" value=\"3\" class=\"radio\" $allKeyChk /> All\n";
		$str .= "$tabStr	</div>\n";
		$str .= "$tabStr</div>\n";
		// Tags.
		if ($showTags) {
				$str .= "$tabStr<div class=\"coverDiv\">\n";
				$str .= "$tabStr	<div class=\"coverHdrDiv\">Tag(s)</div>\n";
				$str .= "$tabStr	<div class=\"coverDetailsDiv\">\n";
				$str .= "$tabStr		".oslContent::outputSelectTags(NULL, "srcReportTag");
				$str .= "$tabStr	</div>\n";
				$str .= "$tabStr</div>\n";
				$str .= "$tabStr\n";

				$str .= "$tabStr<div class=\"coverDiv\">\n";
				$str .= "$tabStr	<div class=\"coverHdrDiv\">Attributes(s)</div>\n";
				$str .= "$tabStr	<div class=\"coverDetailsDiv\">\n";
				$str .= "$tabStr		".oslContent::outputSelectAnnotations(NULL, "srcReportTag");
				$str .= "$tabStr	</div>\n";
				$str .= "$tabStr</div>\n";
				$str .= "$tabStr\n";
		}
		// Get buttons.
		$str .= "$tabStr<div class=\"coverDiv\">\n";
		$str .= "$tabStr	<div class=\"coverDetailsDiv\">\n";
		$str .= oslStructure::getButton("btnClear","CLEAR",NULL,NULL,NULL,"requestConfirmation('Are you sure you want to clear this search form?','includes/search.php?adminType=endSearch&content=2')",$tabs+2);
		$str .= oslStructure::getButton("btnSearch","SEARCH","frmSearch","includes/search.php?adminType=search",NULL,NULL,$tabs+2);
		$str .= "$tabStr	</div>\n";
		$str .= "$tabStr</div>\n";
		$str .= "$tabStr<!-- Close Search form -->\n";
		$str .= "$tabStr</form>\n";
		$str .= oslStructure::getBreak($tabs);
		// Output.
		echo oslStructure::getRoundedContentDiv("divSearch",$title,$str,$tabs-2);
	}

	// Output search tasks.
	static function outputSearchTasks($qRepID)
	{
		// Call database.
		// $get = oslDAO::executeQuery("SELECT *
		// 							 FROM tblTasks
		// 							 WHERE sectionFK=$qRepID
		// 							 ORDER BY taskName");

		$get = "SELECT * FROM tblTasks WHERE sectionFK=? ORDER BY taskName";
		$types = "i";
		$input = array($qRepID);
		$rows = oslDAO::executePrepared($get, $types, $input);
		// Decide what value to return dependent upon query result.
		// while($loop = mysqli_fetch_array($get))
		foreach($rows as $loop)
		{
		 	// Init.
			$optText = $loop["taskName"];
			$optVal = $loop["taskID"];
			// Output.
			echo "obj.options[obj.options.length] = new Option('$optText','$optVal');\n";
		}
	}

	// Output update section.
	static function outputSearchSection($qSectID)
	{
		// Call database.
		// $get = oslDAO::executeQuery("SELECT sectionName
		// 	    					 FROM tblSections
		// 	    					 WHERE sectionID=$qSectID");

		$sql = "SELECT sectionName FROM tblSections WHERE sectionID=?";
		$types = "i";
		$input = array($qSectID);
		$get = oslDAO::executePrepared($sql, $types, $input);
		// Set value to return.
		// $val = mysql_result($get,0,"sectionName");
		$val = oslDAO::getFirstResultEntry($get, "sectionName");
		// Output.
		echo "document.getElementById('adSectionName').value = '$val';\n";
	}

    // Output tags
	static function outputAdminTags()
	{
		$str = "";

		// Call database
		// $get = oslDAO::executeQuery("SELECT tagID, tag
		// 							FROM tblTags");

		$get = "SELECT tagID, tag FROM tblTags";
		$types = "";
		$input = array();
		$rows = oslDAO::executePrepared($get, $types, $input);
		// while ($loop = mysqli_fetch_array($get))
		foreach($rows as $loop)
		{
				$confirmStr = "onclick=\"if (confirm('Are you sure you wish to delete this tag?  It will be removed from all reports and drafts.')) { document.forms['frmAdDelTag".$loop["tagID"]."'].submit(); }\"";
				$str .= "<tr><td></td><td>".$loop["tag"]."&nbsp;";
				$str .= "<form id=\"frmAdDelTag".$loop["tagID"]."\" style=\"display: inline\" action=\"includes/confirmation.php?adminType=delTag\" method=\"POST\" ".$confirmStr.">";
				$str .= "<input type=\"hidden\" name=\"adTagID\" value=\"".$loop["tagID"]."\">";
				$str .= "<img src=\"images/delFile.gif\" $confirmStr/></form></td></tr>\n";
		}
		return $str;
	}

	// does not show annotations/system tags
	static function outputReportTags($reportID, $content)
	{
		$str = "";
		// $query = "SELECT tag
		// 					 FROM tblReportTags, tblTags
		// 					 WHERE tblReportTags.reportFK=$reportID AND tblTags.tagID = tblReportTags.tagFK AND tblTags.type = 0
		// 					 ORDER BY tag";
		// $get = oslDAO::executeQuery($query);

		$get = "SELECT tag FROM tblReportTags, tblTags
						WHERE tblReportTags.reportFK=? AND tblTags.tagID = tblReportTags.tagFK
						AND tblTags.type = 0 ORDER BY tag";
		$types = "i";
		$input = array($reportID);
		$rows = oslDAO::executePrepared($get, $types, $input);
		if ($rows === FALSE) {
				//$str .= "<!-- query failed: ".mysql_error()." -->\n";
				//$str .= "<!-- $query -->\n";
				return $str;
		}
		//$prepend = oslStructure::openDiv("reportTag_$reportID",3,"reportTag")."Tags: ";
		// while ($loop = mysqli_fetch_array($get))
		foreach($rows as $loop)
		{
				$str .= $prepend.$loop["tag"];
				$prepend = ", ";
		}
		if ($first === FALSE) {
		//		$str .= oslStructure::closeDiv("reportTag_$reportID",3);
		}
		return $str;
	}

	static function outputSelectTags($callRep, $fieldName) {
		$stride = 10;
		$tabStr="";
		$allTags = oslDAO::getAllTags();
		$reportTags = array();
		if ($callRep !== NULL && $callRep !== 0) {
				$reportTags = oslDAO::getReportTags($callRep);
		}
		$str .= "$tabStr	<table>\n";
		$openTr = FALSE;
		$tagCnt = 0;
		foreach ($allTags as $tagID => $curTag) {
				if ($tagCnt % $stride == 0 || !$openTr) {
						if ($openTr) {
								$str .= "$tabStr		</tr>\n";
						}
						$str .= "$tabStr		<tr>\n";
						$openTr = TRUE;
				}
				$checked = "";
				if (array_key_exists($tagID, $reportTags)) {
						$checked = " checked=\"checked\"";
				}
				$str .= "$tabStr			<td><input type=\"checkbox\" name=\"".$fieldName."[]\" value=\"$tagID\" $checked>$curTag</td>\n";
				$tagCnt ++;
		}
		if ($openTr) {
				$str .= "$tabStr		</tr>\n";
		}
		$str .= "$tabStr	</table>\n";
		return $str;
	}

	static function outputSelectAnnotations($callRep, $fieldName) {
		$stride = 3;
		$tabStr="";
		$allTags = oslDAO::getAllAnnotations();
		$reportTags = array();
		if ($callRep !== NULL && $callRep !== 0) {
				$reportTags = oslDAO::getReportAnnotations($callRep);
		}
		$str .= "$tabStr	<table>\n";
		$openTr = FALSE;
		$tagCnt = 0;
		foreach ($allTags as $tagID => $curTag) {
				if ($tagCnt % $stride == 0 || !$openTr) {
						if ($openTr) {
								$str .= "$tabStr		</tr>\n";
						}
						$str .= "$tabStr		<tr>\n";
						$openTr = TRUE;
				}
				$checked = "";
				if (array_key_exists($tagID, $reportTags)) {
						$checked = " checked=\"checked\"";
				}
				$str .= "$tabStr			<td><input type=\"checkbox\" name=\"".$fieldName."[]\" value=\"$tagID\" $checked>".str_replace("sys:", "", $curTag)."</td>\n";
				$tagCnt ++;
		}
		if ($openTr) {
				$str .= "$tabStr		</tr>\n";
		}
		$str .= "$tabStr	</table>\n";
		return $str;
	}

	static function outputHiddenTags($callRep, $fieldName) {
		$str = "";
		if ($callRep !== NULL && $callRep !== 0) {
				$reportTags = oslDAO::getReportTags($callRep);
				foreach ($reportTags as $tagID => $curTag) {
						$str .= "<input type=\"hidden\" name=\"".$fieldName."[]\" value=\"$tagID\" >\n";
				}
		}
		return $str;
	}

	// Output Help.
	static function outputHelp($callContent,$tabs)
	{
	 	// Get variables set elsewhere.
	 	global $callHelp;
		// Output title.
		$title = oslContent::getContentTitle($callContent);
		// Add number of tabs required.
	 	$tabStr = oslStructure::getRequiredTabs($tabs);
		// Output contents.
		$str .= "$tabStr	<p><img src=\"images/greenSquareShadowed.gif\" alt=\"\" class=\"greenSquareShadowed\" />This section of the OSLogbook provides an exhaustive list of Help topics. For further detailed information, please refer to the <a href=\"https://tds.ego-gw.it/ql/?c=7631\">User Guide</a>.</p>\n";
		// Get all Help items.
//		$str .= oslContent::getAllHelpItems(0,$callHelp,NULL,$tabs+1);
        $str .= oslContent::getAllHelpItems(0,$callHelp,NULL);
		// Output.
		echo oslStructure::getRoundedContentDiv("divSearch",$title,$str,$tabs);
	}

    // Output the LIGO style guide
	static function outputStyleGuide($callContent, $tabs)
	{
		// Add the number of tabs required
		$tabStr = oslStructure::getRequiredTabs($tabs);
		$str = "$tabStr<p>The aLIGO logbook style guide is found in the LIGO DCC at <a target=\"_blank\" href=\"https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?docid=13879\">T000037 (opens in a new window)</a>.";
		echo $str;
	}

	// Output the Links page
	static function outputLinksPage($callContent, $tabs)
	{
		$tabStr = oslStructure::getRequiredTabs($tabs);
		$str = "<ul><li><a href=\"https://alog.ligo-wa.caltech.edu/aLOG/\">LHO aLOG</a></li><li><a href=\"https://alog.ligo-la.caltech.edu/aLOG/\">LLO aLOG</a></li><li><a href=\"https://logbook.virgo-gw.eu/virgo/\">Virgo aLOG</a></li><li><a href=\"https://klog.icrr.u-tokyo.ac.jp/osl/\">KAGRA</a></li></ul>";
		$html =  oslStructure::getRoundedContentDiv("divSearch", "Logbooks", $str, $tabs);
		$feedURL = "http".($_SERVER["HTTPS"]?"s":"")."://".$_SERVER["HTTP_HOST"].pathinfo($_SERVER["PHP_SELF"],  PATHINFO_DIRNAME )."/rss-feed.php";
		$str = "<p>The aLOG content is made available as a RSS feed as well.  The URL for the RSS feed is <a href=\"$feedURL\">rss-feed.php</a>.</p>";
		$html .= oslStructure::getRoundedContentDiv("divSearch", "Other Links", $str, $tabs);
		echo $html;
	}

	// Output Admin section.
	static function outputAdmin($callUser,$callContent,$tabs)
	{
	 	// Get variables set elsewhere.
	 	global $callFileType, $showTags;
	 	// Check if Admin
		if(oslUser::checkIfAdmin($callUser))
		{
			// Add number of tabs required.
		 	$tabStr = oslStructure::getRequiredTabs($tabs);
			// Output title.
			$title = oslContent::getContentTitle($callContent);
			// Output header.
			$str .= "$tabStr<p><img src=\"images/default-theme/SquareShadowed.gif\" alt=\"\" class=\"greenSquareShadowed\" />This section provides site administrators with the possibility to manage users, sections and tasks.</p>\n";
			$str .= "$tabStr<!-- Open application form -->\n";
			$str .= "$tabStr<form name=\"frmAppName\" id=\"frmAppName\" method=\"post\">\n";
			$str .= "$tabStr<table cellpadding=\"0\" cellspacing=\"1\" border=\"0\" class=\"adminTable\">\n";
			// Application info.
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td colspan=\"3\" class=\"tdHdr\"><strong>Application info</strong></td>\n";
			$str .= "$tabStr	</tr>\n";
			// Application data.
			$appName = oslApp::getAppInfo("appName");
			$appOpen = oslApp::getAppInfo("appOpen");
			$appRestrict = oslApp::getAppInfo("appRestrict");
			$appLoginMethod = oslApp::getAppInfo("appLoginMethod");
			$appMailDomain = oslApp::getAppInfo("appMailDomain");
			$appUseThumbnails = oslApp::getAppInfo("appUseThumbnails");
			$appMaxFileSize = oslApp::getAppInfo("appMaxFileSize");
			$ldapHost = oslApp::getAppInfo("ldapHost");
			$ldapDN = oslApp::getAppInfo("ldapDN");
			if($appMailDomain == NULL)
			{
				$mailDomainNote = " (No domain specified - Using: ".oslApp::getDomain().")";
			}
			// File type values.
			if($callFileType != NULL)
			{
				$adFileType = oslFile::getFileTypeDetails($callFileType,"fileType");
				$adFileMimeType = oslFile::getFileTypeDetails($callFileType,"fileMimeType");
				$adFileImage = oslFile::getFileTypeDetails($callFileType,"fileImage");
			}
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td class=\"tdHdr\">Application Name</td>\n";
			$str .= "$tabStr		<td colspan=\"2\" class=\"tdVal\"><input name=\"adAppName\" id=\"adAppName\" value=\"$appName\" size=\"30\" /></td>\n";
			$str .= "$tabStr	</tr>\n";
			// Mail notification domain.
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td class=\"tdHdr\">E-mail domain</td>\n";
			$str .= "$tabStr		<td colspan=\"2\" class=\"tdVal\">@<input name=\"adAppMailDomain\" id=\"adAppMailDomain\" value=\"$appMailDomain\" size=\"30\" />$mailDomainNote</td>\n";
			$str .= "$tabStr	</tr>\n";
			// Open access or closed?
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td class=\"tdHdr\">Access</td>\n";
			$str .= "$tabStr		<td colspan=\"2\" class=\"tdVal\">\n";
			// Check the default value for the select.
			if($appOpen == 0)
			{
				$defClosed = " selected";
			}
			elseif($appOpen == 1)
			{
				$defOpen = " selected";
			}
			$str .= "$tabStr			<select name=\"adAppAccess\" id=\"adAppAccess\">\n";
			$str .= "$tabStr				<option value=\"0\"$defClosed>Closed</option>\n";
			$str .= "$tabStr				<option value=\"1\"$defOpen>Open</option>\n";
			$str .= "$tabStr			</select>\n";
			$str .= "$tabStr		</td>\n";
			$str .= "$tabStr	</tr>\n";
			// Restrict access to set user group?
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td class=\"tdHdr\">Restrict access to specific users?</td>\n";
			$str .= "$tabStr		<td colspan=\"2\" class=\"tdVal\">\n";
			// Check the default value for the select.
			if($appRestrict == 0)
			{
				$defUnrestricted = " selected";
			}
			elseif($appRestrict == 1)
			{
				$defRestricted = " selected";
				// Set link to list of users.
				$resTxt = " <a href=\"index.php?content=8\">View list of users with access to this Logbook</a>.";
			}
			$str .= "$tabStr			<select name=\"adAppRestrict\" id=\"adAppRestrict\">\n";
			$str .= "$tabStr				<option value=\"0\"$defUnrestricted>No</option>\n";
			$str .= "$tabStr				<option value=\"1\"$defRestricted>Yes</option>\n";
			$str .= "$tabStr			</select>$resTxt\n";
			$str .= "$tabStr		</td>\n";
			$str .= "$tabStr	</tr>\n";
			// Authentication info.
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td colspan=\"3\" class=\"tdHdr\"><strong>Authentication info</strong></td>\n";
			$str .= "$tabStr	</tr>\n";
			// Decide log-in type
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td class=\"tdHdr\">Log-in method</td>\n";
			$str .= "$tabStr		<td colspan=\"2\" class=\"tdVal\">\n";
			// Check the default value for the select.
			if($appLoginMethod == 0)
			{
				$defDB = " selected";
			}
			elseif($appLoginMethod == 1)
			{
				$defUNIX = " selected";
			}
			elseif($appLoginMethod == 2)
			{
				$defLDAP = " selected";
			}
			elseif($appLoginMethod == 3)
			{
				$defApache = " selected";
			}
			$str .= "$tabStr			<select name=\"adAppLoginMethod\" id=\"adAppLoginMethod\">\n";
			$str .= "$tabStr				<option value=\"3\"$defApache>Apache</option>\n";
			$str .= "$tabStr				<option value=\"0\"$defDB>Internal DB</option>\n";
			$str .= "$tabStr				<option value=\"2\"$defLDAP>LDAP</option>\n";
			$str .= "$tabStr				<option value=\"1\"$defUNIX>UNIX</option>\n";
			$str .= "$tabStr			</select>\n";
			$str .= "$tabStr		</td>\n";
			$str .= "$tabStr	</tr>\n";
			// LDAP Host.
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td class=\"tdHdr\">LDAP Host</td>\n";
			$str .= "$tabStr		<td colspan=\"2\" class=\"tdVal\"><input name=\"adLdapHost\" id=\"adLdapHost\" value=\"$ldapHost\" size=\"30\" /></td>\n";
			$str .= "$tabStr	</tr>\n";
			// LDAP DN.
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td class=\"tdHdr\">LDAP DN</td>\n";
			$str .= "$tabStr		<td colspan=\"2\" class=\"tdVal\"><input name=\"adLdapDN\" id=\"adLdapDN\" value=\"$ldapDN\" size=\"30\" /></td>\n";
			$str .= "$tabStr	</tr>\n";
			// File upload info.
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td colspan=\"3\" class=\"tdHdr\"><strong>File upload info</strong></td>\n";
			$str .= "$tabStr	</tr>\n";
			// Use thumbnails?
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td class=\"tdHdr\">Use thumbnails?</td>\n";
			$str .= "$tabStr		<td colspan=\"2\" class=\"tdVal\">\n";
			// If the GD library is available on this server.
			if(oslApp::checkForGD())
			{
				// Check the default value for the select.
				if($appUseThumbnails == 0)
				{
					$defNoUseTn = " selected";
				}
				elseif($appUseThumbnails == 1)
				{
					$defUseTn = " selected";
				}
				$str .= "$tabStr			<select name=\"adAppUseThumbnails\" id=\"adAppUseThumbnails\">\n";
				$str .= "$tabStr				<option value=\"0\"$defNoUseTn>No</option>\n";
				$str .= "$tabStr				<option value=\"1\"$defUseTn>Yes</option>\n";
				$str .= "$tabStr			</select> The GD library required in thumbnail creation has been found on this server.\n";
			}
			// Otherwise, if GD library not found.
			else
			{
				$str .= "$tabStr	<img src=\"images/attention.gif\" alt=\"Attention\" class=\"moveDown\" /> The GD library required in thumbnail creation has not been found on this server. To activate this feature PHP on this server must be re-compiled with the GD library. <a href=\"http://www.php.net/manual/en/book.image.php\" target=\"_blank\">More information is available here</a>.\n";
			}
			$str .= "$tabStr		</td>\n";
			$str .= "$tabStr	</tr>\n";
			// Maximum file size.
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td class=\"tdHdr\">Maximum file size</td>\n";
			$str .= "$tabStr		<td colspan=\"2\" class=\"tdVal\"><input name=\"adAppMaxFileSize\" id=\"adAppMaxFileSize\" value=\"$appMaxFileSize\" size=\"15\" /> bytes</td>\n";
			$str .= "$tabStr	</tr>\n";
			// Uploadable file types.
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td class=\"tdHdr\">Uploadable file types<br />(click on a file type to edit it)</td>\n";
			$str .= "$tabStr		<td colspan=\"2\" class=\"tdVal\">\n";
			$str .= oslFile::outputUploadableTypes(TRUE,6);
			$str .= "$tabStr		</td>\n";
			$str .= "$tabStr	</tr>\n";
			// Add new uploadable file type.
			$str .= "$tabStr	<tr>\n";
			// Set header.
			$ftHdr = "Add new";
			if($callFileType != NULL)
			{
				$ftHdr = "Edit";
			}
			$str .= "$tabStr		<td class=\"tdHdr\">$ftHdr file type</td>\n";
			$str .= "$tabStr		<td colspan=\"2\" class=\"tdVal\">.<input name=\"adFileType\" id=\"adFileType\" value=\"$adFileType\" size=\"10\" /> Mime type: <input name=\"adFileMimeType\" id=\"adFileMimeType\" value=\"$adFileMimeType\" size=\"15\" />\n";
			// Check the default value for the select.
			if($adFileImage == 0)
			{
				$defNotImg = " selected";
			}
			elseif($adFileImage == 1)
			{
				$defImg = " selected";
			}
			$str .= "$tabStr				<select name=\"adFileImage\" id=\"adFileImage\">\n";
			$str .= "$tabStr					<option value=\"0\"$defNotImg>Not image type</option>\n";
			$str .= "$tabStr					<option value=\"1\"$defImg>Image type</option>\n";
			$str .= "$tabStr				</select>\n";
			// Output delete option if editing.
			if($callFileType != NULL)
			{
				$str .= "$tabStr	<a href=\"includes/confirmation.php?adminType=delFileType&callFileType=$callFileType\" onclick=\"return confirm('Are you sure you want to delete the file type:\\n\\n$adFileType\\n\\n?')\"><img src=\"images/delFile.gif\" alt=\"\" /></a>\n";
			}
			$str .= "$tabStr			</td>\n";
			$str .= "$tabStr	</tr>\n";
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td>\n";
			$str .= oslStructure::getButton("btnSaveAdminInfo","SAVE APP., AUTH. & FILE INFO","frmAppName","includes/confirmation.php?adminType=editAppName&callFileType=$callFileType",NULL,NULL,$tabs+2);
			$str .= "$tabStr		</td>\n";
			$str .= "$tabStr		<td colspan=\"2\" class=\"tdBtn\"></td>\n";
			$str .= "$tabStr	</tr>\n";
			// Sections.
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td colspan=\"3\" class=\"tdHdr\"><strong>Sections</strong></td>\n";
			$str .= "$tabStr	</tr>\n";
			// Edit section.
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td class=\"tdHdr\">Select section to edit</td>\n";
			$str .= "$tabStr		<td class=\"tdVal\">\n";
			// Get existing sections.
			$str .= "$tabStr			<select name=\"edSectionID\" id=\"edSectionID\" onchange=\"editSection()\">\n";
			// Output blank.
			$str .= "$tabStr					<option value=\"\"></option>\n";
			$str .= oslStructure::outputSectionsAsOpt("sectionID","sectionName");
			$str .= "$tabStr			</select>\n";
			$str .= "$tabStr		</td>\n";
			$str .= "$tabStr		<td class=\"tdBtn\"><image name=\"delSectionButton\" id=\"delSectionButton\" src=\"images/btnDeleteSection.gif\" class=\"hidden\" onclick=\"delSection()\" /></td>\n";
			$str .= "$tabStr	</tr>\n";
			$str .= "$tabStr<!-- Close application form -->\n";
			$str .= "$tabStr</form>\n";
			// Add section.
			// Open table and form.
			$str .= "$tabStr<!-- Open section form -->\n";
			$str .= "$tabStr<form name=\"frmaddSection\" id=\"frmaddSection\" method=\"post\" >\n";
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td class=\"tdHdr\"><div id=\"sectionTxt\" name=\"sectionTxt\">Add section name</div></td>\n";
			$str .= "$tabStr		<td class=\"tdVal\"><input name=\"adSectionName\" id=\"adSectionName\" value=\"\" size=\"40\" onKeyDown=\"return processEnterKey(event, 'saveButton')\"/></td>\n";
			// Close form.
			$str .= "$tabStr<!-- Close application form -->\n";
			$str .= "$tabStr</form>\n";
			$str .= "$tabStr		<td class=\"tdBtn\"><image name=\"saveButton\" id=\"saveButton\" src=\"images/btnSaveSection.gif\" class=\"buttonVisible\" onclick=\"updateSectionName()\" /></td>\n";
			$str .= "$tabStr	</tr>\n";
			// Tasks.
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td colspan=\"3\" class=\"tdHdr\"><div id=\"taskHdr\" name=\"taskHdr\"></div></td>\n";
			$str .= "$tabStr	</tr>\n";
			// Edit Task.
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td class=\"tdHdr\"><div name=\"taskHdrB\" id=\"taskHdrB\"></div></td>\n";
			$str .= "$tabStr		<td class=\"tdVal\">\n";
			// Get existing tasks.
			$str .= "$tabStr			<select name=\"reportTask\" id=\"reportTask\" onchange=\"editTask()\" class=\"hidden\">\n";
			$str .= "$tabStr				<option value=\"\" selected></option>\n";
			$str .= "$tabStr			</select>\n";
			$str .= "$tabStr		</td>\n";
			$str .= "$tabStr		<td class=\"tdBtn\">\n";
			$str .= "$tabStr			<form name=\"frmdeleteTaskSingle\" method=\"post\" action=\"includes/confirmation.php?adminType=delTask\" onSubmit=\"return validateDeleteTaskForm()\">\n";
			$str .= "$tabStr				<input type=\"hidden\" name=\"reportTask\" value=\"\" />\n";
			$str .= "$tabStr				<input type=\"hidden\" name=\"edSectionID\" value=\"\" />\n";
			$str .= "$tabStr				<input type=\"hidden\" name=\"adTaskName\" value=\"\" />\n";
			$str .= "$tabStr				<input name=\"delTaskBtn\" id=\"delTaskBtn\" type=\"image\" src=\"images/btnDeleteTask.gif\" class=\"hidden\" onclick=\"submitDeleteTaskForm()\" />\n";
			$str .= "$tabStr			</form>\n";
			$str .= "$tabStr			\n";
			$str .= "$tabStr		</td>\n";
			$str .= "$tabStr	</tr>\n";
			// Add task.
			// Open form.
			$str .= "$tabStr<!-- Open task form -->\n";
			$str .= "$tabStr<form name=\"frmaddTask\" id=\"frmaddTask\" method=\"post\" onSubmit=\"submitUpdateTaskForm(); return false;\">\n";
			$str .= "$tabStr	<tr>\n";
			$str .= "$tabStr		<td class=\"tdHdr\"><div id=\"taskTxt\" name=\"taskTxt\"></div></td>\n";
			$str .= "$tabStr		<td class=\"tdVal\"><input name=\"adTaskName\" id=\"adTaskName\" value=\"\" size=\"40\" class=\"hidden\" /></td>\n";
			// Close form.
			$str .= "$tabStr<!-- Close task form -->\n";
			$str .= "$tabStr</form>\n";
			$str .= "$tabStr		<td class=\"tdBtn\">\n";
			$str .= "$tabStr			<!-- open add task form -->\n";
			// open new update form
			$str .= "$tabStr			<form name=\"frmupdateTaskSingle\" method=\"post\" action=\"includes/confirmation.php?adminType=addTask\" onSubmit=\"return validateUpdateTaskForm()\">\n";
			$str .= "$tabStr				<input type=\"hidden\" name=\"reportTask\" value=\"\" />\n";
			$str .= "$tabStr				<input type=\"hidden\" name=\"edSectionID\" value=\"\" />\n";
			$str .= "$tabStr				<input type=\"hidden\" name=\"adTaskName\" value=\"\" />\n";
			$str .= "$tabStr				<input name=\"saveTaskButton\" id=\"saveTaskButton\" type=\"image\" src=\"images/btnSaveTask.gif\" class=\"hidden\" onclick=\"submitUpdateTaskForm()\" />\n";
			// close new update form
			$str .= "$tabStr			<!-- close add task form -->\n";
			$str .= "$tabStr			</form>\n";
			$str .= "$tabStr		</td>\n";
			$str .= "$tabStr	</tr>\n";
			// Tags.
		    if ($showTags) {
				$str .= "$tabStr	<tr>\n";
				$str .= "$tabStr		<td colspan=\"3\" class=\"tdHdr\"><strong>Tags</strong></td>\n";
				$str .= "$tabStr	</tr>\n";
				$str .= oslContent::outputAdminTags();
				//<img src=\"images/delFile.gif\" onclick=\"return confirm('Are you sure you wish to delete this draft report?')\" />
				$str .= "$tabStr	<tr>\n";
				$str .= "$tabStr		<td></td>\n";
				$str .= "$tabStr		<td>\n";
				$str .= "$tabStr			<form action=\"includes/confirmation.php?adminType=addTag\" name=\"frmaddTag\" id=\"frmaddTag\" method=\"post\">\n";
				$str .= "$tabStr			<input type=\"text\" name=\"adTagName\" id=\"adTagName\">\n";
				//$str .= "$tabStr			<input type=\"submit\" value=\"ADD TAG\">\n";
				$str .= "$tabStr			</form>\n";
				$str .= "$tabStr		</td>\n";
				$str .= "$tabStr		<td>\n";
				$str .= oslStructure::getButton("btnAddTag", "ADD TAG", "frmaddTag", "includes/confirmation.php?adminType=addTag", NULL, NULL, $tabs+2);
				$str .= "$tabStr		</td>\n";
				$str .= "$tabStr	</tr>\n";
			}
			// Close table.
			$str .= "$tabStr</table>\n";
			// Output.
			echo oslStructure::getRoundedContentDiv("divAdmin",$title,$str,$tabs-2);
		}
	}

	// Get help button.
	static function getHelpButton($callHelp,$class)
	{
		// Set button.
		$btn = "<img src=\"images/question_mark.gif\" class=\"helpBtn$class\" onclick=\"javascript:window.open('show_help.php?callHelp=$callHelp','blank','toolbar=yes,width=400,height=400,resizable=no')\" />\n";
		// Return.
		return $btn;
	}

	// Get help item.
	static function getHelpItem($callHelp)
	{
		// Get help item.
		// $sql = oslDAO::executeQuery("SELECT *
		// 					 		 FROM tblHelp
		// 					 		 WHERE helpID=$callHelp");

		$sql = "SELECT * FROM tblHelp WHERE helpID=?";
		$types = "i";
		$input = array($callHelp);
		$rows = oslDAO::executePrepared($sql, $types, $input);
		// Init.
		// $helpTitle = mysql_result($sql,0,"helpTitle");
		// $helpText = nl2br(mysql_result($sql,0,"helpTxt"));
		// $helpImgA = nl2br(mysql_result($sql,0,"helpImgA"));

		$helpTitle = oslDAO::getFirstResultEntry($rows, "helpTitle");
		$helpText = nl2br(oslDAO::getFirstResultEntry($rows, "helpTxt"));
		$helpImgA = nl2br(oslDAO::getFirstResultEntry($rows, "helpImgA"));
		// Set contents.
		$content = "<p class=\"title\">$helpTitle</p>\n";
		// If there is an initial image, output it here.
		if($helpImgA != NULL)
		{
				$content .= "<p><img src=\"img/$helpImgA\" class=\"helpImg\" /></p>\n";
		}
		$content .= "<p>$helpText</p>\n";
		// Return.
		return $content;
	}

	// Get all Help items.
	static function getAllHelpItems($callHelpParentFK,$callHelp,$helpRefLetter)
	{
	 	// Init.
	 	$content = NULL;
	 	$a = 0;
		// Add number of tabs required.
	 	$tabStr = oslStructure::getRequiredTabs($tabs);
		// Get help items.
		// $sql = oslDAO::executeQuery("SELECT *
		// 							 FROM tblHelp
		// 					 		 WHERE helpParentFK=$callHelpParentFK
		// 					 		 ORDER BY helpRefLetter, helpTitle");

		$sql = "SELECT * FROM tblHelp WHERE helpParentFK=? ORDER BY helpRefLetter, helpTitle";
		$types = "i";
		$input = array($callHelpParentFK);
		$rows = oslDAO::executePrepared($sql, $types, $input);
		// Loop.
		// while($loop = mysqli_fetch_array($sql))
		foreach($rows as $loop)
		{
		 	// Init.
			$class = NULL;
		 	$a++;
			// If at top level set as subtitle.
		 	if($callHelpParentFK == 0)
		 	{
				$class = " class=\"subtitle\"";
			 	$helpRefLetter = $loop["helpRefLetter"];
			 	$helpRefLetterOutput = $helpRefLetter;
			}
			else
			{
			 	$helpRefLetterOutput = "$helpRefLetter.$a";
			}
		 	$helpID = $loop["helpID"];
		 	// Init.
		 	$helpTitle = $loop["helpTitle"];
			// If not at top level set title to link.
		 	if($callHelpParentFK != 0 && $callHelp != $helpID)
		 	{
				$helpTitle = "<a href=\"index.php?content=4&callHelp=$helpID$searchStr#help_$helpID\">$helpTitle</a>";
			}
			// Set called Help item to bold.
			if($callHelp == $helpID)
		 	{
				$class = " class=\"subtitle\"";
			}
			// Set content.
			$content .= "$tabStr<p$class><a name=\"help_$helpID\"></a>$helpRefLetterOutput $helpTitle</p>\n";
			// If the Help item is being called, output it here.
			if($callHelp == $helpID)
			{
			 	$helpTxt = nl2br($loop["helpTxt"]);
			 	$helpImgA = $loop["helpImgA"];
			 	// Set img if applicable.
			 	if($helpImgA != NULL)
			 	{
					$content .= "$tabStr<p><img src=\"img/$helpImgA\" class=\"helpImg\" /></p>\n";
				}
				$content .= "$tabStr<p>$helpTxt</p>\n".
							"$tabStr<p>&nbsp;</p>\n";
			}
			// If at top level indent child Help items.
		 	if($callHelpParentFK == 0)
		 	{
				$content .= "<ul>";
			}
			// Get sub-content.
			$content .= oslContent::getAllHelpItems($helpID,$callHelp,$helpRefLetter);
			// If at top level un-indent child Help items.
		 	if($callHelpParentFK == 0)
		 	{
				$content .= "</ul>";
			}
		}
		// Return.
		return $content;
	}

}

?>
