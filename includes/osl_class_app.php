<?php
/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/

/////////////////////////////////
// OSLogbook application info //
///////////////////////////////

class oslApp
{
	// Output application name and logo.
	static function getAppInfo($field)
	{
		// Get contents to be output in top links.
		$rows = oslDAO::executePrepared("SELECT $field
									 FROM tblAppInfo
									 WHERE appID=1", "", array());
		// Init.
		return oslDAO::getFirstResultEntry($rows, $field);
	}

	// Output application name and logo.
	static function getAppTitle($field)
	{
		// Get contents to be output in top links.
		$rows = oslDAO::executePrepared("SELECT $field
									 FROM tblAppInfo
									 WHERE appID=1", "", array());
		// Init.
		$val = oslDAO::getFirstResultEntry($rows, $field);
	 	// Output application name.
		echo "				<div class=\"logoTitle\">$val </div><img src=\"images/lForLogbook.gif\" class=\"logo\" /><div class=\"logoTitle\">logbook</div>\n";
	}

	// Get mail domain for use in sending Mail Notifications.
	static function getDomain()
	{
		// Check if domain exists in database.
		$appMailDomain = oslApp::getAppInfo("appMailDomain");
		// If the domain is not currently set, try to re-build it.
		if($appMailDomain == NULL)
		{
			$appMailDomainExp = explode(".",$_SERVER["SERVER_NAME"]);
			$appMailDomain = $appMailDomainExp[1].".".$appMailDomainExp[2];
		}
		// Return domain.
		return $appMailDomain;
	}

	// Edit application info.
	static function editAppInfo($userID,$callFileType,$adAppName,$adAppAccess,$adAppLoginMethod,$adAppMailDomain,$adAppMaxFileSize,$adLdapHost,$adLdapDN,$adFileType,$adFileMimeType,$adFileImage,$adAppRestrict,$adAppUseThumbnails)
	{
	 	// Init.
	 	$url = "../index.php?content=7";
	 	// If the user is Admin.
	 	if(oslUser::checkIfAdmin($userID))
	 	{
			// Update app info.
			oslDAO::executePrepared("UPDATE tblAppInfo
						 SET appName = ?, appOpen = ?, appRestrict = ?, appLoginMethod = ?, appMailDomain = ?, appMaxFileSize = ?, appUseThumbnails = ?, ldapHost = ?, ldapDN = ?
						 WHERE appID = 1", "siiisssss", array($adAppName,$adAppAccess,$adAppRestrict,$adAppLoginMethod, $adAppMailDomain, $adAppMaxFileSize, $adAppUseThumbnails, $adLdapHost, $adLdapDN));
			// If a new file type has been added
			if($adFileType != NULL && $adFileType != '')
			{
				// If adding.
				if($callFileType == NULL)
				{
					// Insert new file type.
					oslDAO::executePrepared("INSERT INTO tblFileTypes
							 (fileType, fileMimeType, fileImage)
							 VALUES
							 (?, ?, ?)", "sss", array($adFileType,$adFileMimeType,$adFileImage));
				}
				// Otherwise.
				else
				{
					// Update file type.
					oslDAO::executePrepared("UPDATE tblFileTypes
								 SET fileType=?, fileMimeType=?, fileImage=?
								 WHERE fileTypeID=?",
								"ssii", array($adFileType, $adFileMimeType, $adFileImage, $callFileType));
				}
			}
		}
		// Return.
		return $url;
	}

	// Check for GD library on server. Used in thumbnail production.
	static function checkForGD()
	{
	 	// Init.
		$exists = FALSE;
		// Check if gd_info function exists.
		if(function_exists("gd_info"))
		{
			// Set exists to true.
			$exists = TRUE;
		}
		// Return.
		return $exists;
	}
}

?>
