<?php
/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/

// Initialise general variables.
require_once("initVar.php");

// If logged in.
if($userID == TRUE)
{
	///////////////////
	// EXPIRED AUTH //
	/////////////////

	if ($authExpired) {
		// when we have an expired credential everything redirects to the index page.
		//$redirectURL = "../index.php?authExpired=1";
		$redirectURL = "../index.php";
		// handling saving (ie posting/save to draft)
		if ($adminType == "saveReport") {
			// force all save report actions to save to draft
			$nextStep = 1;

			oslDAO::saveReport($callRep,$userID,$nextStep,$addCommentTo,$appURL);

			//die ($redirectURL);
		}
		elseif($adminType == "upload")
		{
			oslFile::uploadFile($content,$step,$callRep,$addCommentTo);
		}
		//session_destroy();
	}

 	///////////////////
	// REPORT CALLS //
	/////////////////

	// Save report.
	elseif($adminType == "saveReport")
	{
	 	// Save the report.
		$redirectURL = oslDAO::saveReport($callRep,$userID,$nextStep,$addCommentTo,$appURL);
	}
	// Delete report.
	elseif($adminType == "deleteReport")
	{
	 	// Delete the report.
		$redirectURL = oslDAO::deleteReport($callRep,$userID,$nextStep,$upload_dir,$content,$appDir);
	}

 	/////////////////
	// FILE CALLS //
	///////////////

	// Delete file.
	elseif($adminType == "delFile")
	{
		// Delete specific file.
		oslFile::deleteFile($callRep,$appDir,$upload_dir,$callFile);
		// Re-direct.
		$redirectURL = "../index.php?content=3&step=1&callRep=$callRep";
	}
	// Upload file.
	elseif($adminType == "upload")
	{
		$redirectURL = oslFile::uploadFile($content,$step,$callRep,$addCommentTo);
	}
	// Delete file type.
	elseif($adminType == "delFileType")
	{
		$redirectURL = oslFile::deleteFileType($userID,$callFileType);
	}


	/////////////////
	// USER CALLS //
	///////////////

	// Add User.
	elseif($adminType == "AddUser")
	{
		$redirectURL = oslUser::addUser($userID,$_POST);
	}
	// Edit User.
	elseif($adminType == "EditUser")
	{
		$redirectURL = oslUser::updateUser($userID,$_POST,$callUser);
	}
	// Delete User.
	elseif($adminType == "delUser")
	{
		$redirectURL = oslUser::deleteUser($userID,$callUser);
	}
	// Edit user email address.
	elseif($adminType == "editUserEmail")
	{
		$redirectURL = oslUser::updateUserEmail($userID,$_POST);
	}
	// Edit user mail notifications.
	elseif($adminType == "editMailNotification")
	{
		$redirectURL = oslUser::mailNotification($userID,$_POST);
	}

	//////////////////
	// ADMIN CALLS //
	////////////////

	// Add new section.
	elseif($adminType == "addSection")
	{
		$redirectURL = oslDAO::insertSection($userID,$edSectionID,$adSectionName);
	}

	// Delete section.
	elseif($adminType == "delSection")
	{
		$redirectURL = oslDAO::deleteSection($userID,$edSectionID);
	}

	// Add new task.
	elseif($adminType == "addTask")
	{
		$redirectURL = oslDAO::editTask($userID,$edSectionID,$adTaskName,$reportTask);
	}

	// Delete task.
	elseif($adminType == "delTask")
	{
		$redirectURL = oslDAO::deleteTask($userID,$reportTask);
	}

	////////////////
	// APP CALLS //
	//////////////

	// Edit application name
	elseif($adminType == "editAppName")
	{
	 	$redirectURL = oslApp::editAppInfo($userID,$callFileType,$adAppName,$adAppAccess,$adAppLoginMethod,$adAppMailDomain,$adAppMaxFileSize,$adLdapHost,$adLdapDN,$adFileType,$adFileMimeType,$adFileImage,$adAppRestrict,$adAppUseThumbnails);
	}

	////////////////
	// TAG CALLS //
	//////////////

	// Add new tag.
	elseif ($adminType == "addTag")
	{
		$redirectURL = oslDAO::insertTag($userID,NULL,$adTagName);
	}

	// Delete tag.
	elseif ($adminType == "delTag")
	{
		$redirectURL = oslDAO::deleteTag($userID, $adTagID);
	}
	///////////////////
	// SEARCH CALLS //
	/////////////////

	//elseif($adminType == "search")
	//{
	//	// Build the search array.
	//	$_SESSION["searchArray"] = oslContent::buildSearchArray($_POST);
	//	$redirectURL = "../index.php";
	//}
	//elseif($adminType == "endSearch")
	//{
	//	// Unset the search array.
	//	unset($_SESSION["searchArray"]);
	//	$redirectURL = "../index.php?content=$content";
	//}
}
// If not logged in.
else
{
	///////////////////
	// SEARCH CALLS //
	/////////////////

	//if($adminType == "search")
	//{
	//	// Build the search array.
	//	$_SESSION["searchArray"] = oslContent::buildSearchArray($_POST);
	//	$redirectURL = "../index.php";
	//}
	//elseif($adminType == "endSearch")
	//{
	//	// Unset the search array.
	//	unset($_SESSION["searchArray"]);
	//	$redirectURL = "../index.php?content=$content";
	//} else {
		$redirectURL = "../index.php";
	//}
}

// safety/sanity check.
// Infinite looping can happen if an invalid request was passed in.
if (!isset($redirectURL) || $redirectURL == "") {
    $redirectURL = "../index.php";
}

// Redirect.
header("location: $redirectURL");

?>
