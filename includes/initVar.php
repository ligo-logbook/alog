<?php
/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/

//////////////////////
// Handle sessions //
////////////////////

// session cookie setup, with some security options
// see https://www.php.net/manual/en/session.security.ini.php
session_start( [
    //'cookie_lifetime' => 86400,
    'use_cookies' => 1,
    'use_only_cookies' => 1, // ONLY use cookies, not GET/POST/URL params
    'use_strict_mode' => 1, // prevents misuse of uninit'd session ID
    'cookie_httponly' => 1, // cookies only available in connection, not to JS
    'cookie_secure' => 1, // only access via HTTPS. consider adding HSTS too
    'cookie_samesite' => 'Strict', // prevent CSRF
]);

ini_set('display_errors',1);

// Allow for French diacritical marks in Javascript.
header('Content-type: text/html; charset=ISO-8859-1');

// Disable caching.
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past


///////////////////////////
// These settings should be overriden in localVars.php if needed
/////////////////////////

$SETTING_DB_HOST = "localhost";
$SETTING_DB_USER = "osl";
$SETTING_DB_PASSWORD = "osl";
$SETTING_DB_READONLY_USER = "osl-reader";
$SETTING_DB_READONLY_PASSWORD = "osl";
$callDB = "osl";

$SETTINGS_DOC_ROOT = "/var/www/html";

$SETTINGS_UPLOAD_DIR = "uploads/";

// basic settings
$showCommentStandalone = FALSE;		// are comments displayed as their own entities in the report?
$showLoginButtons = TRUE;			// Should the login/logout buttons be shown
$excludeRobots = TRUE;				// Should the search be able to exclude 'robo' entries
$robotList = array();       // list of 'robots' to excludes
$strongFilenameCleaning = FALSE;	// true -> lowercase, removes '_'
$showTags = TRUE;					// are tags and their interface exposed to the user?
$editorNote = "";					// Assign a string value and this will be shown when reports are edited
$sendLMail = TRUE;			// SET to FALSE to disable the L-Mail
$globalNote = NULL;						// Assign a string value and this will be shown on all pages.

//////////////////////////
// End configurable defaults
////////////////////////

require_once("localVars.php");

// Get libraries.
require_once("osl_class_app.php");
require_once("osl_class_messages.php");
require_once("osl_class_content.php");
require_once("osl_class_dao.php");
require_once("osl_class_files.php");
require_once("osl_class_structure.php");
require_once("osl_class_user.php");

// Instantiate classes.
$oslApp = new oslApp;
$oslContent = new oslContent;
$oslDAO = new oslDAO;
$oslFile = new oslFile;
$oslStructure = new oslStructure;
$oslUser = new oslUser;

//////////////////
// Connections //
////////////////

// Connect to server.
$db = $oslDAO::serverConnect($SETTING_DB_HOST, $SETTING_DB_USER, $SETTING_DB_PASSWORD);

// if logged in
if ($_SERVER['REMOTE_USER'] != "") {
	$db = $oslDAO::serverConnect($SETTING_DB_HOST,$SETTING_DB_USER,$SETTING_DB_PASSWORD);
  // Get a mysqli connection as well for new newer access methods
  $dbi = mysqli_connect($SETTING_DB_HOST, $SETTING_DB_USER, $SETTING_DB_PASSWORD, $callDB);
  if (!$dbi) {
  	die("Unable to connect to database");
  }
} else { // not logged in
	$db = $oslDAO::serverConnect($SETTING_DB_HOST,$SETTING_DB_READONLY_USER,$SETTING_DB_READONLY_PASSWORD);
  // Get a mysqli connection as well for new newer access methods
  $dbi = mysqli_connect($SETTING_DB_HOST, $SETTING_DB_READONLY_USER, $SETTING_DB_READONLY_PASSWORD, $callDB);
  if (!$dbi) {
  	die("Unable to connect to database");
  }
}
// Connect to database.
$oslDAO::dbConnect($db,"$callDB");

/////////////////////
// User variables //
///////////////////

// Get URL from calling file.
$callURL = str_replace("__AND__","&",$_GET["callURL"]);

// Init user variables.
$authExpired = (isset($_GET["authExpired"]) ? $_GET["authExpired"] : 0);
$username = oslUser::setUsernameAndLogin($_POST, $callDB);
$password = $_POST["password"];
$userID = $_SESSION["user".$callDB."ID"];
$callUser = $_GET["callUser"];

//////////////////////////////////////
// Current time and date variables //
////////////////////////////////////

$unix_timestamp = time();
$nowYmd = date('Y-m-d');
$nowYmdHis = date('YmdHis');
$now = date("Y-m-d H:i:s");
$defaultDateFormat = "dd-mm-yyyy";

/////////////////////
// Call variables //
///////////////////

// Set callContent default.
$content = 1;
// Set callContent
if($_GET["content"] != NULL)
{
	$content = $_GET["content"];
}

///////////////////////////////////////////////////
// Check if permission to view content granted. //
/////////////////////////////////////////////////

if(!$oslUser::checkContentAccess($userID,$content))
{
	$oslUser::kickout();
}

/////////////////////
// File variables //
///////////////////

// Instead of checking the file type, the file extension will be used instead.
$allowed_file_type = $oslFile::getAllowedFileTypes();
// Set upload file directory.
$upload_dir = $SETTINGS_UPLOAD_DIR;
$tn_dir = $upload_dir."tn/";
$callFile = $_GET["callFile"];
$callFileType = $_GET["callFileType"];
// Set width and height of thumbnails.
$tnW = 100;
$tnH = 100;

///////////////////////
// Search variables //
/////////////////////

$qSectID = $_GET["qSectID"];
$qRepID = $_GET["qRepID"];
$counter = $_GET["counter"];
$startPage = $_GET["startPage"];
$searchArray = $_SESSION["searchArray"];

//////////////////////
// Admin variables //
////////////////////

// Set admin type.
$adminType = $_GET["adminType"];

// Manage report variables.
$callRep = $_GET["callRep"];
// If on homepage and calling a specific report, make sure that any search session is ended.
if($callRep != NULL && $content == 1 && !empty($searchArray))
{
	// Unset the search array.
	unset($_SESSION["searchArray"]);
}
$step = $_GET["step"];
$addCommentTo = $_GET["addCommentTo"];
if($addCommentTo == NULL)
{
	$addCommentTo = $_POST["addCommentTo"];
}
$adminDel = $_GET["adminDel"];
$preview = $_GET["preview"];
$printCall = $_GET["printCall"];
$callHelp = $_GET["callHelp"];

$editorInstance = $_POST["editorInstance"];
$callEditor = $_GET["callEditor"];

$nextStep = $_GET["nextStep"];
$reportTitle = $oslDAO::filterForMySQL($_POST["reportTitle"]);
$reportText = $oslDAO::filterForMySQL($_POST["editor1"]);
$reportTask = $_POST["reportTask"];
if($reportTask == NULL)
{
	$reportTask = $_GET["reportTask"];
}
$reportAuthors = $oslDAO::filterForMySQL($_POST["reportAuthors"]);

// ADMIN variables.
$adSectionName = $oslDAO::filterForMySQL($_POST["adSectionName"]);
$edSectionID = $_POST["edSectionID"];
if($edSectionID == NULL)
{
	$edSectionID = $_GET["edSectionID"];
}
$adTaskName = $oslDAO::filterForMySQL($_POST["adTaskName"]);
$adAppName = $oslDAO::filterForMySQL($_POST["adAppName"]);
$adAppAccess = $oslDAO::filterForMySQL($_POST["adAppAccess"]);
$adAppRestrict = $_POST["adAppRestrict"];
$adAppLoginMethod = $_POST["adAppLoginMethod"];
$adUserEmail = $oslDAO::filterForMySQL($_POST["adUserEmail"]);
$editorInstance = $_POST["editorInstance"];
$MNTaskFK = $_POST["MNTaskFK"];
$appDocRoot = $SETTINGS_DOC_ROOT;
$appScriptName = $_SERVER["SCRIPT_NAME"];
$appDir = $appDocRoot."/";
$appDir = str_replace("index.php","",$appDir);
$appDir = str_replace("includes/confirmation.php","",$appDir);
$appURL = $_SERVER["SERVER_NAME"].$appScriptName;
$appURL = str_replace("index.php","",$appURL);
$appURL = str_replace("iframeSrc.php","",$appURL);
$appURL = str_replace("includes/confirmation.php","",$appURL);
$adAppMailDomain = $oslDAO::filterForMySQL($_POST["adAppMailDomain"]);
$adAppUseThumbnails = $_POST["adAppUseThumbnails"];
$adAppMaxFileSize = $oslDAO::filterForMySQL($_POST["adAppMaxFileSize"]);
$adFileType = $oslDAO::filterForMySQL($_POST["adFileType"]);
$adFileMimeType = $oslDAO::filterForMySQL($_POST["adFileMimeType"]);
$adFileImage = $_POST["adFileImage"];
$adLdapHost = $oslDAO::filterForMySQL($_POST["adLdapHost"]);
$adLdapDN = $oslDAO::filterForMySQL($_POST["adLdapDN"]);
$adTagID = $oslDAO::filterForMySQL($_POST["adTagID"]);
$adTagName = $oslDAO::filterForMySQL($_POST["adTagName"]);
$tagList = array();
if (isset($_POST["reportTag"]) && is_array($_POST["reportTag"])) {
		foreach ($_POST["reportTag"] as $curTag) {
				$curTagVal = intval($oslDAO::filterForMySQL($curTag));
				if ($curTagVal > 0) {
						$tagList[] = $curTagVal;
				}
		}
}

if ($content == 1) {
		//$reportSection
		$searchReportSection = $oslDAO::filterForMySQL($_GET["searchSection"]); //intval($_GET["searchSection"],10);
		$searchReportTask = $oslDAO::filterForMySQL($_GET["searchTask"]); //intval($_GET["searchTask"],10);

		if ($searchReportSection != "") {
				oslContent::buildSearchArrayFromTextInput($searchReportSection, $searchReportTask);
		}
}

// User management.
$callUser = $_GET["callUser"];

?>
