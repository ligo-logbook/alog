<?php
/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/

// Initialise general variables.
require_once("initVar.php");

///////////////////
// SEARCH CALLS //
/////////////////

if($adminType == "search")
{
    // Build the search array.
    $_SESSION["searchArray"] = oslContent::buildSearchArray($_POST);
    $redirectURL = "../index.php";
}
elseif($adminType == "quickSearch")
{
    // Build the search array for a quick search
    $_SESSION["searchArray"] = oslContent::buildQuickSearchArray($_POST);
    $redirectURL = "../index.php";
}
elseif($adminType == "endSearch")
{
    // Unset the search array.
    unset($_SESSION["searchArray"]);
    $redirectURL = "../index.php?content=$content";
} else {
    $redirectURL = "../index.php";
}

// Redirect.
header("location: $redirectURL");
?>