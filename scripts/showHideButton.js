/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/

/* Show/Hide button. */
function showHideButton()
{
	// Get values.
	var reportTitle = document.getElementById("reportTitle").value;
	var reportText = document.getElementById("reportText").value;
	var reportAuthors = document.getElementById("reportAuthors").value;
	if(reportTitle == '' || reportText == '' || reportAuthors == '')
	{
		document.getElementById("uploadFilesButton").className="buttonHidden";
		document.getElementById("saveToDraftButton").className="buttonHidden";
		document.getElementById("postToLogbookButton").className="buttonHidden";
		document.getElementById("warningMsg").innerHTML = 'Enter report details and a title to have access to the buttons.';
	}
	else
	{
		document.getElementById("uploadFilesButton").className="buttonVisible";
		document.getElementById("saveToDraftButton").className="buttonVisible";
		document.getElementById("postToLogbookButton").className="buttonVisible";
		document.getElementById("warningMsg").innerHTML = '';
	}
}