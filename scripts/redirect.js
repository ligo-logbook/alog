/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/

/* Send to requested location. */
function send(way,repID,addCommentTo)
{
	var result = false;
	var str = "The following fields are empty: ";
	var strA = "";
	var strB = "";
	var strC = "";
	var reportTitle = document.getElementById("reportTitle").value;
//	var reportText = document.getElementById("editor1").value;
//	var cke = CKEDITOR.instances.editor1;
//	var ckeContents = cke.getData();
	var reportAuthors = document.getElementById("reportAuthors").value;
	if(way != 6
	&& (reportTitle == '' || reportAuthors == ''))
	{
	 	if(reportTitle == '')
	 	{
			strA = "Title ";
			document.getElementById("reportTitle").focus();
		}
//	 	if(reportText == '' && ckeContents == '')
//	 	{
//			strB = "Details ";
//		}
	 	if(reportAuthors == '')
	 	{
			strC = "Author ";
			document.getElementById("reportAuthors").focus();
		}
		str = str + strA + strB + strC + "- Please complete them and then try again.";
		alert(str);
	}
	else
	{
		// Initialise array.
		var ways = new Array();
		ways[1] = 'includes/confirmation.php?adminType=saveReport&callRep=' + repID + '&addCommentTo=' + addCommentTo + '&nextStep=' + way;
		ways[2] = 'includes/confirmation.php?adminType=saveReport&callRep=' + repID + '&addCommentTo=' + addCommentTo + '&nextStep=' + way;
		ways[3] = 'includes/confirmation.php?adminType=saveReport&callRep=' + repID + '&addCommentTo=' + addCommentTo + '&nextStep=' + way;
		ways[4] = 'includes/confirmation.php?adminType=deleteReport&callRep=' + repID + '&addCommentTo=' + addCommentTo + '&nextStep=' + way;
		ways[5] = 'includes/confirmation.php?adminType=saveReport&callRep=' + repID + '&addCommentTo=' + addCommentTo + '&nextStep=' + way;
		ways[6] = 'includes/confirmation.php?adminType=saveReport&callRep=' + repID + '&addCommentTo=' + addCommentTo + '&nextStep=' + way;
		// Set location to form and submit.
		document.forms["manageReport"].action=ways[way];
		document.forms["manageReport"].submit();
		result = true;
	}
	return result;
}