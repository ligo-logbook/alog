<?php
/*
Copyright (C) 2010,  LIGO.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Jonathan Hanks <hanks_j@ligo-wa.caltech.edu>.
*/

define('WS_VER2', 2.0);
define('WS_DO_POST_LOG', 3);
define('WS_DO_SAVE_DRAFT', 1);
define('WS_HTTP_OK', '200 OK');
define('WS_HTTP_ERR_GEN', '400 Bad Request');
define('WS_HTTP_ERR_ARG', '400 Bad Arguments');

// define some things that should be set before the cleanup function gets registered
$ws_delete_on_shutdown = FALSE;
$ws_err_msg = '';
$ws_err = FALSE;
$ws_http_code = '200 OK';

// skip doing anything other than replying "OK" if it's not an HTTP POST
// this will keep ECP-cookie-init output quieter
if($_SERVER['REQUEST_METHOD'] != 'POST') {
    header("HTTP/1.1 ".WS_HTTP_OK);
    header("Content-type: application/json");
    echo "{}";
    exit(0);
}

// I don't like this positioning, but too many things can do output, so ...
ob_start();
register_shutdown_function('ws_cleanup');

// setup all global variables
require_once('../includes/initVar.php');

// ws_cleanup
//  Exit callback for the 'web-service'.  This will do all cleanup, removing sessions, on error it will remove the report and
// any attachments.  It is also the source of all output from the web-service.
function ws_cleanup()
{
    global $ws_http_code, $ws_err, $ws_err_msg;
    global $ws_delete_on_shutdown,$ws_report_id, $userID, $upload_dir, $appDir;

    if ($ws_delete_on_shutdown) {
        // we are here because adding an attachment failed
        oslDAO::deleteReport($ws_report_id,$userID,'',$upload_dir,'',$appDir);
    }
    // clean up the session
    session_destroy();
    // throw any output away
    ob_end_clean();
    header("HTTP/1.1 ".$ws_http_code);
    header("Content-type: application/json");
    $json_success_template = '{"version":%f,"report_id":%d}';
    $json_error_template = '{"version":%f,"error_message":"%s"}';
    if ($ws_err) {
      $retjson = sprintf($json_error_template, WS_VER2, $ws_err_msg);
    } else {
      $retjson = sprintf($json_success_template, WS_VER2, $ws_report_id);
    }
    echo $retjson;
}

function ws_error($msg, $http_code=WS_HTTP_ERR_GEN)
{
    global $ws_err, $ws_err_msg;

    $ws_err_msg = $msg;
    $ws_err = TRUE;
    exit(1);
}

$WS_VARS = array('WS_VER', 'WS_TITLE', 'WS_SECTION', 'WS_TASK', 'WS_TEXT', 'WS_TEXT_FORMAT', 'WS_NUM_ATTACHMENTS');

if($userID == FALSE) {
    $msg =  "Not authorized\n_SERVER = ".print_r($_SERVER, TRUE)."\n";
    $msg .= "_SESSION = ".print_r($_SESSION)."\n";
    $msg .= "userID = $userID\n";

    ws_error($msg, WS_HTTP_ERR_GEN);
}

foreach ($WS_VARS as $var_name) {
    if (!isset($_POST[$var_name])) {
        ws_error("Missing parameter $var_name", WS_HTTP_ERR_ARG);
    }
}
$ws_ver = $_POST['WS_VER'];
if ($ws_ver != WS_VER2) {
    ws_error("Unsuported API version", WS_HTTP_ERR_ARG);
}
$ws_num_attachments = intval($_POST['WS_NUM_ATTACHMENTS']);
for ($i=0; $i < $ws_num_attachments; $i++) {
    if (!isset($_FILES['WS_ATTACHMENT_'.$i])) {
        ws_error("Missing attachment $i", WS_HTTP_ERR_ARG);
    }
}


$ws_title = oslDAO::filterForMySQL($_POST['WS_TITLE']);
$ws_section = oslDAO::filterForMySQL($_POST['WS_SECTION']);
$ws_task = oslDAO::filterForMySQL($_POST['WS_TASK']);
$ws_text = oslDAO::filterForMySQL($_POST['WS_TEXT']);
$ws_text_format = intval($_POST['WS_TEXT_FORMAT']);

if (strlen($ws_title) == 0 || strlen($ws_text) == 0) {
    ws_error('The title and the text must be specified', WS_HTTP_ERR_ARG);
}
if ($ws_text_format <= 0 || $ws_text_format > 2) {
    ws_error('Invalid message format', WS_HTTP_ERR_ARG);
}

// $lookup = oslDAO::executeQuery("SELECT taskID FROM tblTasks, tblSections WHERE sectionID = sectionFK AND sectionName='$ws_section' AND taskName='$ws_task'");
$get = "SELECT taskID FROM tblTasks, tblSections
        WHERE sectionID = sectionFK
        AND sectionName=? AND taskName=?";
$types = "ss";
$input = array($ws_section, $ws_task);
$lookup = oslDAO::executePrepared($get, $types, $input);
if (!$lookup) {
    ws_error('Invalid section or task', WS_HTTP_ERR_ARG);
}
// $ws_task_id = intval(mysql_result($lookup, 0));
$ws_task_id = intval(oslDAO::getFirstResultEntry($lookup, 'taskID'));
if ($ws_task_id == 0) {
    ws_error("Unable to lookup section and task id", WS_HTTP_ERR_ARG);
}
$reportTitle = $ws_title;
$reportText = $ws_text;
$reportTask = $ws_task_id;
$editorInstance = $ws_text_format;
$reportAuthors = $username;
oslDAO::saveReport(NULL, $userID, ($ws_num_attachments > 0 ? WS_DO_SAVE_DRAFT : WS_DO_POST_LOG), '', '');
$ws_report_id = oslContent::getNewReportID($userID);
if ($ws_num_attachments > 0) {
    // this is a special case
    // oslFile::uploadFile may call exit on us, so it may not return.  To catch and cleanup errors
    // here we use a global.
    // We aslo prepopulate the error fields so that they are appropriate
    $ws_err = TRUE;
    $ws_err_msg = "Error processing attachment";
    $ws_http_code = WS_HTTP_ERR_ARG;
    $ws_delete_on_shutdown = TRUE;
    for ($i=0; $i < $ws_num_attachments; $i++) {
        $_FILES['filetoupload'] = $_FILES['WS_ATTACHMENT_'.$i];
        oslFile::uploadFile('', '', $ws_report_id, '');
        unset($_FILES['filetoupload']);
    }
    // turn our flag off.
    $ws_delete_on_shutdown = FALSE;
    // saveReport does not check for errors on the update, so we have no way
    // to know of an error.  We just assume it works.  I prefer not modifying it
    // now, as it returns a url in the main code base.
    // we would clear the error flags here, but we need to do it after the block
    // anyways.
    oslDAO::saveReport($ws_report_id, $userID, WS_DO_POST_LOG, '', '', $appURL);
}
// success
$ws_http_code = WS_HTTP_OK;
$ws_err = FALSE;
?>
