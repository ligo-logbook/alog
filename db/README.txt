This directory contains the database schema definitions and the update scripts to move from older to newer versions.

FRESH INSTALLS:

* pick the latest schema version file (ie osl_schema_v?.sql) and apply that file only.

UPGRADES:

* DO NOT apply any of the osl_schema_v*.sql files.  They will drop your tables!
* Apply the update scripts in order from oldest to newest to migrate your database.