--
-- Table structure for table `tblTags`
--

SET @saved_cs_client    = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `tblTags` (
  `tagID` int(11) NOT NULL auto_increment,
  `tag` varchar(100) NOT NULL UNIQUE,
  PRIMARY KEY (`tagID`),
  UNIQUE KEY `tagUKidx` (`tag`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;


--
-- Table structure for `tblReportTags`
--

SET @saved_cs_client    = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `tblReportTags` (
  `reportTagID` int(11) NOT NULL auto_increment,
  `reportFK` int(11) NOT NULL,
  `tagFK` int(11) NOT NULL,
  PRIMARY KEY (`reportTagID`),
  KEY `reportFKidx` (`reportFK`),
  KEY `tagFKidx` (`tagFK`),
  UNIQUE KEY `reportTagidx` (`reportFK`, `tagFK`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;