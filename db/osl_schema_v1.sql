-- phpMyAdmin SQL Dump
-- version 2.6.4-rc1
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: May 21, 2010 at 03:20 PM
-- Server version: 4.1.12
-- PHP Version: 5.1.2
-- 
-- Database: `osl`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `tblAppInfo`
-- 

CREATE TABLE `tblAppInfo` (
  `appID` int(11) NOT NULL auto_increment,
  `appName` text NOT NULL,
  `appOpen` int(11) NOT NULL default '0',
  `appRestrict` int(11) NOT NULL default '0',
  `appLoginMethod` int(11) NOT NULL default '0',
  `appMailDomain` text NOT NULL,
  `appMaxFileSize` double NOT NULL default '10000000',
  `appUseThumbnails` int(11) NOT NULL default '0',
  `ldapHost` text NOT NULL,
  `ldapDN` text NOT NULL,
  PRIMARY KEY  (`appID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `tblAppInfo`
-- 

INSERT INTO `tblAppInfo` VALUES (1, 'OS', 0, 0, 0, '', 10000000, 1, '', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `tblContents`
-- 

CREATE TABLE `tblContents` (
  `contentID` int(11) NOT NULL auto_increment,
  `contentName` text NOT NULL,
  `orderPos` int(11) NOT NULL default '0',
  `accessLevel` int(11) NOT NULL default '0',
  PRIMARY KEY  (`contentID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Section information.' AUTO_INCREMENT=9 ;

-- 
-- Dumping data for table `tblContents`
-- 

INSERT INTO `tblContents` VALUES (1, 'Home', 1, 0);
INSERT INTO `tblContents` VALUES (2, 'Search', 2, 0);
INSERT INTO `tblContents` VALUES (3, 'Add report', 3, 2);
INSERT INTO `tblContents` VALUES (4, 'Help', 6, 0);
INSERT INTO `tblContents` VALUES (5, 'L-mail', 5, 2);
INSERT INTO `tblContents` VALUES (6, 'Drafts', 4, 2);
INSERT INTO `tblContents` VALUES (7, 'Admin', 7, 1);
INSERT INTO `tblContents` VALUES (8, 'Users', 8, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `tblFileTypes`
-- 

CREATE TABLE `tblFileTypes` (
  `fileTypeID` int(11) NOT NULL auto_increment,
  `fileType` text NOT NULL,
  `fileMimeType` text NOT NULL,
  `fileImage` int(11) NOT NULL default '0',
  PRIMARY KEY  (`fileTypeID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

-- 
-- Dumping data for table `tblFileTypes`
-- 

INSERT INTO `tblFileTypes` VALUES (1, 'doc', 'application/msword', 0);
INSERT INTO `tblFileTypes` VALUES (2, 'pdf', 'application/pdf', 0);
INSERT INTO `tblFileTypes` VALUES (3, 'xls', 'application/vnd.ms-excel', 0);
INSERT INTO `tblFileTypes` VALUES (4, 'ppt', 'application/vnd.ms-powerpoint', 0);
INSERT INTO `tblFileTypes` VALUES (8, 'jpg', 'image/jpeg', 1);
INSERT INTO `tblFileTypes` VALUES (9, 'png', 'image/png', 1);
INSERT INTO `tblFileTypes` VALUES (10, 'gif', 'image/gif', 1);
INSERT INTO `tblFileTypes` VALUES (11, 'tif', 'image/tiff', 0);
INSERT INTO `tblFileTypes` VALUES (12, 'ps', 'application/postscript', 0);
INSERT INTO `tblFileTypes` VALUES (13, 'txt', 'text/plain', 0);
INSERT INTO `tblFileTypes` VALUES (14, 'dft', 'application/octet-stream', 0);
INSERT INTO `tblFileTypes` VALUES (15, 'asm', 'text/plain', 0);
INSERT INTO `tblFileTypes` VALUES (16, 'pptx', 'application/vnd.ms-powerpoint', 0);
INSERT INTO `tblFileTypes` VALUES (17, 'docx', 'application/msword', 0);
INSERT INTO `tblFileTypes` VALUES (18, 'xlsx', 'application/vnd.ms-excel', 0);
INSERT INTO `tblFileTypes` VALUES (19, 'mpp', 'application/msproject', 0);
INSERT INTO `tblFileTypes` VALUES (20, 'pps', 'application/vnd.ms-powerpoint', 0);
INSERT INTO `tblFileTypes` VALUES (21, 'mpg', 'video/mpeg', 0);
INSERT INTO `tblFileTypes` VALUES (22, 'avi', 'video/x-msvideo', 0);
INSERT INTO `tblFileTypes` VALUES (23, 'wav', 'audio/x-wav', 0);
INSERT INTO `tblFileTypes` VALUES (24, 'bmp', 'image/bmp', 1);
INSERT INTO `tblFileTypes` VALUES (25, 'dwg', 'application/acad', 0);
INSERT INTO `tblFileTypes` VALUES (26, 'rtf', 'text/rtf', 0);
INSERT INTO `tblFileTypes` VALUES (27, 'mov', 'video/quicktime', 0);
INSERT INTO `tblFileTypes` VALUES (28, 'par', 'application/octat-stream', 0);
INSERT INTO `tblFileTypes` VALUES (29, 'xmcd', 'application/octet-stream', 0);
INSERT INTO `tblFileTypes` VALUES (30, 'kat', '', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `tblFiles`
-- 

CREATE TABLE `tblFiles` (
  `fileID` int(11) NOT NULL auto_increment,
  `reportFK` int(11) NOT NULL default '0',
  `filename` text NOT NULL,
  `fileTypeFK` int(11) NOT NULL default '0',
  `date_uploaded` datetime NOT NULL default '0000-00-00 00:00:00',
  `filesize` int(11) NOT NULL default '0',
  PRIMARY KEY  (`fileID`),
  KEY `reportFK` (`reportFK`),
  KEY `fileTypeFK` (`fileTypeFK`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `tblFiles`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tblHelp`
-- 

CREATE TABLE `tblHelp` (
  `helpID` int(11) NOT NULL auto_increment,
  `helpParentFK` int(11) NOT NULL default '0',
  `helpTitle` text NOT NULL,
  `helpTxt` text NOT NULL,
  `helpImgA` text NOT NULL,
  `helpRefLetter` text NOT NULL,
  PRIMARY KEY  (`helpID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `tblHelp`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tblMailNotification`
-- 

CREATE TABLE `tblMailNotification` (
  `MNID` int(11) NOT NULL auto_increment,
  `taskFK` int(11) NOT NULL default '0',
  `userFK` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MNID`),
  KEY `taskFK` (`taskFK`),
  KEY `userFK` (`userFK`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `tblMailNotification`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tblReports`
-- 

CREATE TABLE `tblReports` (
  `reportID` int(11) NOT NULL auto_increment,
  `taskFK` int(11) NOT NULL default '0',
  `reportTitle` text NOT NULL,
  `reportText` text NOT NULL,
  `authorFK` int(11) NOT NULL default '0',
  `authorNames` text NOT NULL,
  `parentFK` int(11) NOT NULL default '0',
  `postConfirmed` int(11) NOT NULL default '0',
  `dateAdded` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`reportID`),
  KEY `taskFK` (`taskFK`),
  KEY `authorFK` (`authorFK`),
  KEY `parentFK` (`parentFK`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `tblReports`
-- 

INSERT INTO `tblReports` VALUES (1, 1, 'The OSLogbook', ' Welcome to the OSLogbook application.', 1, 'admin', 0, 1, '2010-05-21 15:18:48');

-- --------------------------------------------------------

-- 
-- Table structure for table `tblSections`
-- 

CREATE TABLE `tblSections` (
  `sectionID` int(11) NOT NULL auto_increment,
  `sectionName` text NOT NULL,
  `sectionColour` varchar(6) NOT NULL default '',
  PRIMARY KEY  (`sectionID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `tblSections`
-- 

INSERT INTO `tblSections` VALUES (1, 'Logbook Admin', '99FF99');

-- --------------------------------------------------------

-- 
-- Table structure for table `tblTasks`
-- 

CREATE TABLE `tblTasks` (
  `taskID` int(11) NOT NULL auto_increment,
  `sectionFK` int(11) NOT NULL default '0',
  `taskName` text NOT NULL,
  PRIMARY KEY  (`taskID`),
  KEY `sectionFK` (`sectionFK`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `tblTasks`
-- 

INSERT INTO `tblTasks` VALUES (1, 1, 'General');

-- --------------------------------------------------------

-- 
-- Table structure for table `tblUserGroups`
-- 

CREATE TABLE `tblUserGroups` (
  `userGroupID` int(11) NOT NULL auto_increment,
  `userGroup` text NOT NULL,
  PRIMARY KEY  (`userGroupID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `tblUserGroups`
-- 

INSERT INTO `tblUserGroups` VALUES (2, 'Standard user');
INSERT INTO `tblUserGroups` VALUES (1, 'Administrator');
INSERT INTO `tblUserGroups` VALUES (5, 'Forbidden');

-- --------------------------------------------------------

-- 
-- Table structure for table `tblUsers`
-- 

CREATE TABLE `tblUsers` (
  `userID` int(11) NOT NULL auto_increment,
  `forename` text NOT NULL,
  `surname` text NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `email` text NOT NULL,
  `lastLogin` datetime NOT NULL default '0000-00-00 00:00:00',
  `userGroupFK` int(11) NOT NULL default '0',
  PRIMARY KEY  (`userID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `tblUsers`
-- 

INSERT INTO `tblUsers` VALUES (1, '', '', 'admin', 'osl_admin!260508', '', '2010-05-21 15:18:03', 1);
