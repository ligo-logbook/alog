DROP TABLE IF EXISTS `tblReportsHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblReportsHistory` (
  changeID int(20) not null auto_increment  primary key,
  reportID int(11) not null,
  taskFK int(11) not null,
  reportTitle text not null,
  reportText text not null,
  authorNames text not null,
  changeDate timestamp default current_timestamp
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

delimiter //
CREATE TRIGGER `tblReports_update` AFTER UPDATE ON `tblReports`
FOR EACH ROW BEGIN
insert into tblReportsHistory
set 
	reportID=OLD.reportID,
	taskFK=OLD.taskFK,
	reportTitle=OLD.reportTitle,
	reportText=OLD.reportText,
	authorNames=OLD.authorNames,
	changeDate=NOW();
END
//
delimiter ;
